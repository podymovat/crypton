<?php

class CatalogController extends BaseController {

	const ERROR_400 = "Не удалось обработать сообщение об ошибке";

	const ERROR_405 = "Метод не поддерживается";

	const ERROR_421 = "Размер архива слишком велик";

	const ERROR_429 = "Повторите запрос позже";

	const ERROR_500 = "Неизвестная ошибка";

    public function anyCurrentOld()
    {
        return json_encode(array("status" => "success","zip_url" => "http://173.255.193.235/sync/0000000009.zip"));
    }



    public function anyCurrent()
    {
        $result = '';

        $syncfolder = "/var/www/public/sync/";
        $testdatafolder = "/var/www/public/testdata/";
        $documentsfolder = "/var/www/catalogdata/documents/";
        $mediafolder = "/var/www/catalogdata/media/";
        $needlepicsfolder = "/var/www/public/needlepics/";


        //Проверяем, нет ли готового архива с самыми свежими изменениями

        $lastChange = ChangelogItem::orderBy('time','DESC')->first();

        if(file_exists($syncfolder.($lastChange->time).'.zip')){
            //Файл уже есть. Не перегенерируем ничего.
            $path = ("http://173.255.193.235/sync/".($lastChange->time).".zip");
            return json_encode(array("status" => "success","zip_url" => $path,"last_updated" => '"'.($lastChange->time).'"'));
        }

        //Файла не оказалось. Собираем.

        $date = new DateTime();

        $name = ($lastChange->time);

        $zip = new ZipArchive;
        $res = $zip->open($syncfolder.($name).'.zip', ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
        if ($res === TRUE) {
            $result = 'ok';
            //contents file
            $zip->addFile($testdatafolder.'index.json','index.json');
            //product lists
            $zip->addFromString('products-main/index.json', $this->getMainjson());
            $zip->addFromString('products-other/index.json', $this->getOtherjson());
            //

            //product descriptions
            $prods = ProductCategory::where('type','=','main')->get();

             foreach ($prods as $prod) {
                try{
                     $zip->addFromString('product_descriptions/'.($prod->export_id).'.html','<html>'.($prod->html_description).'</html>');
                }catch(Exception $e){

                }
            }



            //documents
            $zip->addFromString('documents/index.json',$this->getDocumentjson());
            //media (stub)
            $zip->addFromString('media/index.json',$this->getMediajson());
            //$zip->addFile($testdatafolder.'/media/index.json','media/index.json');
            //
            //add document files
            $docs = Document::all();
            foreach ($docs as $doc) {
                try{
                     $zip->addFile(($documentsfolder.($doc->id).'/'.($doc->url)),iconv('cp866', 'utf-8', ('documents/'.($doc->url))));
                }catch(Exception $e){

                }
            }

            $docs = MediaFile::all();
            foreach ($docs as $doc) {
                try{
                     $zip->addFile(($mediafolder.($doc->id).'/'.($doc->file)),iconv('cp866', 'utf-8', ('media/'.($doc->file))));
                }catch(Exception $e){

                }
            }

            //needle pics

            $needlepics = array_diff(scandir($needlepicsfolder), array('..', '.'));
            $info = "";

            if(count($needlepics) > 0){
                foreach ($needlepics as $npic) {
                    try{
                        $zip->addFile(($needlepicsfolder.$npic),('needlepics/'.$npic));
                       // $info.= "NPIC:: ".$npic." ".($zip->addFile(($needlepicsfolder.$npic),('needlepics/'.$npic)) == TRUE);
                    }catch(Exception $e){
                       // $info.= "NPIC:: ".$npic." FAIL! ";
                    }
                    
                }
               // $info = implode(" :: ", $needlepics);
            }else{
                return json_encode(array("status" => "success","neeldefail" => 1, "zip_url" => $path,"last_updated" => '"'.($lastChange->time).'"'));
            }
            //
            $zip->close();

            $path = ("http://173.255.193.235/sync/".($name).".zip");

            return json_encode(array("status" => "success","ninfo" => $info, "zip_url" => $path,"last_updated" => '"'.($lastChange->time).'"'));
        } else {
            $result = 'failed, code:' . $res;
            return makeError($res);
        }
        return $result;
    }

    public function trimNeedleCode($src){
        $result = $src;

        $result = trim(htmlspecialchars($src)," ");
        //Убираем лишние пробелы
        $result = trim(preg_replace('/\s+/',' ', $result));

        return $result;
    }

    public function getMainjson(){
        $maindata = array();

        $products = MainProduct::where('enabled','=','1')->get();

        foreach ($products as $product) {
            $nxt = array();

            $ct = ProductCategory::find($product->product_id);
            $nt = NeedleType::find($product->needle_type);

            $nxt["product_id"] = $ct->export_id;
            $nxt["code"] = $product->code;
            $nxt["description"] = $product->description;
            $nxt["string_size"] = $product->string_size;
            $nxt["string_length"] = $product->string_length;
            $nxt["string_colored"] = $product->string_colored;
            $nxt["string_count"] = $product->string_count;
            //Код иглы может содержать лишние пробелы и переносы. Вычищаем их для корректной работы приложения
            $nxt["needle_code"] = $this->trimNeedleCode($product->needle_code);
            $nxt["needle_size"] = $product->needle_size;
            $nxt["needle_curveness"] = $product->needle_curveness;
            $nxt["needle_color"] = $product->needle_color;
            $nxt["needle_type"] = $nt->name;
            $nxt["needle_subtype"] = $product->needle_subtype;
            $nxt["needle_count"] = $product->needle_count;
            $nxt["is_ethalloy"] = (($product->is_ethalloy == 1)? true:false);
            $nxt["steel_type"] = $product->steel_type;
            $nxt["pack_size"] = $product->pack_size;
            $nxt["price"] = $product->price;
            $nxt["notes"] = $product->notes;
            $nxt["similar1"] = $product->similar1;
            $nxt["similar2"] = $product->similar2;

            array_push($maindata, $nxt);
        }


        return json_encode(array(
            'items' => $maindata
            ), JSON_UNESCAPED_UNICODE);
    }
    /*
        JSON: Прочие продукты (клеи, сетки, дренажи,...)
    */
    public function getOtherjson(){
        $otherdata = array();

        $products = OtherProduct::where('enabled','=','1')->get();

        foreach ($products as $product) {
            $nxt = array();

            $ct = ProductCategory::find($product->product_id);

            $nxt["product_id"] = $ct->export_id;
            $nxt["code"] = $product->code;
            $nxt["name"] = $product->name;

            //Для клеев и сеток описание отсутствует согласно API v1.0
            if(($ct->export_id != "SkinGlue") && ($ct->export_id != "MashImplants")){
                $nxt["description"] = $product->description_ru;
            }

            //Форма сетки - только для сеток, согласно API v1.0
            if($ct->export_id == "MashImplants"){
                $nxt["mash_shape"] = $product->mash_shape;
            }

            //Длина раны - только для клеев, согласно API v1.0
            if($ct->export_id == "SkinGlue"){
                $nxt["glue_wound_length"] = $product->glue_wound_length;
            }

            //Строка, т.к. единицы измерения различны
            $nxt["size"] = $product->size;

            $nxt["pack_size"] = $product->pack_size;
            $nxt["price"] = $product->price;

            array_push($otherdata, $nxt);
        }

        return json_encode(array(
            'items' => $otherdata
            ), JSON_UNESCAPED_UNICODE);
    }

    public function getDocumentjson(){
        $docdata = array();

        $docs = Document::all();

        foreach ($docs as $doc) {
            $nxt = array();
            $nxt["id"] = $doc->id;
            $nxt["filename"] = $doc->filename;
            $nxt["url"] = $doc->url;
            array_push($docdata, $nxt);
        }

        return json_encode(array(
            'items' => $docdata
            ), JSON_UNESCAPED_UNICODE);

    }

    public function getMediajson(){
        $docdata = array();

        $docs = MediaFile::all();

        foreach ($docs as $doc) {
            $nxt = array();
            $nxt["id"] = $doc->id;
            $nxt["product_id"] = ProductCategory::find($doc->product_id);
            $nxt["product_name"] = ProductCategory::find($doc->product_id)->export_id;
            $nxt["url"] = $doc->file;
            array_push($docdata, $nxt);
        }

        return json_encode(array(
            'items' => $docdata
            ), JSON_UNESCAPED_UNICODE);

    }

    /*
		Пытаются вызвать что-то не то
    */
    public function missingMethod($parameters = array())
    {
        return $this->makeError(405);
    }

    /*
		Что-то пошло не так - формируем сообщение об ошибке
    */
    protected function makeError($code){

    	$message = "Unknown error";

    	switch ($code) {
    		case 400:
    			$message = self::ERROR_400;
    			break;

    		case 405:
    			$message = self::ERROR_405;
    			break;
    		
    		default:
    			$message = self::ERROR_500;
    			break;
    	}

    	return json_encode(array('status' => 'error', 'error_code' => ''.($code), 'error_message' => $message));
    }

}