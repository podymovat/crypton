<?php

class DocumentAdminController extends BaseController {

	const ERROR_400 = "Не удалось обработать сообщение об ошибке";

	const ERROR_405 = "Метод не поддерживается";

	const ERROR_421 = "Размер архива слишком велик";

	const ERROR_429 = "Повторите запрос позже";

	const ERROR_500 = "Неизвестная ошибка";


    /*
    Минималистичный UI для теста
    */
    
    public function show(){
        $result = '<html><form enctype="multipart/form-data" action="/admin/docs/upload" method="POST"><input type="file" name="docFile"><input type="submit" value="Загрузить" /></form></html>';

        $result.="<table border='1'>";
        $result.="<tr><th>id</th><th>Имя</th><th>Файл</th></tr>";

        $docdata = array();

        $docs = Document::all();

        foreach ($docs as $doc) {
            $result.="<tr><td>".($doc->id)."</td><td>".($doc->filename)."</td><td><a href='#'>".($doc->url)."</a></td></tr>";
        }

        $result.="</table></html>";


        return $result;
    }

    public function upload(){
        if (Input::hasFile('docFile'))
        {
            //FIXME: привести в порядок модели и убрать
            Eloquent::unguard();

            //Вычищаем киррилицу предельно простым способом, независимым от платформы
            //iconv работает недостаточно стабильно, а потери любых символов кроме некиррилических нас не волнуют.
            $source = Input::file('docFile')->getClientOriginalName();

           // $rus = array('ё','ж','ц','ч','ш','щ','ю','я','Ё','Ж','Ц','Ч','Ш','Щ','Ю','Я');
           // $lat = array('yo','zh','tc','ch','sh','sh','yu','ya','YO','ZH','TC','CH','SH','SH','YU','YA');

            //$source = str_replace($rus,$lat,$source);
            //$source = strtr($source,
             //    "АБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬЭабвгдезийклмнопрстуфхъыьэ",
           //      "ABVGDEZIJKLMNOPRSTUFH_I_Eabvgdezijklmnoprstufh_i_e");
            //Добиваем все некиррилические символы
            //$source = iconv("UTF-8", "ASCII//TRANSLIT//IGNORE", $source);

            try{
                //Запись в БД
                $newItem = new Document;
                $newItem->filename = Input::file('docFile')->getClientOriginalName();

                if(Input::get('docName')){
                    $newItem->filename = Input::get('docName');
                }

                $newItem->url = $source;
                $newItem->save();
                //
                mkdir("/var/www/catalogdata/documents/".$newItem->id, 0777);
                //Файл
                Input::file('docFile')->move("/var/www/catalogdata/documents/".$newItem->id,$source);

                //Запись об изменении

                $newChange = new ChangelogItem;
                $newChange->time = time();
                $newChange->description = 'Загружен документ: '.($newItem->filename);
                $newChange->save();
                //

                return Redirect::to('/admin/documents?success=true');
            }catch(Exception $e){                
                return Redirect::to('/admin/documents?success=false');
            }
        }
        return Redirect::to('/admin/documents?success=false');

    }


    public function uploadMedia(){
        if (Input::hasFile('docFile'))
        {
            //FIXME: привести в порядок модели и убрать
            Eloquent::unguard();

            //Вычищаем киррилицу предельно простым способом, независимым от платформы
            //iconv работает недостаточно стабильно, а потери любых символов кроме некиррилических нас не волнуют.
            $source = Input::file('docFile')->getClientOriginalName();

           // $rus = array('ё','ж','ц','ч','ш','щ','ю','я','Ё','Ж','Ц','Ч','Ш','Щ','Ю','Я');
           // $lat = array('yo','zh','tc','ch','sh','sh','yu','ya','YO','ZH','TC','CH','SH','SH','YU','YA');

            //$source = str_replace($rus,$lat,$source);
            //$source = strtr($source,
             //    "АБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬЭабвгдезийклмнопрстуфхъыьэ",
           //      "ABVGDEZIJKLMNOPRSTUFH_I_Eabvgdezijklmnoprstufh_i_e");
            //Добиваем все некиррилические символы
            //$source = iconv("UTF-8", "ASCII//TRANSLIT//IGNORE", $source);

            try{
                //Запись в БД
                $newItem = new MediaFile;
                $newItem->file = Input::file('docFile')->getClientOriginalName();
                $newItem->type = ((preg_match('/^.*\.(.jpg|.jpeg|.png|.tif|.tiff|.JPG|.PNG|.gif|.GIF)$/i', $newItem->file))?'image':'video');
                $newItem->product_id = intval(Input::get('productID'),10);
                $newItem->save();
                //
                mkdir("/var/www/catalogdata/media/".$newItem->id, 0777);
                //Файл
                Input::file('docFile')->move("/var/www/catalogdata/media/".$newItem->id,$source);

                //Запись об изменении

                $newChange = new ChangelogItem;
                $newChange->time = time();
                $newChange->description = 'Загружен медиафайл: '.($newItem->file);
                $newChange->save();
                //

                return Redirect::to('/admin/media?success=true');
            }catch(Exception $e){                
                return Redirect::to('/admin/media?success=false');
            }
        }
        return Redirect::to('/admin/media?success=false');
    }

    public function uploadOthers(){
        $src = file_get_contents(Input::file('docFile')->getRealPath());
        $result = App::make('ProductController')->uploadWithData($src,intval(Input::get('productID')));

        if($result){
            if(Input::get('productID') == '2'){
                return Redirect::to('/admin/products-drainage?success=true');
            }else if(Input::get('productID') == '3'){
                return Redirect::to('/admin/products-glue?success=true');
            }else if(Input::get('productID') == '4'){
                return Redirect::to('/admin/products-mesh?success=true');
            }else if(Input::get('productID') == '5'){
                return Redirect::to('/admin/products-other?success=true');
            }else{
                Redirect::to("/admin");
            }
        }else{
            if(Input::get('productID') == '2'){
                return Redirect::to('/admin/products-drainage?success=false');
            }else if(Input::get('productID') == '3'){
                return Redirect::to('/admin/products-glue?success=false');
            }else if(Input::get('productID') == '4'){
                return Redirect::to('/admin/products-mesh?success=false');
            }else if(Input::get('productID') == '5'){
                return Redirect::to('/admin/products-other?success=false');
            }else{
                Redirect::to("/admin");
            }
        }

        return Redirect::to("/admin");
    }

    public function uploadMain(){
        $src = file_get_contents(Input::file('docFile')->getRealPath());
        $tmp = App::make('ParserController');
        $tmp->main_data = $src;
        $res = $tmp->anyMain();

        if($res == false){
            return Redirect::to('/admin/products-other?success=false');
        }

        return Redirect::to('/admin/products-other?success=true');
    }



    public function editNeedlePic(){
        $columns = array('Поле','Значение');
        $values = array();
        $returnpage = 'needlepics';

        $pic = NeedlePicture::where('id','=',Input::get('id'))->first();

        if(!$pic){
            return Redirect::to('/admin');
        }

        $nameBase = ($pic->name);
        $is_double = ($pic->is_double);

        if($is_double == 1){
            $nameBase = $nameBase."-double";
        }

        $urlBig = '../../needlepics/'.$nameBase.'@2x.png';
        $urlSmall = '../../needlepics/'.$nameBase.'.png';

        $needle_name = ($pic->is_double == 1)?(htmlspecialchars($pic->name)." (double)"):(htmlspecialchars($pic->name));

        $values[] = array('id',$pic->id);
        $values[] = array('Имя',$needle_name."<input type='hidden' name='id' value='".(htmlspecialchars($pic->id))."'></input><input type='hidden' name='name' value='".(htmlspecialchars($pic->name))."'></input>");
        $values[] = array('Двойная игла?',(($pic->is_double == 1)?('Да'):('Нет')));
        $values[] = array('Изображние (высокое разрещение)<br/><img class="needleimg" src="../../needlepics/'.($urlBig).'?rand='.(rand(1,1000000)).'"></img>','Загрузить новый:<br/><br/><input type="file" class="form-control" name="file_original"></input>');
        $values[] = array('Изображние (стандарный размер)<br/><img class="needleimg" src="../../needlepics/'.($urlSmall).'?rand='.(rand(1,1000000)).'"></img>','Загрузить новый:<br/><br/><input type="file" class="form-control" name="file_ipad"></input>');

        return View::make('editNeedle',array('page' => 'item-edit', 'pagename' => 'Редактирование иглы', 'returnpage' => $returnpage, 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));
    }

    public function deleteNeedlePic(){

        if(Input::get('id')){
            $removeID = intval(Input::get('id'),10);

            $pic = NeedlePicture::where('id','=',$removeID)->first();
            if($pic){
                try{
                    $nameBase = ($pic->name);
                    if($pic->is_double == 1){
                        $nameBase = $nameBase.'-double';
                    }

                    unlink("/var/www/public/needlepics/".$nameBase."@2x.png");
                    unlink("/var/www/public/needlepics/".$nameBase.".png");
                }catch(Exception $e){
                    //Если файлов и не было, не страшно. lock файлам не делаем.
                }

                
                $pic->delete();
            }
        }

        return Redirect::to('/admin/needlepics');        
    }

    public function changeNeedlePic(){
        $result = null;

        //FIXME: привести в порядок модели и убрать
        Eloquent::unguard();

        if(Input::get('id')){

            //По неясной причине, с id не работает редактирование

            //Обрезаем пробелы по краям
          //  $name = trim(htmlspecialchars(Input::get('name'))," ");
            //Убираем лишние пробелы
           // $name = trim(preg_replace('/\s+/',' ', $name));

            $pic = NeedlePicture::where('id','=',intval(Input::get('id')))->first();

            //

            $is_double = $pic->is_double;

            if(Input::get('is_double')){
                if(Input::get('is_double') == 1 || Input::get('is_double') == true || Input::get('is_double') == 'true'){
                    $is_double = 1;
                }
            }


            $nameBase = ($pic->name);

            if($is_double == 1){
                $nameBase = $nameBase."-double";
            }

            $bigName = $nameBase."@2x.png";
            $smallName = $nameBase.".png";


            if(Input::file('file_original')){
                
                try{
                    unlink("/var/www/public/needlepics/".($pic->file_original));

                }catch(Exception $e){
                    //no file = ok
                }

                Input::file('file_original')->move("/var/www/public/needlepics",$bigName);

                //original = @2x.png
                //iPad = name.png
            }


            if(Input::file('file_ipad')){

                try{
                    unlink("/var/www/public/needlepics/".($pic->file_ipad));
                }catch(Exception $e){
                    //no file = ok
                }


                Input::file('file_ipad')->move("/var/www/public/needlepics",$smallName);
            }

            $pic->save();

            //Запись об изменении

            $newChange = new ChangelogItem;
            $newChange->time = time();
            $newChange->description = 'Изменена игла: '.($pic->name);
            $newChange->save();


            $result = Redirect::to('/admin/needlepics?success=true');

        }else{
            $result = Redirect::to('/admin/needlepics?success=false');
        }
        return $result;
    }


    public function createNeedlePic(){
        $result = null;

        //FIXME: привести в порядок модели и убрать
        Eloquent::unguard();

        if(Input::file('file_original') && Input::file('file_ipad') && Input::get('name')){
            //Обрезаем пробелы по краям
            $name = trim(htmlspecialchars(Input::get('name'))," ");
            //Убираем лишние пробелы
            $name = trim(preg_replace('/\s+/',' ', $name));


            ///Проверяем, нет ли уже такой иглы в базе. Критерия два: имя+двойная ли.

            $pic = NeedlePicture::where('name','=',$name)->where('is_double', '=', 1)->first();

            if($pic){
                //Такая игла есть, заменяем ее.
            }else{
                $pic = new NeedlePicture;
                $pic->name = $name;
            }

   

            if(Input::get('is_double') && Input::get('is_double') == 'true'){
                $name=$name.'-double';
                $pic->is_double = 1;
            }


            $filename = $name."@2x.png";//(str_replace(' ','_',$name))."_original.png";

            $pic->file_original = $filename;

            Input::file('file_original')->move("/var/www/public/needlepics",$filename);


            $filename2 = $name.".png";

            $pic->file_ipad = $filename2;

            Input::file('file_ipad')->move("/var/www/public/needlepics",$filename2);

            $pic->save();

            //Запись об изменении

            $newChange = new ChangelogItem;
            $newChange->time = time();
            $newChange->description = 'Загружена игла: '.($pic->name);
            $newChange->save();


            $result = Redirect::to('/admin/needlepics?success=true');

        }else{
            $result = Redirect::to('/admin/needlepics?success=false');
        }
        return $result;
    }




    /*
		Пытаются вызвать что-то не то
    */
    public function missingMethod($parameters = array())
    {
        return $this->makeError(405);
    }

    /*
		Что-то пошло не так - формируем сообщение об ошибке
    */
    protected function makeError($code){

    	$message = "Unknown error";

    	switch ($code) {
    		case 400:
    			$message = self::ERROR_400;
    			break;

    		case 405:
    			$message = self::ERROR_405;
    			break;
    		
    		default:
    			$message = self::ERROR_500;
    			break;
    	}

    	return json_encode(array('status' => 'error', 'error_code' => ''.($code), 'error_message' => $message));
    }

}