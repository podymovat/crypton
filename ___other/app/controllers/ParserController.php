<?php

class ParserController extends BaseController {

	public $main_data = "Код	Англ описание	Продукт	Окраш: да (1), нет (0), (2) - окраш и неокраш	Размер нити	Длина нити	Код иглы	Размер иглы	Кривизна	Тип иглы	Тип иглы пиктограмма	Кол-во отрезков	Тип стали	Цвет иглы: 1 (Visi Black), 0 обычн	Кол-во игл	Шт. в упаковке	Рекомендованная цена (вкл.НДС)	примечания	Аналог 1 	Аналог 2	
MPY493H	MONOCRYL	MONOCRYL	0	5/0	45	P-3 PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	11849,98	 	MCP493H	 	
MPY500H	MONOCRYL	MONOCRYL	0	5/0 	45	PS-3 PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	11511,41	 	MCP500H	 	
MPY501H	MONOCRYL	MONOCRYL	0	4/0	45	PS-3 PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	9740,49	 	MCP3205G	 	
MPY496H	MONOCRYL	MONOCRYL	0	4/0	45	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	9740,48	 	MCP496H	 	
MPY497H	MONOCRYL	MONOCRYL	0	3/0 	45	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	36	9740,49	 	MCP4271H	 	
MPY3213H	MONOCRYL	MONOCRYL	0	3/0 	70	PS PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	36	9740,48	 	 	 	
PEE2993H	PDS	PDS II	1	4/0	90	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	18405,79	 	PDP2993H	 	
PFF2992E	PDS	PDS II	1	4/0	90	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	2	24	12396,87	 	 	 	
PFF2993H	PDS	PDS II	1	3/0	90	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	2	36	16603,00	 	 	 	
V170H	VICRYL	VICRYL	0	0	90	CTX  PLUS	48	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	17149,86	 	 	 	
V1226H	VICRYL	VICRYL	1	2/0	45	-	-	-	-	 	6	-	-	-	36	7117,88	 	 	 	
V1227E	VICRYL	VICRYL	1	0	45	-	-	-	-	 	6	-	-	-	24	6524,45	 	 	 	
V1228E	VICRYL	VICRYL	1	1	45	-	-	-	-	 	6	-	-	-	24	8116,55	 	 	 	
V219H	VICRYL	VICRYL	0	3/0	70	SH-1 PLUS	22	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	3313,04	 	 	 	
V244H	VICRYL	VICRYL	1	0	90	SH PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6460,64	 	VCP244H	 	
V277H	VICRYL	VICRYL	1	3/0	70	V-6	22	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	2186,99	 	 	 	
V318H	VICRYL	VICRYL	1	0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5814,49	 	VCP318H	 	
V324H	VICRYL	VICRYL	1	0	70	MH  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	4092,58	 	VCP324H	 	
V326H	VICRYL	VICRYL	1	2/0	90	CT-2  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7571,27	 	VCP326H	 	
V375H	VICRYL	VICRYL	1	2/0	70	UR-5	36	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	36	6955,06	 	VCP376H	 	
V572G	VICRYL	VICRYL	1	4/0	45	S-4	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7150,71	 	 	 	
V741G	VICRYL	VICRYL	1	1	45	CT-1  PLUS	36	1/2	Колющая PLUS	 	8	Нержавеющая сталь	0	1	12	17091,32	 	 	 	
V782H	VICRYL	VICRYL	1	3/0	45	SH-1  PLUS	36	1/2	Колющая PLUS	 	4	Нержавеющая сталь	1	1	36	10936,93	 	 	 	
V7820E	VICRYL	VICRYL	1	3/0	45	JB-1	22	1/2	Колющая	 	4	Нержавеющая сталь	1	1	24	8024,66	 	 	 	
V824H	VICRYL	VICRYL	0	3/0	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	8818,93	 	 	 	
V903H	VICRYL	VICRYL	1	4/0	45	-	-	-	-	 	12	-	-	-	36	12363,19	 	 	 	
V904H	VICRYL	VICRYL	1	3/0	45	-	-	-	-	 	12	-	-	-	36	12009,96	 	 	 	
V905E	VICRYL	VICRYL	1	2/0	45	-	-	-	-	 	12	-	-	-	24	9565,72	 	 	 	
V906E	VICRYL	VICRYL	1	0	45	-	-	-	-	 	12	-	-	-	24	11084,47	 	 	 	
V946H	VICRYL	VICRYL	0	0	90	CT-1  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	3858,72	 	 	 	
V960G	VICRYL	VICRYL	1	10/0	10	CS140-6	6,5	3/8    	Шпательная	 	1	Нержавеющая сталь	0	1	12	11853,63	 	 	 	
W1703	VICRYL	VICRYL	1	9/0	10	TG140-6	6,5	3/8    	Шпательная	 	1	Нержавеющая сталь	0	1	12	10391,94	 	 	 	
W3201	MONOCRYL	MONOCRYL	0	4/0	45	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2235,05	 	 	 	
W3202	MONOCRYL	MONOCRYL	0	3/0 	70	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2647,80	 	 	 	
W3203	MONOCRYL	MONOCRYL	0	5/0	45	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3172,03	 	MCP493H	 	
W3204	MONOCRYL	MONOCRYL	0	5/0 	45	PS-3  PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3172,03	 	MCP500H	 	
W3205	MONOCRYL	MONOCRYL	0	4/0	45	PS-3  PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	2865,22	 	MCP3205G	 	
W3206	MONOCRYL	MONOCRYL	0	4/0	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	12	2809,04	 	 	 	
W3207	MONOCRYL	MONOCRYL	0	3/0 	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	12	2809,04	 	MCP497H	 	
W3209	MONOCRYL	MONOCRYL	0	5/0 	70	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2809,04	 	MCP3209G	 	
W3213	MONOCRYL	MONOCRYL	0	3/0 	70	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	2865,22	 	MCP3213H	 	
W3214	MONOCRYL	MONOCRYL	0	6/0	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3341,73	 	 	 	
W3215	MONOCRYL	MONOCRYL	0	6/0	45	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3341,73	 	 	 	
W3221	MONOCRYL	MONOCRYL	0	5/0 	45	FS-3	16	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2235,05	 	MCP3221G	 	
W3224	MONOCRYL	MONOCRYL	1	6/0	45	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	1	12	2999,66	 	MCP3224G	 	
W3326	MONOCRYL	MONOCRYL	0	3/0 	70	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2554,22	 	MCP3326G	 	
W3327	MONOCRYL	MONOCRYL	0	2/0 	70	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2406,96	 	MCP3327G	 	
W3416	MONOCRYL	MONOCRYL	1	2/0 	70	TE	30	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	2377,92	 	 	 	
W3435	MONOCRYL	MONOCRYL	1	4/0	70	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2322,80	 	MCP3435G	 	
W3437	MONOCRYL	MONOCRYL	1	3/0 	70	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2322,80	 	MCP215H	 	
W3439	MONOCRYL	MONOCRYL	0	3/0 	70	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	3535,03	 	 	 	
W3440	MONOCRYL	MONOCRYL	1	2/0 	70	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2642,88	 	 	 	
W3442	MONOCRYL	MONOCRYL	1	0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2368,78	 	MCP247H	 	
W3447	MONOCRYL	MONOCRYL	1	3/0 	70	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2368,78	 	MCP4160H	 	
W3448	MONOCRYL	MONOCRYL	1	2/0 	70	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2368,78	 	MCP4170H	 	
W3457	MONOCRYL	MONOCRYL	1	1	70	MH	36	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2764,14	 	MCP229H	 	
W3488	MONOCRYL	MONOCRYL	1	2/0 	70	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2507,74	 	MCP3488G	 	
W3489	MONOCRYL	MONOCRYL	1	0	70	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2717,10	 	MCP3489G	 	
W3548	MONOCRYL	MONOCRYL	1	4/0	70	BB	17	3/8    	Колющая	 	1	Ethalloy	0	1	12	2411,70	 	MCP3548H	 	
W3552	MONOCRYL	MONOCRYL	0	6/0	45	S-14	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	9646,84	 	 	 	
W3604	MONOCRYL	MONOCRYL	1	2/0 	70	UR-5	36	5/8   	Колющая	 	1	Нержавеющая сталь	0	2	12	3507,79	 	 	 	
W3627	MONOCRYL	MONOCRYL	1	3/0 	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	2	12	4298,89	 	 	 	
W3637	MONOCRYL	MONOCRYL	1	3/0 	70	UR-6	26	5/8   	Колющая	 	1	Нержавеющая сталь	0	2	12	4384,73	 	 	 	
W3650	MONOCRYL	MONOCRYL	0	3/0 	70	KS	60	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	2595,89	 	 	 	
W3653	MONOCRYL	MONOCRYL	0	3/0 	70	KS	60	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	5208,598217	 	 	 	
W3660	MONOCRYL	MONOCRYL	1	4/0	70	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2278,41	 	MCP218H	 	
W3661	MONOCRYL	MONOCRYL	1	3/0 	70	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2278,41	 	MCP219G	 	
W3662	MONOCRYL	MONOCRYL	1	2/0 	70	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2411,70	 	MCP220H	 	
W3664	MONOCRYL	MONOCRYL	1	3/0 	70	JB VB	26	1/2	Колющая	 	1	Нержавеющая сталь	1	1	12	2766,97	 	 	 	
W3665	MONOCRYL	MONOCRYL	1	2/0 	70	JB VB	26	1/2	Колющая	 	1	Нержавеющая сталь	1	1	12	2849,98	 	 	 	
W3727	MONOCRYL	MONOCRYL	1	1	90	MO-45	45	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2766,97	 	 	 	
W3758	MONOCRYL	MONOCRYL	1	0	70	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2454,62	 	MCP4958H	 	
W3770	MONOCRYL	MONOCRYL	1	1	90	CP	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2849,98	 	 	 	
W9000	VICRYL	VICRYL	1	0	250	-	-	-	-	 	1	-	-	-	12	3252,24	Катушка LIGAPAK	 	 	
W9001	VICRYL	VICRYL	1	1	250	-	-	-	-	 	1	-	-	-	12	3636,78	Катушка LIGAPAK	 	 	
W9020	VICRYL	VICRYL	1	2/0	250	-	-	-	-	 	1	-	-	-	12	3025,00	Катушка LIGAPAK	 	 	
W9023	VICRYL	VICRYL	1	4/0	150	-	-	-	-	 	1	-	-	-	12	1676,79	 	 	 	
W9024	VICRYL	VICRYL	1	3/0	150	-	-	-	-	 	1	-	-	-	12	1876,57	 	 	 	
W9025	VICRYL	VICRYL	1	2/0	150	-	-	-	-	 	1	-	-	-	12	1863,49	 	 	 	
W9026	VICRYL	VICRYL	1	0	150	-	-	-	-	 	1	-	-	-	12	2047,89	 	 	 	
W9027	VICRYL	VICRYL	1	1	150	-	-	-	-	 	1	-	-	-	12	2323,94	 	 	 	
W9028	VICRYL	VICRYL	1	2	150	-	-	-	-	 	1	-	-	-	12	3306,16	 	 	 	
W9030	VICRYL	VICRYL	1	3/0	250	-	-	-	-	 	1	-	-	-	12	2980,18	Катушка LIGAPAK	 	 	
W9067	VICRYL	VICRYL	1	4/0	60	C-1	13	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	2429,65	 	VCP9067H	 	
W9073H	PDS	PDS II	1	5/0	70	BB	17	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	36	7587,56	 	PDP9073H	 	
W9074	VICRYL	VICRYL	1	4/0	45	BB  PLUS	17	3/8    	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	2429,82	 	VCP9074H	 	
W9077H	PDS	PDS II	1	4/0	70	BB	17	3/8    	Колющая	 	1	Ethalloy	0	1	36	7587,56	 	PDP9077H	 	
W9095T	PDS	PDS II	1	7/0	45	C-1	13	3/8    	Колющая	 	1	Ethalloy	0	2	24	11619,87	 	 	 	
W9096H	PDS	PDS II	1	6/0	45	C-1	13	3/8    	Колющая	 	1	Ethalloy	0	2	36	15828,38	 	 	 	
W9100H	PDS	PDS II	1	6/0	45	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	1	36	8331,20	 	PDP9100H	 	
W9101H	PDS	PDS II	1	5/0	45	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	1	36	7849,60	 	PDP9101H	 	
W9102H	PDS	PDS II	1	4/0	45	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	1	36	7587,56	 	PDP9102H	 	
W9105	VICRYL	VICRYL	1	5/0	75	RB-1  PLUS	17	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	2069,95	 	VCP303H	 	
W9106	VICRYL	VICRYL	1	4/0	75	RB-1  PLUS	17	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1944,95	 	VCP214H	VCP304H	
W9108H	PDS	PDS II	1	5/0	90	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	9865,97	 	PDP9108H	 	
W9109H	PDS	PDS II	1	4/0	90	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	9865,97	 	PDP9109H	 	
W9113	VICRYL	VICRYL	1	4/0	75	SH-2    PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1779,43	 	 	 	
W9114	VICRYL	VICRYL	1	3/0	75	SH-2  PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1802,69	 	 	 	
W9115H	PDS	PDS II	1	4/0	70	SH-2  PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7148,87	 	PDP9115H	 	
W9116H	PDS	PDS II	1	3/0	70	SH-2  PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7148,87	 	PDP9116H	 	
W9118	VICRYL	VICRYL	1	3/0	75	CC-20	20	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	1	12	2689,00	 	 	 	
W9120	VICRYL	VICRYL	1	3/0	75	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1762,15	 	VCP316H	 	
W9121	VICRYL	VICRYL	1	2/0	75	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1840,83	 	VCP317H	 	
W9122	VICRYL	VICRYL	1	3/0	70	JB	26	1/2	Колющая	 	1	Нержавеющая сталь	1	1	12	2566,25	 	 	 	
W9124H	PDS	PDS II	1	3/0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6839,36	 	PDP9124H	 	
W9125H	PDS	PDS II	1	2/0	70	SH	26	1/2	Колющая	 	1	Ethalloy	0	1	36	6358,18	 	 	 	
W9130	VICRYL	VICRYL	1	3/0	75	MH-1  PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1762,15	 	VCP319H	 	
W9131H	PDS	PDS II	1	4/0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7620,97	 	PDP9131H	 	
W9132H	PDS	PDS II	1	3/0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7408,22	 	PDP9132H	 	
W9133H	PDS	PDS II	1	2/0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7512,80	 	PDP9133H	 	
W9134H	PDS	PDS II	1	3/0	90	MH1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	9470,62	 	PDP9134H	 	
W9136	VICRYL	VICRYL	1	2/0	75	MH-1  PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1813,98	 	VCP320H	 	
W9138	VICRYL	VICRYL	1	0	75	MH-1  PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1876,57	 	VCP247H	 	
W9140	VICRYL	VICRYL	1	2/0	75	MH  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1834,84	 	VCP323H	 	
W9141	VICRYL	VICRYL	1	0	75	MH  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1876,57	 	VCP324H	 	
W9150	VICRYL	VICRYL	1	2/0	75	LH	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1822,96	 	 	 	
W9151T	PDS	PDS II	1	2/0	70	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	4937,34	 	PDP9151H	 	
W9152T	PDS	PDS II	1	3/0	70	LH	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	4854,00	 	PDP9152H	 	
W9154	VICRYL	VICRYL	1	0	75	LH	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1904,42	 	 	 	
W9158	VICRYL	VICRYL	1	2/0	75	TP1	65	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	3094,87	 	 	 	
W9165	VICRYL	VICRYL	1	0	75	UR-4	40	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	12	2304,39	 	 	 	
W9168	PDS	PDS II	1	0	150	MF-110 HVY	110	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	12	4428,07	Нить в виде петли	 	 	
W9179H	PDS	PDS II	1	3/0	70	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8514,45	 	PDP9179H	 	
W9180	VICRYL	VICRYL	1	3/0	75	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2358,89	 	VCP998H	 	
W9181	VICRYL	VICRYL	1	2/0	75	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2403,71	 	VCP999H	 	
W9184H	PDS	PDS II	1	2/0	70	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8217,96	 	PDP9184H	 	
W9201H	PDS	PDS II	1	5/0	70	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	2	36	11400,73	 	PDP9201H	 	
W9205H	PDS	PDS II	1	0	70	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7663,04	 	 	 	
W9210H	PDS	PDS II	1	0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7663,04	 	 	 	
W9211H	PDS	PDS II	1	1	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8464,51	 	PDP9211H	 	
W9213	VICRYL	VICRYL	1	1	75	MH-1  PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	2047,89	 	VCP9213H	 	
W9215	VICRYL	VICRYL	1	0	75	MO-5	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2217,01	 	 	 	
W9216	VICRYL	VICRYL	1	1	75	MO-5	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2403,71	 	 	 	
W9219T	PDS	PDS II	1	2/0	70	ASH-35	36	Игла-крючок	Колющая	 	1	Нержавеющая сталь	0	1	24	5743,28	 	 	 	
W9230	VICRYL	VICRYL	1	0	75	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1690,82	 	VCP352H	 	
W9231	VICRYL	VICRYL	1	1	75	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2070,84	 	VCP353H	 	
W9234T	PDS	PDS II	1	1	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	5876,57	 	PDP9234H	 	
W9235T	PDS	PDS II	1	2	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	6138,63	 	 	 	
W9236T	PDS	PDS II	1	0	150	MO-2	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	7101,83	Нить в виде петли	PDP9236T	 	
W9237T	PDS	PDS II	1	1	150	MO-2	49	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	7587,56	Нить в виде петли	PDP9237T	 	
W9244	VICRYL	VICRYL	1	0	75	MO-45	45	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2454,94	 	 	 	
W9245	VICRYL	VICRYL	1	1	75	MO-45	45	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2496,69	 	VCP9245H	 	
W9246	VICRYL	VICRYL	1	2	75	MO-45	45	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2538,45	 	VCP9246H	 	
W9250	VICRYL	VICRYL	1	0	75	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1740,55	 	VCP370H	 	
W9251	VICRYL	VICRYL	1	1	75	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1939,97	 	VCP371H	 	
W9252	VICRYL	VICRYL	1	2	75	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2387,90	 	VCP372H	 	
W9256T	PDS	PDS II	1	2	90	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	6358,18	 	 	 	
W9261T	PDS	PDS II	1	0	150	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	7320,96	Нить в виде петли	PDP9261T	 	
W9262T	PDS	PDS II	1	1	150	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	7368,42	Нить в виде петли	PDP9262T	 	
W9282	VICRYL	VICRYL	1	2/0	75	LS-1	45	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2195,60	 	 	 	
W9287	VICRYL	VICRYL	1	2	75	OS-4	22	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2792,18	 	 	 	
W9289	VICRYL	VICRYL	1	1	100	MO-80	80	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	3688,78	 	VCP9289G	 	
W9295	VICRYL	VICRYL	1	0	75	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2131,75	 	VCP9295H	 	
W9296	VICRYL	VICRYL	1	1	75	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2257,55	 	VCP1058H	 	
W9297	VICRYL	VICRYL	1	2	75	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2501,11	 	VCP1059H	 	
W9299T	PDS	PDS II	1	1	90	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	24	5700,35	 	PDP9299H	 	
W9320	VICRYL	VICRYL	1	0	75	CP	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2070,84	 	VCP479H	 	
W9321	VICRYL	VICRYL	1	1	75	CP	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2429,65	 	VCP486H	 	
W9340	VICRYL	VICRYL	1	1	75	TR-40	40	Сложный изгиб	Троакарная	 	1	Нержавеющая сталь	0	1	12	2944,31	 	 	 	
W9341	VICRYL	VICRYL	1	2/0	75	SKI-34	34	Лыжеобразная	Колющая	 	1	Нержавеющая сталь	0	1	12	2730,75	 	 	 	
W9350	VICRYL	VICRYL	1	2/0	75	V-7	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2484,79	 	VCP999H	 	
W9352T	PDS	PDS II	1	1	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	6577,30	 	 	 	
W9355T	PDS	PDS II	1	0	90	V-35	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	5876,57	 	 	 	
W9357	VICRYL	VICRYL	1	2	75	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2557,87	 	VCP520H	 	
W9360	VICRYL	VICRYL	1	2/0	75	V-30	31	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2047,89	 	VCP9360H	 	
W9361	VICRYL	VICRYL	1	0	75	V-30	31	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2173,48	 	VCP9361H	 	
W9362	VICRYL	VICRYL	1	1	75	V-30	31	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	4482,35	 	VCP9362H	 	
W9363	VICRYL	VICRYL	1	2/0	75	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2277,81	 	VCP517H	 	
W9364	VICRYL	VICRYL	1	0	75	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	1992,92	 	VCP518H	 	
W9365	VICRYL	VICRYL	1	1	75	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2403,71	 	VCP519H	 	
W9366T	PDS	PDS II	1	0	90	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	6089,24	 	PDP9366H	 	
W9368	VICRYL	VICRYL	1	1	75	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2428,26	 	VCP9468H	 	
W9369	VICRYL	VICRYL	1	0	75	V-80	80	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	3282,76	 	 	 	
W9370T	PDS	PDS II	1	1	90	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	5919,49	 	PDP9370H	 	
W9372	VICRYL	VICRYL	1	0	75	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2428,26	 	 	 	
W9373	VICRYL	VICRYL	1	1	75	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2129,35	 	VCP9468H	 	
W9375	VICRYL	VICRYL	1	2/0	75	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2475,81	 	VCP9375H	 	
W9376	VICRYL	VICRYL	1	0	75	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	1992,92	 	VCP9376H	 	
W9377	VICRYL	VICRYL	1	1	75	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2358,89	 	VCP9377H	 	
W9378	VICRYL	VICRYL	1	2	75	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2444,25	 	VCP9378H	 	
W9379	VICRYL	VICRYL	1	1	75	V-80	80	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	3282,76	 	 	 	
W9380H	PDS	PDS II	1	2/0	70	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7408,22	 	PDP9380H	 	
W9381H	PDS	PDS II	1	0	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7892,94	 	PDP9355H	PDP9381H	
W9384T	PDS	PDS II	1	0	90	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	5438,31	 	PDP9384H	 	
W9385T	PDS	PDS II	1	1	90	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	6663,15	 	PDP9385H	 	
W9386	VICRYL	VICRYL	1	4/0	45	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1931,94	 	VCP397H	 	
W9388	VICRYL	VICRYL	1	3/0	45	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1767,83	 	VCP452H	 	
W9390	VICRYL	VICRYL	1	2/0	75	FSLX	36	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1876,57	 	 	 	
W9391	VICRYL	VICRYL	1	1	100	BP-5	65	3/8    	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3667,72	 	 	 	
W9396T	PDS	PDS II	1	1	90	V-40	48	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	6491,47	 	PDP9396H	 	
W9410	VICRYL	VICRYL	1	4/0	45	SC-1	13	Прямая	Режущая	 	1	Ethalloy	0	1	12	3118,15	 	 	 	
W9415	VICRYL	VICRYL	1	3/0	75	KS	60	Прямая	Режущая	 	1	Ethalloy	0	1	12	2132,96	 	 	 	
W9420	VICRYL	VICRYL	1	0	90	CP	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2474,41	 	 	 	
W9421	VICRYL	VICRYL	1	1	90	CP	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2174,28	 	VCP486H	 	
W9430	VICRYL	VICRYL	1	0	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2217,01	 	VCP358H	 	
W9431	VICRYL	VICRYL	1	1	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2403,71	 	VCP359H	 	
W9436	VICRYL	VICRYL	0	5/0	45	PS-4C PRIME	16	Сложный изгиб	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	12	2986,07	 	 	 	
W9440	VICRYL	VICRYL	1	2/0	90	MH  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	1866,06	 	VCP323H	 	
W9441	VICRYL	VICRYL	1	0	90	MH  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	12	2298,08	 	VCP324H	 	
W9442	VICRYL	VICRYL	1	5/0	75	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	2132,96	 	 	 	
W9443	VICRYL	VICRYL	1	4/0	75	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	1932,86	 	 	 	
W9444	VICRYL	VICRYL	1	3/0	75	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	1932,86	 	 	 	
W9450	VICRYL	VICRYL	1	0	90	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2131,65	 	VCP370H	 	
W9451	VICRYL	VICRYL	1	1	90	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2428,26	 	VCP371H	 	
W9452	VICRYL	VICRYL	1	2	90	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2528,17	 	VCP372H	 	
W9463	VICRYL	VICRYL	1	2/0	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2209,59	 	VCP517H	 	
W9464	VICRYL	VICRYL	1	0	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2720,98	 	VCP518H	 	
W9465	VICRYL	VICRYL	1	1	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2858,57	 	VCP519H	 	
W9466	VICRYL	VICRYL	1	2/0	90	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2240,49	 	 	 	
W9467	VICRYL	VICRYL	1	0	90	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2282,22	 	 	 	
W9468	VICRYL	VICRYL	1	1	90	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2246,91	 	VCP9468H	 	
W9496	VICRYL	VICRYL	1	1	90	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2403,71	 	VCP1058H	 	
W9497	VICRYL	VICRYL	1	2	90	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2814,66	 	VCP1059H	 	
W9500T	VICRYL	VICRYL	0	6/0	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5716,82	 	 	 	
W9501T	VICRYL	VICRYL	0	5/0	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5092,73	 	 	 	
W9505T	VICRYL	VICRYL	0	5/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5118,84	 	 	 	
W9506T	VICRYL	VICRYL	0	4/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5118,65	 	 	 	
W9507T	VICRYL	VICRYL	0	3/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5269,39	 	VCP9507H	 	
W9510T	VICRYL	VICRYL	0	4/0	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4610,86	 	 	 	
W9511T	VICRYL	VICRYL	0	3/0	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4610,86	 	 	 	
W9514T	VICRYL	VICRYL	0	5/0	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5202,56	 	 	 	
W9515T	VICRYL	VICRYL	0	4/0	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5151,64	 	VCP496H	 	
W9516T	VICRYL	VICRYL	0	3/0	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5151,55	 	VCP497H	 	
W9520T	VICRYL	VICRYL	0	4/0	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5312,59	 	 	 	
W9521T	VICRYL	VICRYL	0	3/0	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5290,47	 	 	 	
W9522T	VICRYL	VICRYL	0	2/0	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4916,26	 	 	 	
W9525T	VICRYL	VICRYL	0	3/0	45	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	4236,02	 	VCP683H	 	
W9526T	VICRYL	VICRYL	0	3/0	75	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5118,65	 	VCP683H	 	
W9527T	VICRYL	VICRYL	0	2/0	75	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5545,40	 	VCP684H	 	
W9531T	VICRYL	VICRYL	0	3/0	75	PCLX  PRIME	36	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5622,48	 	VCP9531H	 	
W9532T	VICRYL	VICRYL	0	2/0	75	PCLX  PRIME	36	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5355,67	 	VCP9532H	 	
W9545	VICRYL	VICRYL	1	8/0	30	G-7  PRIME	8	1/2	Обратно-режущая PRIME	 	1	Ethalloy	0	2	12	9746,56	 	 	 	
W9552	VICRYL	VICRYL	0	6/0	45	S-24	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7770,72	 	 	 	
W9553	VICRYL	VICRYL	0	5/0	45	S-24	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7684,35	 	 	 	
W9559	VICRYL	VICRYL	1	8/0	45	TG140-8	6,5	3/8    	Шпательная	 	1	Ethalloy	0	2	12	8397,00	 	 	 	
W9560	VICRYL	VICRYL	1	8/0	30	TG140-8	6,5	3/8    	Шпательная	 	1	Ethalloy	0	2	12	8183,55	 	 	 	
W9561	VICRYL	VICRYL	1	7/0	30	TG140-8	6,5	3/8    	Шпательная	 	1	Ethalloy	0	2	12	7947,19	 	 	 	
W9564	VICRYL	VICRYL	1	8/0	30	TG175-8	7	1/2	Шпательная	 	1	Ethalloy	0	2	12	8526,60	 	 	 	
W9570T	VICRYL	VICRYL	0	4/0	75	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5503,66	 	 	 	
W9571T	VICRYL	VICRYL	0	3/0	75	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5461,84	 	VCP9571H	 	
W9574	PDS	PDS II	1	7/0	23	BV175-6	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	6839,36	 	 	 	
W9581T	VICRYL	VICRYL	0	3/0	75	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5503,66	 	 	 	
W9582T	VICRYL	VICRYL	0	2/0	75	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5545,25	 	VCP9582H	 	
W9615T	PDS	PDS II	0	4/0	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	7107,73	 	 	 	
W9625T	PDS	PDS II	0	3/0	70	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	6663,15	 	PDP9625H	 	
W9636	VICRYL	VICRYL	1	2/0	75	MH1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3178,37	 	 	 	
W9713	VICRYL	VICRYL	0	2/0	75	KS-55	55	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	2281,66	 	 	 	
W9714T	PDS	PDS II	0	3/0	70	KS	60	3/8    	Режущая	 	1	Ethalloy	0	1	24	5175,84	 	PDP9714H	 	
W9718	VICRYL	VICRYL	0	2/0	75	KS	60	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	2174,72	 	 	 	
W9730	VICRYL	VICRYL	0	3/0	75	UX-25	26	5/8   	Режущая	 	1	Ethalloy	0	1	12	2195,70	 	 	 	
W9733T	PDS	PDS II	0	5/0	70	PC-3	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	24	5261,68	 	 	 	
W9734T	PDS	PDS II	0	4/0	70	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	24	5175,84	 	PDP9734H	 	
W9740T	PDS	PDS II	0	3/0	45	X-1	22	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	24	5218,76	 	PDP9740H	 	
W9741	VICRYL	VICRYL	0	3/0	45	X-1 CONV	22	1/2	Режущая	 	1	Нержавеющая сталь	0	1	12	1897,44	 	 	 	
W9752	VICRYL	VICRYL	0	6/0	45	S-24	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7770,72	 	 	 	
W9753	VICRYL	VICRYL	0	5/0	45	S-24	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7684,35	 	 	 	
W9761	VICRYL	VICRYL	0	5/0	45	SPAT-10	11	3/8    	Шпательная	 	1	Ethalloy	0	2	12	7684,35	 	 	 	
W9762	VICRYL	VICRYL	0	4/0	45	SPAT-10	11	3/8    	Шпательная	 	1	Ethalloy	0	2	12	7460,53	 	 	 	
W9820	VICRYL	VICRYL	0	4/0	75	UV-17	17	5/8   	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2793,78	 	 	 	
W9824	VICRYL	VICRYL	0	5/0	75	V-4	17	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2313,28	 	VCP240H	 	
W9826	VICRYL	VICRYL	0	3/0	75	V-4	17	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2303,08	 	VCP9826H	 	
W9828	VICRYL	VICRYL	1	2/0	75	V-6	22	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2517,57	 	VCP278H	 	
W9831T	VICRYL	VICRYL	0	6/0	45	PC-1 PRIME	13	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	6315,01	 	 	 	
W9832T	VICRYL	VICRYL	0	5/0	45	PC-1 PRIME	13	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	6233,77	 	 	 	
W9834T	VICRYL	VICRYL	0	4/0	45	PC-3 PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	6055,67	 	 	 	
W9860H	PDS	PDS II	1	6/0	45	P-6 PRIME	8	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	36	13679,14	 	 	 	
W9861T	PDS	PDS II	0	6/0	45	PC-1 PRIME	13	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	7892,94	 	PDP9861H	 	
W9867T	PDS	PDS II	0	4/0	45	PC-3 PRIME	16	3/8    	Режущая PRIME	 	1	Нержавеющая сталь	0	1	24	7235,13	 	 	 	
W9874T	PDS	PDS II	0	4/0	45	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	7237,14	 	 	 	
W9890	VICRYL	VICRYL	0	3/0	75	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1904,42	 	VCP442H	 	
W9900	VICRYL	VICRYL	0	2/0	75	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	1992,92	 	VCP9900H	 	
W9901	VICRYL	VICRYL	0	0	75	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2028,63	 	VCP9901H	 	
W9906	VICRYL	VICRYL	0	1	75	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2484,79	 	 	 	
W9950T	PDS	PDS II	0	4/0	45	FS-2	19	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	24	5128,38	 	PDP9950H	 	
W9951	VICRYL	VICRYL	0	4/0	45	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1739,35	 	VCP422H	 	
W9957T	PDS	PDS II	0	3/0	45	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	24	5066,76	 	PDP9957H	 	
W9967T	PDS	PDS II	1	1	150	CTXB ETHIGUARD	48	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	24	9294,40	Игла ETHIGUARD. Нить в виде петли	PDP9967T	 	
W9981	VICRYL	VICRYL	0	6/0	45	RB-2	13	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2768,09	 	vcp433h	 	
W9982	VICRYL	VICRYL	0	5/0	45	RB-2	13	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2143,73	 	VCP9982H	 	
W9984	VICRYL	VICRYL	1	2/0	75	MHB-1 ETHIGUARD	31	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	2651,21	Игла ETHIGUARD	 	 	
W9986	VICRYL	VICRYL	1	0	75	CTB-1 ETHIGUARD	36	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	2984,37	Игла ETHIGUARD	 	 	
W9989	VICRYL	VICRYL	1	1	75	CTB ETHIGUARD	40	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	2980,18	Игла ETHIGUARD	 	 	
W9991	VICRYL	VICRYL	1	1	90	CTXB ETHIGUARD	48	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3542,10	Игла ETHIGUARD	 	 	
W9994	VICRYL	VICRYL	1	0	90	CTB-1 ETHIGUARD	36	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3512,80	Игла ETHIGUARD	 	 	
W9995	VICRYL	VICRYL	1	1	90	CTB-1 ETHIGUARD	36	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3646,28	Игла ETHIGUARD	 	 	
W9997	VICRYL	VICRYL	1	1	90	CTB ETHIGUARD	40	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3542,10	Игла ETHIGUARD	 	 	
W9998	VICRYL	VICRYL	1	1	90	BT-3 ETHIGUARD	45	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3370,67	Игла ETHIGUARD	 	 	
W9999	VICRYL	VICRYL	1	2	90	BT-3 ETHIGUARD	45	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	3689,27	Игла ETHIGUARD	 	 	
Y864G	MONOCRYL	MONOCRYL	1	3/0	45	SH  PLUS	26	1/2	Колющая PLUS	 	8	Нержавеющая сталь	0	1	12	17528,93	 	 	 	
Y3864G	MONOCRYL	MONOCRYL	1	3/0	45	JB VB	26	1/2	Колющая	 	8	Нержавеющая сталь	1	1	12	17528,93	 	 	 	
Y7840G	MONOCRYL	MONOCRYL	1	3/0	70	JB VB	26	1/2	Колющая	 	8	Нержавеющая сталь	1	1	12	18029,76	 	 	 	
Z1002H	PDS	PDS II	1	6/0	70	BV	11	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	20608,87	 	PDP1002H	 	
Z1032H	PDS	PDS II	1	6/0	60	CC-1	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	18063,86	 	PDP1032H	 	
Z1311H	PDS	PDS II	1	3/0	70	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	16068,34	 	PDP1311H	 	
Z1370E	PDS	PDS II	 	7/0	70	TF-6	6,5	1/2	Колющая	 	1	Нержавеющая сталь	0	2	24	23463,72	 	 	 	
Z1701E	PDS	PDS II	1	7/0	70	BV-1	9,3	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	24	16603,00	 	 	 	
Z1702H	PDS	PDS II	1	6/0	70	BV-1	9,3	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	21485,82	 	PDP1702H	 	
Z1711E	PDS	PDS II	1	7/0	70	CC	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	24	17101,08	 	 	 	
Z1712H	PDS	PDS II	1	6/0	70	CC	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	21924,51	 	PDP1712H	 	
E9103S	PDS	PDS II	1	3/0	20	SKI-22	22	Лыжеобразная	Колющая	 	1	Нержавеющая сталь	0	1	24	11400,73	 	 	 	
E9902S	VICRYL	VICRYL	1	2/0	20	SKI-22	22	Лыжеобразная	Колющая	 	1	Нержавеющая сталь	0	1	24	7331,41	 	 	 	
E9903S	VICRYL	VICRYL	1	3/0	20	SKI-22	22	Лыжеобразная	Колющая	 	1	Нержавеющая сталь	0	1	24	7331,41	 	 	 	
E9904S	VICRYL	VICRYL	1	4/0	20	SKI-22	22	Лыжеобразная	Колющая	 	1	Нержавеющая сталь	0	1	24	7331,41	 	 	 	
PDP9355H	PDS PLUS	PDS PLUS	1	0	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	9981,53	 	W9381H	 	
PDP9381H	PDS PLUS	PDS PLUS	1	0	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8937,59	 	W9381H	 	
PDP9366H	PDS PLUS	PDS PLUS	1	0	90	V-37  	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	9959,96	 	W9366T	 	
PDP9384H	PDS PLUS	PDS PLUS	1	0	90	V-39  	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	9237,16	 	W9384T	 	
PDP9236T	PDS PLUS	PDS PLUS	1	0	150	MO-2  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	8041,78	Нить в виде петли	W9236T	 	
PDP9261T	PDS PLUS	PDS PLUS	1	0	150	CTX  	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	8289,91	Нить в виде петли	W9261T	 	
PDP9967T	PDS PLUS	PDS PLUS	1	1	150	CTXB  	48	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	24	10524,54	Нить в виде петли	W9967T	 	
PDP9299H	PDS PLUS	PDS PLUS	1	1	90	CPX  	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	9682,22	 	W9299T	 	
PDP9352H	PDS PLUS	PDS PLUS	1	1	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	11171,74	 	 	 	
PDP9370H	PDS PLUS	PDS PLUS	1	1	90	V-37  	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	10054,43	 	W9370T	 	
PDP9385H	PDS PLUS	PDS PLUS	1	1	90	V-39  	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	11317,55	 	W9385T	 	
PDP9396H	PDS PLUS	PDS PLUS	1	1	90	V-40  	48	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	11025,98	 	W9396T	 	
PDP9211H	PDS PLUS	PDS PLUS	1	1	70	MH-1  	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	9584,80	 	W9211H	 	
PDP9237T	PDS PLUS	PDS PLUS	1	1	150	MO-2  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	8591,79	Нить в виде петли	W9237T	 	
PDP9234H	PDS PLUS	PDS PLUS	1	1	90	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	9981,53	 	W9234T	 	
PDP9262T	PDS PLUS	PDS PLUS	1	1	150	CTX  	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	8343,66	Нить в виде петли	W9262T	 	
PDP9184H	PDS PLUS	PDS PLUS	1	2/0	70	V-7  	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	9305,63	 	W9184H	 	
PDP9380H	PDS PLUS	PDS PLUS	1	2/0	70	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8388,73	 	W9380H	 	
PDP9133H	PDS PLUS	PDS PLUS	1	2/0	70	MH-1  	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8507,14	 	W9133H	 	
PDP9151H	PDS PLUS	PDS PLUS	1	2/0	70	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8386,23	 	W9151T	 	
PDP9740H	PDS PLUS	PDS PLUS	0	3/0	45	X-1 CONV	22	1/2	Режущая	 	1	Нержавеющая сталь	0	1	36	8864,22	 	W9740T	 	
PDP9625H	PDS PLUS	PDS PLUS	0	3/0	70	PC-25	26	3/8    	Режущая	 	1	Ethalloy	0	1	36	11317,55	 	W9625T	 	
PDP9957H	PDS PLUS	PDS PLUS	0	3/0	45	FS  	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	8606,04	 	W9957T	 	
PDP9714H	PDS PLUS	PDS PLUS	0	3/0	70	KS  	60	Прямая	Режущая	 	1	Ethalloy	0	1	36	8791,31	 	W9714T	 	
PDP9179H	PDS PLUS	PDS PLUS	1	3/0	70	V-7  	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	9641,36	 	W9179H	 	
PDP1311H	PDS PLUS	PDS PLUS	1	3/0	70	SH-1  	22	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	17675,17	 	Z1311H	 	
PDP9132H	PDS PLUS	PDS PLUS	1	3/0	70	MH-1  	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8388,73	 	W9132H	 	
PDP9134H	PDS PLUS	PDS PLUS	1	3/0	90	MH-1  	31	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	11039,49	 	W9134H	 	
PDP9152H	PDS PLUS	PDS PLUS	1	3/0	70	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8244,67	 	W9152T	 	
PDP9116H	PDS PLUS	PDS PLUS	1	3/0	70	SH-2 PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	8095,04	 	W9116H	 	
PDP9124H	PDS PLUS	PDS PLUS	1	3/0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7972,36	 	W9124H	 	
PDP9734H	PDS PLUS	PDS PLUS	0	4/0	70	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	36	8791,31	 	W9734T	 	
PDP9950H	PDS PLUS	PDS PLUS	0	4/0	45	FS-2  	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	8710,72	 	W9950T	 	
PDP9102H	PDS PLUS	PDS PLUS	1	4/0	45	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	1	36	8591,79	 	W9102H	 	
PDP9109H	PDS PLUS	PDS PLUS	1	4/0	90	RB-1  	17	1/2	Колющая	 	1	Ethalloy	0	2	36	11500,33	 	W9109H	 	
PDP2993H	PDS PLUS	PDS PLUS	1	4/0	90	SH-1  	22	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	20246,37	 	PEE2993H	 	
PDP9131H	PDS PLUS	PDS PLUS	1	4/0	70	MH-1  	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8883,45	 	W9131H	 	
PDP9077H	PDS PLUS	PDS PLUS	1	4/0	70	BB  	17	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	36	8591,79	 	W9077H	 	
PDP9115H	PDS PLUS	PDS PLUS	1	4/0	70	SH-2 PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	8095,04	 	W9115H	 	
PDP9733H	PDS PLUS	PDS PLUS	0	5/0	70	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	36	8937,12	 	 	 	
PDP9201H	PDS PLUS	PDS PLUS	1	5/0	70	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	2	36	13289,34	 	W9201H	 	
PDP9101H	PDS PLUS	PDS PLUS	1	5/0	45	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	1	36	8888,52	 	W9101H	 	
PDP9108H	PDS PLUS	PDS PLUS	1	5/0	90	RB-1  	17	1/2	Колющая	 	1	Ethalloy	0	2	36	11500,33	 	W9108H	 	
PDP9073H	PDS PLUS	PDS PLUS	1	5/0	70	BB  	17	3/8    	Колющая	 	1	Ethalloy	0	1	36	8591,79	 	W9073H	 	
PDP9861H	PDS PLUS	PDS PLUS	0	6/0	45	PC-1    PRIME	13	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	13406,41	 	W9861T	 	
PDP1712H	PDS PLUS	PDS PLUS	1	6/0	70	CC  	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	24826,27	 	Z1712H	 	
PDP1032H	PDS PLUS	PDS PLUS	1	6/0	70	CC-1 MP	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	20454,67	 	Z1032H	 	
PDP9100H	PDS PLUS	PDS PLUS	1	6/0	45	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	1	36	9433,86	 	W9100H	 	
PDP1702H	PDS PLUS	PDS PLUS	1	6/0	70	BV-1  	9,3	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	24329,53	 	Z1702H	 	
PDP1002H	PDS PLUS	PDS PLUS	1	6/0	70	BV  	11	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	23336,51	 	Z1002H	 	
PDP1721H	PDS PLUS	PDS PLUS	1	6/0	70	C-1  	13	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	17923,32	 	 	 	
PDP528H	PDS PLUS	PDS PLUS	1	2/0	90	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	11170,85	 	 	 	
VCP497H	VICRYL PLUS	VICRYL PLUS	0	3/0	45	PS-2  	19	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	36	7701,21	 	W9516T	 	
VCP1036H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	TE PLUS	31	3/8    	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	8172,70	 	 	 	
VCP1058H	VICRYL PLUS	VICRYL PLUS	1	1	90	CPX  	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	7933,63	 	W9296	 	
VCP1059H	VICRYL PLUS	VICRYL PLUS	1	2	90	CPX  	49	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	9290,00	 	W9297	 	
VCP1214E	VICRYL PLUS	VICRYL PLUS	1	4/0	70	-	-	-	-	 	5	-	-	-	24	8229,74	 	 	 	
VCP1215E	VICRYL PLUS	VICRYL PLUS	1	3/0	70	-	-	-	-	 	5	-	-	-	24	8229,74	 	 	 	
VCP1216E	VICRYL PLUS	VICRYL PLUS	1	2/0	70	-	-	-	-	 	5	-	-	-	24	7990,04	 	 	 	
VCP1217E	VICRYL PLUS	VICRYL PLUS	1	0	70	-	-	-	-	 	5	-	-	-	24	9124,14	 	 	 	
VCP1218H	VICRYL PLUS	VICRYL PLUS	1	1	70	-	-	-	-	 	5	-	-	-	36	14578,51	 	 	 	
VCP1219H	VICRYL PLUS	VICRYL PLUS	1	2	70	-	-	-	-	 	5	-	-	-	36	18248,29	 	 	 	
VCP214H	VICRYL PLUS	VICRYL PLUS	0	4/0	70	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6730,46	 	W9106	 	
VCP240H	VICRYL PLUS	VICRYL PLUS	0	4/0	70	V-4	17	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7383,81	 	W9824	 	
VCP242H	VICRYL PLUS	VICRYL PLUS	1	3/0	90	SH PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6614,99	 	 	 	
VCP244H	VICRYL PLUS	VICRYL PLUS	1	0	90	SH PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	10840,28	 	V244H	 	
VCP247H	VICRYL PLUS	VICRYL PLUS	1	0	70	MH-1 PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5844,89	 	W9138	 	
VCP250H	VICRYL PLUS	VICRYL PLUS	1	1	70	MO-7	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8962,69	 	 	 	
VCP251H	VICRYL PLUS	VICRYL PLUS	1	2	70	MO-7	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	9160,37	 	 	 	
VCP2593H	VICRYL PLUS	VICRYL PLUS	1	2/0	90	UR-6  	26	5/8   	Колющая	 	1	Нержавеющая сталь	0	2	36	9920,71	 	 	 	
VCP278H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	V-6  	22	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8064,78	 	W9828	 	
VCP303H	VICRYL PLUS	VICRYL PLUS	1	5/0	70	RB-1  PLUS	17	1/2	Колющая PLUS	 	1	Ethalloy	0	1	36	7865,81	 	W9105	 	
VCP304H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	RB-1  PLUS	18	1/2	Колющая PLUS	 	1	Ethalloy	0	1	36	6660,62	 	W9106	 	
VCP305H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	RB-1  PLUS	19	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6615,04	 	 	 	
VCP306H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	RB-1  PLUS	20	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6552,64	 	 	 	
VCP308H	VICRYL PLUS	VICRYL PLUS	0	3/0	70	MF	31	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	36	8473,95	 	 	 	
VCP310H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	SH-1  PLUS	22	1/2	Колющая PLUS	 	1	Ethalloy	0	1	36	6393,10	 	 	 	
VCP3110H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	JB-1 VB  	22	1/2	Колющая	 	1	Нержавеющая сталь	1	1	36	6743,43	 	 	 	
VCP311H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	SH-1  PLUS	22	1/2	Колющая PLUS	 	1	Ethalloy	0	1	36	6224,28	 	 	 	
VCP312H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	SH-1  PLUS	22	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6343,38	 	 	 	
VCP315H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6283,54	 	 	 	
VCP316H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5705,33	 	W9120	 	
VCP317H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5902,19	 	W9121	 	
VCP318H	VICRYL PLUS	VICRYL PLUS	1	0	70	SH  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6092,44	 	V318H	 	
VCP319H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	5816,12	 	W9130	 	
VCP320H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	MH-1  PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5705,33	 	W9136	 	
VCP322H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	MH  PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6343,38	 	 	 	
VCP323H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	MH  PLUS	37	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5770,95	 	W9140	W9440	
VCP324H	VICRYL PLUS	VICRYL PLUS	1	0	70	MH  PLUS	38	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	5902,19	 	V324H	W9141	W9441
VCP325H	VICRYL PLUS	VICRYL PLUS	1	1	70	MH  PLUS	39	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7061,66	 	 	 	
VCP326H	VICRYL PLUS	VICRYL PLUS	1	2/0	90	CT-2  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7039,48	 	V326H	 	
VCP328H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	CT-3  	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6083,58	 	 	 	
VCP330H	VICRYL PLUS	VICRYL PLUS	1	0	90	CT-2  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6928,42	 	 	 	
VCP331H	VICRYL PLUS	VICRYL PLUS	1	1	90	CT-2  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7190,84	 	 	 	
VCP345H	VICRYL PLUS	VICRYL PLUS	1	2/0	90	CT-1  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6451,28	 	 	 	
VCP346H	VICRYL PLUS	VICRYL PLUS	1	0	90	CT-1  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6661,94	 	 	 	
VCP347H	VICRYL PLUS	VICRYL PLUS	1	1	90	CT-1  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7329,11	 	 	 	
VCP348H	VICRYL PLUS	VICRYL PLUS	1	2	90	CT-1  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8054,05	 	 	 	
VCP351H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6566,69	 	 	 	
VCP352H	VICRYL PLUS	VICRYL PLUS	1	0	70	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6253,44	 	W9230	 	
VCP353H	VICRYL PLUS	VICRYL PLUS	1	1	70	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6834,99	 	W9231	 	
VCP358H	VICRYL PLUS	VICRYL PLUS	1	0	90	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7178,04	 	W9430	 	
VCP359H	VICRYL PLUS	VICRYL PLUS	1	1	90	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7782,52	 	W9431	 	
VCP360H	VICRYL PLUS	VICRYL PLUS	1	2	90	CT  	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8261,57	 	 	 	
VCP370H	VICRYL PLUS	VICRYL PLUS	1	0	90	CTX  	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	6834,65	 	W9250	W9450	
VCP371H	VICRYL PLUS	VICRYL PLUS	1	1	90	CTX  	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7563,24	 	W9251	W9451	
VCP372H	VICRYL PLUS	VICRYL PLUS	1	2	90	CTX  	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8402,36	 	W9252	W9452	
VCP376H	VICRYL PLUS	VICRYL PLUS	1	0	70	UR-5  	36	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	36	8075,05	 	V375H	 	
VCP395H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	FS-3  	16	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6020,24	 	 	 	
VCP397H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	FS-2  	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6018,60	 	W9386	 	
VCP422H	VICRYL PLUS	VICRYL PLUS	0	4/0	70	FS-2  	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6018,60	 	W9951	 	
VCP433H	VICRYL PLUS	VICRYL PLUS	0	5/0	70	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	1	36	8447,69	 	W9981	 	
VCP434H	VICRYL PLUS	VICRYL PLUS	0	4/0	70	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	1	36	8201,65	 	 	 	
VCP442H	VICRYL PLUS	VICRYL PLUS	0	3/0	70	FS-1  	24	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6195,61	 	W9890	 	
VCP452H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	FS-1  	24	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	5732,00	 	W9388	 	
VCP479H	VICRYL PLUS	VICRYL PLUS	1	0	70	CP  	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	7481,18	 	W9320	 	
VCP486H	VICRYL PLUS	VICRYL PLUS	1	1	90	CP  	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	7810,68	 	W9321	W9421	
VCP493H	VICRYL PLUS	VICRYL PLUS	0	5/0	45	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	8453,03	 	 	 	
VCP494H	VICRYL PLUS	VICRYL PLUS	0	4/0	45	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	8706,62	 	 	 	
VCP496H	VICRYL PLUS	VICRYL PLUS	0	4/0	45	PS-2  	19	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	36	7701,21	 	W9515T	 	
VCP500H	VICRYL PLUS	VICRYL PLUS	0	5/0	45	PS-3  	16	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	36	8211,52	 	 	 	
VCP516H	VICRYL PLUS	VICRYL PLUS	1	3/0	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7598,15	 	 	 	
VCP517H	VICRYL PLUS	VICRYL PLUS	1	2/0	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7711,30	 	W9363	W9463	
VCP518H	VICRYL PLUS	VICRYL PLUS	1	0	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8724,19	 	W9364	W9464	
VCP519H	VICRYL PLUS	VICRYL PLUS	1	1	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	9165,37	 	W9365	W9465	
VCP520H	VICRYL PLUS	VICRYL PLUS	1	2	90	V-34  	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7966,90	 	W9357	 	
VCP584G	VICRYL PLUS	VICRYL PLUS	1	1	120	XLH  	70	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	4945,10	 	 	 	
VCP585H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	FSL  	30	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	7061,66	 	 	 	
VCP586H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	FSL  	30	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6661,94	 	 	 	
VCP587H	VICRYL PLUS	VICRYL PLUS	1	0	70	FSL  	30	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	7082,10	 	 	 	
VCP602H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	UR-6  	26	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	36	7750,23	 	 	 	
VCP603H	VICRYL PLUS	VICRYL PLUS	1	0	70	UR-6  	26	5/8   	Колющая	 	1	Нержавеющая сталь	0	1	36	7978,06	 	 	 	
VCP624E	VICRYL PLUS	VICRYL PLUS	1	3/0	70	-	-	-	-	 	2	-	-	-	24	4081,09	 	 	 	
VCP625E	VICRYL PLUS	VICRYL PLUS	1	2/0	70	-	-	-	-	 	2	-	-	-	24	3849,10	 	 	 	
VCP626E	VICRYL PLUS	VICRYL PLUS	1	0	70	-	-	-	-	 	2	-	-	-	24	4298,08	 	 	 	
VCP627H	VICRYL PLUS	VICRYL PLUS	1	1	70	-	-	-	-	 	2	-	-	-	36	7238,31	 	 	 	
VCP628H	VICRYL PLUS	VICRYL PLUS	1	2	70	-	-	-	-	 	2	-	-	-	36	10297,60	 	 	 	
VCP683H	VICRYL PLUS	VICRYL PLUS	0	3/0	45	PS-1  	24	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	36	7259,97	 	W9525T	W9526T	
VCP684H	VICRYL PLUS	VICRYL PLUS	0	2/0	45	PS-1  	24	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	36	7259,97	 	W9527T	 	
VCP829E	VICRYL PLUS	VICRYL PLUS	1	1	150	V-40  	48	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	24	9984,39	 	 	 	
VCP9067H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	C-1  	13	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	36	8331,40	 	W9067	 	
VCP9074H	VICRYL PLUS	VICRYL PLUS	1	4/0	45	BB  PLUS	17	3/8    	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	8170,10	 	W9074	 	
VCP9213H	VICRYL PLUS	VICRYL PLUS	1	1	70	MH-1 PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6566,12	 	W9213	 	
VCP9221H	VICRYL PLUS	VICRYL PLUS	1	0	70	ASH-35  	36	Игла-крючок	Колющая	 	1	Нержавеющая сталь	0	1	36	8478,80	 	 	 	
VCP9245H	VICRYL PLUS	VICRYL PLUS	1	1	70	MO-45  	45	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8005,11	 	W9245	 	
VCP9246H	VICRYL PLUS	VICRYL PLUS	1	2	70	MO-45  	45	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8218,76	 	W9246	 	
VCP924H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	TF  PLUS	13	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7021,67	 	 	 	
VCP9289G	VICRYL PLUS	VICRYL PLUS	1	1	100	MO-80  	80	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	3880,40	 	W9289	 	
VCP9295H	VICRYL PLUS	VICRYL PLUS	1	0	70	CPX  	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6834,99	 	W9295	 	
VCP9360H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	V-30  	31	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	6566,12	 	W9360	 	
VCP9361H	VICRYL PLUS	VICRYL PLUS	1	0	70	V-30  	31	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	6968,78	 	W9361	 	
VCP9362H	VICRYL PLUS	VICRYL PLUS	1	1	70	V-30  	31	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	8259,11	 	W9362	 	
VCP9375H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	V-39  	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7938,17	 	W9375	 	
VCP9376H	VICRYL PLUS	VICRYL PLUS	1	0	70	V-39  	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7160,10	 	W9376	 	
VCP9377H	VICRYL PLUS	VICRYL PLUS	1	1	70	V-39  	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7785,69	 	JV522 	W9377	
VCP9378H	VICRYL PLUS	VICRYL PLUS	1	2	70	V-39  	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7836,92	 	W9378	 	
VCP9391G	VICRYL PLUS	VICRYL PLUS	1	1	100	BP-5  	65	3/8    	Тупоконечная 	 	1	Нержавеющая сталь	0	1	12	4035,21	 	 	 	
VCP9468H	VICRYL PLUS	VICRYL PLUS	1	1	90	V-37  	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7608,66	 	W9373	W9368	
VCP9507H	VICRYL PLUS	VICRYL PLUS	0	3/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	8083,70	 	W9507T	 	
VCP9531H	VICRYL PLUS	VICRYL PLUS	0	3/0	70	PCLX  PRIME	36	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	9030,40	 	W9531T	 	
VCP9532H	VICRYL PLUS	VICRYL PLUS	0	2/0	70	PCLX  PRIME	36	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	9162,49	 	W9532T	 	
VCP9571H	VICRYL PLUS	VICRYL PLUS	0	3/0	70	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	6009,08	 	W9571T	 	
VCP9582H	VICRYL PLUS	VICRYL PLUS	0	2/0	70	PC-25 PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	36	9620,62	 	W9582T	 	
VCP9826H	VICRYL PLUS	VICRYL PLUS	0	3/0	70	V-4  	17	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7173,33	 	W9826	 	
VCP987H	VICRYL PLUS	VICRYL PLUS	1	0	70	V-5  	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7082,10	 	 	 	
VCP9900H	VICRYL PLUS	VICRYL PLUS	0	2/0	70	V-37  	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7160,10	 	W9900	 	
VCP9901H	VICRYL PLUS	VICRYL PLUS	0	0	70	V-37  	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7079,31	 	W9901	 	
VCP994H	VICRYL PLUS	VICRYL PLUS	1	4/0	70	V-5  	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7294,56	 	 	 	
VCP9982H	VICRYL PLUS	VICRYL PLUS	0	5/0	45	RB-2  	13	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7865,81	 	W9982	 	
VCP998H	VICRYL PLUS	VICRYL PLUS	1	3/0	70	V-7  	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7635,27	 	W9180	 	
VCP999H	VICRYL PLUS	VICRYL PLUS	1	2/0	70	V-7  	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7933,63	 	W9181	W9350	
VCP9996H	VICRYL PLUS	VICRYL PLUS	1	0	90	CTB  	40	1/2	Тупоконечная 	 	1	Нержавеющая сталь	0	1	36	9118,10	 	 	 	
V55H	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	70	CT-2  PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	6641,17	 	 	 	
W9910	VICRYL RAPIDE	VICRYL RAPIDE	0	5/0	45	PS-4C PRIME	16	Сложный изгиб	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3405,62	 	 	 	
W9911	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	45	PS-4C PRIME	16	Сложный изгиб	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3458,91	 	 	 	
W9913	VICRYL RAPIDE	VICRYL RAPIDE	0	6/0	45	P-1 PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	4044,27	 	 	 	
W9915	VICRYL RAPIDE	VICRYL RAPIDE	0	5/0	45	P-1 PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3594,05	 	 	 	
W9918	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	75	PC-3 PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	12	2809,04	 	 	 	
W9922	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	75	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	2980,12	 	 	 	
W9923	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3192,87	 	 	 	
W9924	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	75	FS-3	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	2512,55	 	 	 	
W9925	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	FS-3	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	2454,63	 	 	 	
W9926	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	45	PS-22 PRIME	22	1/2	Режущая PRIME	 	1	Ethalloy	0	1	12	2411,70	 	 	 	
W9928	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	75	UV-17	75	5/8   	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	3276,21	 	 	 	
W9930	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	75	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2512,55	 	 	 	
W9931	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2512,55	 	 	 	
W9932	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	PS	26	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	12	2554,22	 	 	 	
W9935	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	45	X-1 CONV	22	1/2	Режущая	 	1	Нержавеющая сталь	0	1	12	2512,55	 	 	 	
W9936	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	75	CP-2	26	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2554,22	 	 	 	
W9937	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	PSLX	36	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	12	2980,12	 	 	 	
W9938	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	75	PSLX	36	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	12	2980,12	 	 	 	
W9940	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2587,92	 	 	 	
W9941	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	75	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2587,92	 	 	 	
W9946	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	90	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2983,27	 	 	 	
W9947	VICRYL RAPIDE	VICRYL RAPIDE	0	0	90	V-39	90	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	3641,08	 	 	 	
W9961	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	75	FSLX	36	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2683,63	 	 	 	
W9962	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2725,30	 	 	 	
W9963	VICRYL RAPIDE	VICRYL RAPIDE	0	0	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2893,32	 	 	 	
W9964	VICRYL RAPIDE	VICRYL RAPIDE	0	1	90	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2938,45	 	 	 	
W9969	VICRYL RAPIDE	VICRYL RAPIDE	0	5/0	75	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	3429,56	 	 	 	
W9970	VICRYL RAPIDE	VICRYL RAPIDE	0	4/0	75	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	3350,32	 	 	 	
W9974	VICRYL RAPIDE	VICRYL RAPIDE	0	3/0	75	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2235,49	 	 	 	
W9975	VICRYL RAPIDE	VICRYL RAPIDE	0	2/0	75	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2325,88	 	 	 	
V8655H	VICRYL RAPIDE	VICRYL RAPIDE	0	1	70	-	-	-	-	 	2	-	-	-	36	8488,01	 	 	 	
V8654E	VICRYL RAPIDE	VICRYL RAPIDE	0	0	70	-	-	-	-	 	2	-	-	-	24	6535,79	 	 	 	
MCP3548H	MONOCRYL PLUS	MONOCRYL PLUS	1	4/0	70	BB	17	3/8    	Колющая	 	1	Ethalloy	0	1	36	7346,86	 	W3548	 	
MCP4957H	MONOCRYL PLUS	MONOCRYL PLUS	1	2/0 	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7562,19	 	 	 	
MCP4958H	MONOCRYL PLUS	MONOCRYL PLUS	1	0	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7871,64	 	W3758	 	
MCP4959H	MONOCRYL PLUS	MONOCRYL PLUS	1	1	90	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8283,97	 	 	 	
MCP229H	MONOCRYL PLUS	MONOCRYL PLUS	1	1	70	MH PLUS	36	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	8396,42	 	W3457	 	
MCP247H	MONOCRYL PLUS	MONOCRYL PLUS	1	0	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7562,19	 	W3442	 	
MCP3200H	MONOCRYL PLUS	MONOCRYL PLUS	1	3/0 	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7570,43	 	 	 	
MCP3441G	MONOCRYL PLUS	MONOCRYL PLUS	1	2/0 	70	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2378,98	 	 	 	
MCP493H	MONOCRYL PLUS	MONOCRYL PLUS	0	5/0 	70	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	10654,28	 	W3203	MPY493H	
MCP3212H	MONOCRYL PLUS	MONOCRYL PLUS	0	4/0	70	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	9392,62	 	 	 	
MCP3213H	MONOCRYL PLUS	MONOCRYL PLUS	0	3/0 	70	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	9392,62	 	W3213	 	
MCP3209G	MONOCRYL PLUS	MONOCRYL PLUS	0	5/0 	70	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2930,00	 	W3209	 	
MCP4271H	MONOCRYL PLUS	MONOCRYL PLUS	0	3/0 	70	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	9392,62	 	MPY497H	 	
MCP496H	MONOCRYL PLUS	MONOCRYL PLUS	0	4/0	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	9392,62	 	MPY496H	 	
MCP497H	MONOCRYL PLUS	MONOCRYL PLUS	0	3/0 	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	8894,39	 	W3207	 	
MCP3205G	MONOCRYL PLUS	MONOCRYL PLUS	0	4/0	70	PS-3  PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	12	3130,87	 	W3205	 	
MCP500H	MONOCRYL PLUS	MONOCRYL PLUS	0	5/0 	70	PS-3  PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	36	10514,47	 	W3204	MPY500H	
MCP215H	MONOCRYL PLUS	MONOCRYL PLUS	1	3/0 	70	RB-1  PLUS	17	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7850,48	 	W3437	 	
MCP3435G	MONOCRYL PLUS	MONOCRYL PLUS	1	4/0	70	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2663,58	 	W3435	 	
MCP3224G	MONOCRYL PLUS	MONOCRYL PLUS	1	6/0	45	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	1	12	3130,87	 	W3224	 	
MCP4160H	MONOCRYL PLUS	MONOCRYL PLUS	1	3/0 	70	SH PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7496,93	 	W3447	 	
MCP4170H	MONOCRYL PLUS	MONOCRYL PLUS	1	2/0 	70	SH PLUS	26	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7776,66	 	W3448	 	
MCP219G	MONOCRYL PLUS	MONOCRYL PLUS	1	3/0 	70	SH-1	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2570,36	 	W3661	 	
MCP218H	MONOCRYL PLUS	MONOCRYL PLUS	1	4/0	70	SH-1  PLUS	22	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7570,43	 	W3660	 	
MCP220H	MONOCRYL PLUS	MONOCRYL PLUS	1	2/0 	70	SH-1  PLUS	22	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1	36	7570,43	 	W3662	 	
MCP3488G	MONOCRYL PLUS	MONOCRYL PLUS	1	2/0 	70	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2944,03	 	W3488	 	
MCP3489G	MONOCRYL PLUS	MONOCRYL PLUS	1	0	70	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2944,03	 	W3489	 	
MCP3490G	MONOCRYL PLUS	MONOCRYL PLUS	1	1	70	V-34	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2676,36	 	 	 	
MCP3221G	MONOCRYL PLUS	MONOCRYL PLUS	0	5/0 	45	FS-3	16	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2336,50	 	W3221	 	
MCP3326G	MONOCRYL PLUS	MONOCRYL PLUS	0	3/0 	70	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2623,88	 	W3326	 	
MCP3327G	MONOCRYL PLUS	MONOCRYL PLUS	0	2/0 	70	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	2803,80	 	W3327	 	
MCP4330H	MONOCRYL PLUS	MONOCRYL PLUS	1	0	90	CT-2	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	7838,47	 	 	 	
MCP4331H	MONOCRYL PLUS	MONOCRYL PLUS	1	1	90	CT-2	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8092,20	 	 	 	
PN8714H	PRONOVA	PRONOVA	1	7/0	60	BV175-8 	9,3	3/8    	Колющая	 	1	Ethalloy	0	2	36	29691,85	 	 	 	
PN8706H	PRONOVA	PRONOVA	1	7/0	60	BV175-8	9,3	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	29691,86	 	 	 	
PN1839H	PRONOVA	PRONOVA	1	7/0	60	CC	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	31661,42	 	 	 	
PN1890G	PRONOVA	PRONOVA	1	8/0	60	BV175-6 	8	3/8    	Колющая	 	1	Ethalloy	0	2	12	7164,23	 	 	 	
PN8803G	PRONOVA	PRONOVA	1	8/0	60	CC	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	9875,06	 	 	 	
PN1809H	PRONOVA	PRONOVA	1	6/0	60	CC	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	24351,23	 	 	 	
PN8605H	PRONOVA	PRONOVA	1	6/0	60	CC-1	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	24184,92	 	 	 	
PN1812H	PRONOVA	PRONOVA	1	5/0	75	CC-1	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	21432,17	 	 	 	
PN8500H	PRONOVA	PRONOVA	1	5/0	75	RB-2	13	1/2	Колющая	 	1	Ethalloy	0	2	36	20624,82	 	 	 	
L153	MERSILENE	MERSILENE	1	2/0	250	-	-	-	-	 	1	-	-	-	12	1099,75	Катушка LIGAPAK	 	 	
L154	MERSILENE	MERSILENE	1	0	250	-	-	-	-	 	1	-	-	-	12	1132,10	Катушка LIGAPAK	 	 	
L155	MERSILENE	MERSILENE	1	1	250	-	-	-	-	 	1	-	-	-	12	1277,17	Катушка LIGAPAK	 	 	
EH6407G	MERSILENE	MERSILENE	1	2	250	-	-	-	-	 	1	-	-	-	12	1106,23	Катушка LIGAPAK	 	 	
W1775	MERSILENE	MERSILENE	1	10/0	30	TG140-6	6,5	3/8    	Шпательная	 	1	Нержавеющая сталь	0	2	12	11832,24	 	 	 	
W394	MERSILENE	MERSILENE	0	7	150	-	-	-	-	 	1	-	-	-	12	3686,08	 	 	 	
W832	MERSILENE	MERSILENE	0	4/0	45	S-4	6,5	1/4	Шпательная	 	1	Ethalloy	0	2	12	7011,98	 	 	 	
W843	MERSILENE	MERSILENE	0	5/0	45	S-14	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	6598,49	 	 	 	
6518H	ETHIBOND EXCEL	ETHIBOND EXCEL	1	1	75	 OS-4	22	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	6009,91	 	 	 	
EH7715LG	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-5 	17	1/2	Колюще-режущая	 	4	Ethalloy	0	2	12	16615,41	2 - зеленые/2 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
EH7716LG	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-7 	26	1/2	Колюще-режущая	 	4	Ethalloy	0	2	12	16194,72	2 - зеленые/2 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
MEH6937N	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-5  	17	1/2	Колюще-режущая	 	8	Нержавеющая сталь	0	2	6	20834,38	4 - зеленые, 4 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
MEH7710LG	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	90	V-7 	26	1/2	Колюще-режущая	 	4	Ethalloy	0	2	12	16827,77	2 - зеленые/2 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
MEH7714LG	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-6 	22	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	2	12	17531,71	2 - зеленые/2 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
MEH7715N	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-5 	17	1/2	Колюще-режущая	 	8	Нержавеющая сталь	0	2	6	16615,41	4 - зеленые, 4 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
MEH7716N	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-7 	26	1/2	Колюще-режущая	 	8	Нержавеющая сталь	0	2	6	21238,92	4 - зеленые, 4 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
MEH7718LG	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	90	V-5 	17	1/2	Колюще-режущая	 	4	Ethalloy	0	2	12	16827,77	2 - зеленые/2 - неокрашенные, овальная прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
W10B52	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-5 	17	1/2	Колюще-режущая	 	10	Нержавеющая сталь	0	2	6	15496,20	5 - зеленых/ 5 - неокрашенных	 	 	
W10B54	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-5 	17	1/2	Колюще-режущая	 	10	Нержавеющая сталь	0	2	6	22137,36	5 - зеленых/ 5 - неокрашенных, прокладка PTFE 3 mm x 3 mm x 1,5 mm	 	 	
X976H	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	75	V-7  	26	1/2	Колюще-режущая	 	1	Ethalloy	0	2	36	10313,02	5 - зеленых/ 5 - неокрашенных	 	 	
PX17H	ETHIBOND EXCEL	ETHIBOND EXCEL	0	2/0	90	V-5  	17	1/2	Колюще-режущая	 	1	Ethalloy	0	2	36	12500,63	5 - зеленых/ 5 - неокрашенных, овальная прокладка PTFE 7 mm x 3 mm x 1,5 mm	 	 	
X726H	ETHIBOND EXCEL	ETHIBOND EXCEL	1	4/0	75	C-1  	13	3/8    	Колющая	 	1	Ethalloy	0	2	36	11719,34	5 - зеленых/ 5 - неокрашенных, прокладка PTFE  7 mm x 3 mm x 1.5 mm	 	 	
X806H	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	90	V-5  	17	1/2	Колюще-режущая	 	1	Ethalloy	0	2	36	10457,26	5 - зеленых/ 5 - неокрашенных	 	 	
X807H	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-5  	17	1/2	Колюще-режущая	 	1	Ethalloy	0	2	36	11042,22	5 - зеленых/ 5 - неокрашенных	 	 	
X966H	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-7  	26	1/2	Колюще-режущая	 	1	Ethalloy	0	2	36	10208,85	 	 	 	
W10B55	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-5 	17	1/2	Колюще-режущая	 	10	Нержавеющая сталь	0	2	6	20366,44	5 - зеленых/ 5 - неокрашенных, прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
W10B62	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	SH-2 	20	1/2	Колющая	 	10	Ethalloy	0	2	6	15496,20	5 - зеленых/ 5 - неокрашенных	 	 	
W10B72	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-7 	26	1/2	Колюще-режущая	 	10	Нержавеющая сталь	0	2	6	15496,20	5 - зеленых/ 5 - неокрашенных	 	 	
W10B77	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	V-7 	26	1/2	Колюще-режущая	 	10	Нержавеющая сталь	0	2	6	20366,44	5 - зеленых/ 5 - неокрашенных, прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
W10B82	ETHIBOND EXCEL	ETHIBOND EXCEL	2	2/0	75	SH  	26	1/2	Колющая	 	10	Ethalloy	0	2	6	12839,40	5 - зеленых/ 5 - неокрашенных	 	 	
W4843	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2	75	V-39 	45	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	1	12	7454,25	 	 	 	
W4846	ETHIBOND EXCEL	ETHIBOND EXCEL	1	5	75	V-55 	55	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	1	12	8597,79	 	 	 	
W4B37	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-5 	17	1/2	Колюще-режущая	 	4	Ethalloy	0	2	12	17363,13	прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
W4B77	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-7 	26	1/2	Колюще-режущая	 	4	Ethalloy	0	2	12	17363,13	прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
W6154	ETHIBOND EXCEL	ETHIBOND EXCEL	1	0	180	-	-	-	-	 	1	-	-	-	12	990,82	 	 	 	
W6155	ETHIBOND EXCEL	ETHIBOND EXCEL	1	1	180	-	-	-	-	 	1	-	-	-	12	1039,67	 	 	 	
W6156	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2	180	-	-	-	-	 	1	-	-	-	12	1083,84	 	 	 	
W6191	ETHIBOND EXCEL	ETHIBOND EXCEL	1	0	75	XLH  	70	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3521,62	 	 	 	
W623	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	75	KS  	60	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	1760,80	 	 	 	
W6232	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	60	-	-	-	-	 	13	-	-	-	12	1804,98	 	 	 	
W6233	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	60	-	-	-	-	 	13	-	-	-	12	2804,63	 	 	 	
W6234	ETHIBOND EXCEL	ETHIBOND EXCEL	1	0	60	-	-	-	-	 	13	-	-	-	12	2030,50	 	 	 	
W6552	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	100	SH  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	2526,12	 	 	 	
W6582	ETHIBOND EXCEL	ETHIBOND EXCEL	1	4/0	45	 PS-2	19	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	12	2128,72	 	 	 	
W6584	ETHIBOND EXCEL	ETHIBOND EXCEL	0	4/0	45	VT-7	26	Прямая	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2844,64	 	 	 	
W6597	ETHIBOND EXCEL	ETHIBOND EXCEL	1	6/0	75	BV  1	11	3/8    	Колющая	 	1	Ethalloy	0	2	12	7211,89	 	 	 	
W6757	ETHIBOND EXCEL	ETHIBOND EXCEL	1	5/0	60	RB-1 	17	1/2	Колющая	 	1	Ethalloy	0	2	12	2454,63	 	 	 	
W6759	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	60	RB-1 	17	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6760	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	RB-1 	17	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6761	ETHIBOND EXCEL	ETHIBOND EXCEL	1	4/0	75	SH-2 	20	1/2	Колющая	 	1	Ethalloy	0	2	12	2454,63	 	 	 	
W6763	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	75	SH-2 	20	1/2	Колющая	 	1	Ethalloy	0	2	12	2454,63	 	 	 	
W6767	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	SH  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	2855,46	 	 	 	
W6831	ETHIBOND EXCEL	ETHIBOND EXCEL	1	4/0	75	SH  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1  	12	1710,97	 	 	 	
W6832	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	75	SH  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	1  	12	1689,51	 	 	 	
W6890	ETHIBOND EXCEL	ETHIBOND EXCEL	1	5/0	75	С-1 	13	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	2936,22	 	 	 	
W6891	ETHIBOND EXCEL	ETHIBOND EXCEL	1	4/0	75	С-1 	13	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	2807,06	 	 	 	
W6915	ETHIBOND EXCEL	ETHIBOND EXCEL	0	4/0	90	V-5 	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6917	ETHIBOND EXCEL	ETHIBOND EXCEL	0	2/0	90	V-5  	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6923	ETHIBOND EXCEL	ETHIBOND EXCEL	0	3/0 	75	V-4  	17	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6934	ETHIBOND EXCEL	ETHIBOND EXCEL	1	5/0	90	V-5 	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2630,84	 	 	 	
W6935	ETHIBOND EXCEL	ETHIBOND EXCEL	1	4/0	90	V-5 	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6936	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	90	V-5 	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6937	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-5 	17	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2717,95	 	 	 	
W6952	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-34 	36	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2587,92	 	 	 	
W6976	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	90	V-7 	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6977	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-7 	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2717,95	 	 	 	
W6977P	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-7 	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	4604,29	прокладка PTFE 6 mm x 3 mm x 1,5 mm	 	 	
W6978	ETHIBOND EXCEL	ETHIBOND EXCEL	1	0	90	V-7 	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2807,06	 	 	 	
W6987	ETHIBOND EXCEL	ETHIBOND EXCEL	0	2/0	90	V-7 	26	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2544,99	 	 	 	
W6995	ETHIBOND EXCEL	ETHIBOND EXCEL	0	4/0	45	V-26 	26	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2587,92	 	 	 	
W6997	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	90	V-26 	26	3/8    	Колюще-режущая	 	1	Нержавеющая сталь	0	2	12	2630,84	 	 	 	
W882	ETHIBOND EXCEL	ETHIBOND EXCEL	0	4/0	45	S-4 	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7273,88	 	 	 	
W894	ETHIBOND EXCEL	ETHIBOND EXCEL	1	5/0	45	S-14 	8	1/4	Шпательная	 	1	Ethalloy	0	2	12	7066,06	 	 	 	
W931	ETHIBOND EXCEL	ETHIBOND EXCEL	1	3/0 	75	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1  	12	1732,43	 	 	 	
W932	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	75	MH-1  PLUS	31	1/2	Колющая PLUS	 	1	Нержавеющая сталь	0	1  	12	1732,43	 	 	 	
W975	ETHIBOND EXCEL	ETHIBOND EXCEL	1	0	75	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1  	12	1661,14	 	 	 	
W976	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	75	MH	36	1/2	Колющая	 	1	Ethalloy	0	1  	12	1732,43	 	 	 	
W979	ETHIBOND EXCEL	ETHIBOND EXCEL	1	0	75	V-40	48	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1  	12	1894,72	 	 	 	
W993	ETHIBOND EXCEL	ETHIBOND EXCEL	1	2/0	100	KP-3	90	3/8    	Режущая	 	1	Нержавеющая сталь	0	1  	12	2214,03	 	 	 	
W998	ETHIBOND EXCEL	ETHIBOND EXCEL	1	1	100	KP-3	90	3/8    	Режущая	 	1	Нержавеющая сталь	0	1  	12	2192,56	 	 	 	
E6102S	ETHIBOND EXCEL	ETHIBOND EXCEL	0	2/0	20	SKI-22	22	Лыжеобразная	Колющая	 	1	Нержавеющая сталь	0	1  	24	7320,96	 	 	 	
EH7796H	ETHILON	ETHILON	1	2/0 	45	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	3458,91	 	 	 	
EH7939H	ETHILON	ETHILON	1	3/0 	75	FSL	30	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	4006,08	 	 	 	
U7003	ETHILON	ETHILON	1	10/0 	30	CS140-6	6,5	3/8    	Шпательная	 	1	Нержавеющая сталь	0	2	12	11957,27	 	 	 	
W1600T	ETHILON	ETHILON	1	6/0 	45	P-6  PRIME	8	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5755,71	 	 	 	
W1610T	ETHILON	ETHILON	1	6/0 	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5755,71	 	 	 	
W1611T	ETHILON	ETHILON	1	5/0 	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5534,33	 	 	 	
W1612T	ETHILON	ETHILON	1	4/0 	45	P-1 PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5192,16	 	 	 	
W1615T	ETHILON	ETHILON	1	6/0 	45	PC-3 PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	3288,65	 	 	 	
W1616T	ETHILON	ETHILON	1	5/0 	45	PС-3 PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	3021,75	 	 	 	
W1618T	ETHILON	ETHILON	1	5/0 	45	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	24	3462,90	 	 	 	
W1619T	ETHILON	ETHILON	1	4/0 	75	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	3405,62	 	 	 	
W1620T	ETHILON	ETHILON	1	4/0 	45	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	2934,55	 	 	 	
W1621T	ETHILON	ETHILON	1	3/0 	45	PS-2 PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	3322,29	 	 	 	
W1625T	ETHILON	ETHILON	1	3/0 	45	PS PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	2850,71	 	 	 	
W1626T	ETHILON	ETHILON	1	2/0 	75	PS PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	3192,87	 	 	 	
W1627T	ETHILON	ETHILON	1	3/0 	75	PSLX	36	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	24	3322,29	 	 	 	
W1632T	ETHILON	ETHILON	1	2/0 	75	CEC135-40	40	3/8    	Режущая	 	1	Ethalloy	0	1	24	3363,96	 	 	 	
W1685T	ETHILON	ETHILON	1	3/0 	75	PS PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	3322,29	 	 	 	
W1717	ETHILON	ETHILON	1	9/0 	30	TG140-8	6,5	3/8    	Шпательная	 	1	Ethalloy	0	2	12	7696,87	 	 	 	
W1718	ETHILON	ETHILON	1	10/0 	30	TG140-8	6,5	3/8    	Шпательная	 	1	Ethalloy	0	2	12	8098,52	 	 	 	
W1765	ETHILON	ETHILON	1	8/0 	45	G-7	8	1/2	Обратно-режущая	 	1	Ethalloy	0	1	12	5747,08	 	 	 	
W1770	ETHILON	ETHILON	1	10/0 	30	TG140-6	6,5	3/8    	Шпательная	 	1	Нержавеющая сталь	0	2	12	9286,48	 	 	 	
W1854T	ETHILON	ETHILON	1	5/0 	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4470,17	 	 	 	
W1856T	ETHILON	ETHILON	1	4/0 	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4257,02	 	 	 	
W1857T	ETHILON	ETHILON	1	3/0 	45	PS-1  PRIME	24	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	4257,02	 	 	 	
W2808	ETHILON	ETHILON	1	8/0 	13	BV130-5	6,5	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	5321,58	 	 	 	
W2812	ETHILON	ETHILON	1	8/0 	13	BV130-4	5	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	5478,09	 	 	 	
W2813	ETHILON	ETHILON	1	9/0 	13	BV130-4	5	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	5959,83	 	 	 	
W2829	ETHILON	ETHILON	1	9/0 	13	BV100-4	5,1	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	7237,14	 	 	 	
W2850	ETHILON	ETHILON	1	10/0 	13	BV75-4	5,1	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	10642,77	 	 	 	
W2870	ETHILON	ETHILON	1	10/0 	13	BV75-3	3,8	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	10642,77	 	 	 	
W2871	ETHILON	ETHILON	1	9/0 	13	BV75-3	3,8	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	10955,79	 	 	 	
W2881	ETHILON	ETHILON	1	11/0 	13	BV50-3	3,8	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	20033,06	 	 	 	
W2898	ETHILON	ETHILON	1	9/0 	13	TGW140-4	3,5	1/4	Колющая	 	1	Нержавеющая сталь	0	1	12	8130,22	 	 	 	
W2901	ETHILON	ETHILON	1	8/0 	13	TG135-6	4,5	3/8    	Шпательная	 	1	Нержавеющая сталь	0	1	12	7669,01	 	 	 	
W2908	ETHILON	ETHILON	1	8/0 	13	BV130-5 VB	6,5	3/8    	Колющая	 	1	Нержавеющая сталь	1	1	12	6302,40	 	 	 	
W2913	ETHILON	ETHILON	1	9/0 	13	BV130-4 VB	5	3/8    	Колющая	 	1	Нержавеющая сталь	1	1	12	6811,64	 	 	 	
W319	ETHILON	ETHILON	1	4/0 	45	FS-2	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1098,51	 	 	 	
W320	ETHILON	ETHILON	1	3/0 	45	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1064,19	 	 	 	
W558	ETHILON	ETHILON	1	2/0 	100	LR-60	60	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1442,28	 	 	 	
W568	ETHILON	ETHILON	1	2/0 	100	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1442,28	 	 	 	
W736	ETHILON	ETHILON	1	2/0 	100	LS-1	45	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1534,76	 	 	 	
W737	ETHILON	ETHILON	1	0	100	CP	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1534,76	 	 	 	
W738	ETHILON	ETHILON	1	1	100	CPX	48	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1491,83	 	 	 	
W740	ETHILON	ETHILON	1	0	150	СТ	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2454,63	Нить в виде петли	 	 	
W741	ETHILON	ETHILON	1	0	150	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2161,97	Нить в виде петли	 	 	
W748	ETHILON	ETHILON	1	1	150	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2377,93	Нить в виде петли	 	 	
W749	ETHILON	ETHILON	1	1	150	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1513,29	 	 	 	
W757	ETHILON	ETHILON	1	1	200	V-39	45	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2936,22	Нить в виде петли	 	 	
W760	ETHILON	ETHILON	1	0	200	СТ	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2411,70	Нить в виде петли	 	 	
W768	ETHILON	ETHILON	1	1	200	CTX	48	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2630,84	Нить в виде петли	 	 	
W786	ETHILON	ETHILON	1	2/0 	45	CEC135-40	40	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	1272,30	 	 	 	
W798	ETHILON	ETHILON	1	2	100	KP-3	90	3/8    	Режущая	 	1	Ethalloy	0	1	12	1973,03	 	 	 	
W8170	ETHILON	ETHILON	1	8/0 	45	BV175-6	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	7015,99	 	 	 	
C5435G	NUROLON	NUROLON	1	2/0 	75	SH C/R	26	1/2	Колющая	 	8	Нержавеющая сталь	0	1	12	10434,02	 	 	 	
EH6586H	NUROLON	NUROLON	1	1	70	-	-	-	-	 	2	-	-	-	36	6410,58	 	 	 	
W5205	NUROLON	NUROLON	1	1	45	-	-	-	-	 	17	-	-	-	12	2192,56	 	 	 	
W5222	NUROLON	NUROLON	1	3/0 	75	-	-	-	-	 	10	-	-	-	12	3605,95	 	 	 	
W5223	NUROLON	NUROLON	1	2/0 	75	-	-	-	-	 	10	-	-	-	12	2411,70	 	 	 	
W5224	NUROLON	NUROLON	1	0	75	-	-	-	-	 	10	-	-	-	12	2411,70	 	 	 	
W5225	NUROLON	NUROLON	1	1	75	-	-	-	-	 	10	-	-	-	12	2630,84	 	 	 	
W5321	NUROLON	NUROLON	1	2/0 	45	FS	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1534,76	 	 	 	
W5415	NUROLON	NUROLON	1	1	100	V-37	40	1/2	Колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2031,06	 	 	 	
W5534	NUROLON	NUROLON	1	2/0 	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Нержавеющая сталь	0	1	12	1667,80	 	 	 	
W5723	NUROLON	NUROLON	1	1	75	MO-5	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1736,87	 	 	 	
W5775	NUROLON	NUROLON	1	2/0 	100	KS-55	55	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	1535,30	 	 	 	
W5957	NUROLON	NUROLON	1	1	75	OS-4	22	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1760,80	 	 	 	
W5985	NUROLON	NUROLON	1	1	75	ASH-30	31	Игла-крючок	Колющая	 	1	Нержавеющая сталь	0	1	12	2161,97	 	 	 	
W6540	NUROLON	NUROLON	1	3/0 	35	RB-1	17	1/2	Колющая	 	6	Нержавеющая сталь	0	1	12	6768,88	 	 	 	
FSN13	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	2/0 	30	Х1	50	1/2	Режущая	 	1	Нержавеющая сталь	0	2	12	9365,86	Вторая игла ST (22мм) прямая	 	 	
M624G	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	7	45	CPX	48	1/2	Обратно-режущая	 	4	Нержавеющая сталь	0	1	12	10903,42	вращающаяся	 	 	
M635G	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	5	45	CPXX	55	1/2	Обратно-режущая	 	4	Нержавеющая сталь	0	1	12	12051,66	вращающаяся	 	 	
M649G	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	6	45	V-40	48	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	1	12	12489,66	 	 	 	
M650G	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	5	45	V-40	48	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	1	12	10736,78	 	 	 	
M651G	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	4	45	V-40	48	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	1	12	11707,32	 	 	 	
M660G	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	1	45	V-37	40	1/2	Колюще-режущая	 	4	Нержавеющая сталь	0	1	12	9571,01	 	 	 	
W311	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	2/0 	75	MH-1	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2921,62	 	 	 	
W945	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	5	75	TR-55	55	1/2	Троакарная	 	1	Нержавеющая сталь	0	1	12	3420,00	 	 	 	
W995	Stainless Steel	Хирургическая проволока из нержавеющей стали	0	5	75	CPXX	55	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	4173,69	вращающаяся	 	 	
8523H	PROLENE	PROLENE	1	  2/0 	90	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	7682,30	 	 	 	
EP8732H	PROLENE	PROLENE	1	8/0	60	BV130-5 	6,5	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	48400,00	 	 	 	
EP8730H	PROLENE	PROLENE	1	8/0	45	BV130-5 	6,5	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	46200,00	 	 	 	
8335H	PROLENE	PROLENE	1	7/0	60	BV175-6	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	20118,83	 	 	 	
8528H	PROLENE	PROLENE	1	  3/0 	75	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	17491,61	 	 	 	
8558H	PROLENE	PROLENE	1	  3/0 	90	RB-1 	17	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	9623,40	 	 	 	
8675H	PROLENE	PROLENE	1	  3/0 	75	FSL 	30	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	3815,32	 	 	 	
8711H	PROLENE	PROLENE	1	 6/0	75	RB-2 	13	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	12528,94	 	 	 	
8735H	PROLENE	PROLENE	1	7/0	60	BV175-6 	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	18688,11	 	 	 	
8741H	PROLENE	PROLENE	1	8/0	60	BV175-6 	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	22024,04	 	 	 	
8766H	PROLENE	PROLENE	1	7/0	60	BV175-7 	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	24933,62	 	 	 	
8841H	PROLENE	PROLENE	1	8/0	60	BV175-6	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	22464,29	 	 	 	
8871H	PROLENE	PROLENE	1	4/0	75	RB-1 	17	1/2	Колющая	 	1	Ethalloy	0	1	36	5340,61	 	 	 	
BP8557	PROLENE	PROLENE	1	4/0	90	RB-1 	17	1/2	Колющая	 	1	Ethalloy	0	2	36	11326,80	 	 	 	
EH7236H	PROLENE	PROLENE	1	7/0	75	CCX	9,3	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	12105,31	 	 	 	
EH7584H	PROLENE	PROLENE	1	  3/0 	120	SH 	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	11505,30	 	 	 	
EH7598G	PROLENE	PROLENE	1	10/0	30	GS-12	5,5	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	2	12	14188,52	 	 	 	
EH7666G	PROLENE	PROLENE	1	  3/0 	60	PS-1 PRIME	24	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	12	4642,29	 	 	 	
EH7697H	PROLENE	PROLENE	1	  2/0 	75	FSL 	30	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	36	4239,16	 	 	 	
EH7811E	PROLENE	PROLENE	1	7/0	30	TF-6 	6,5	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	13474,53	 	 	 	
EH7812E	PROLENE	PROLENE	1	 6/0	30	TF-6 	6,5	1/2	Колющая	 	1	Нержавеющая сталь	0	1	24	13189,13	 	 	 	
EH7835H	PROLENE	PROLENE	1	 6/0	75	TF-1 	10	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	15368,50	 	 	 	
EH7972E	PROLENE	PROLENE	1	8/0	60	CC 	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	24	14790,43	 	 	 	
EH8021E	PROLENE	PROLENE	1	7/0	75	ACC VB 	9,3	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	24	16475,27	 	 	 	
EH8025H	PROLENE	PROLENE	1	 6/0	75	ACC VB 	9,3	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	36	19130,19	 	 	 	
EH8030H	PROLENE	PROLENE	1	 6/0	75	ACC-1 VB	13	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	36	20086,64	 	 	 	
EH8036H	PROLENE	PROLENE	1	 5/0 	75	ACC-1 VB	13	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	36	19130,19	 	 	 	
EH8066H	PROLENE	PROLENE	1	 5/0 	90	JB-1VB 	22	1/2	Колющая	 	1	Нержавеющая сталь	1	2	36	25562,85	 	 	 	
EP7972H	PROLENE	PROLENE	1	8/0	60	CC 175-8	9,3	3/8    	Колющая игла с микрозаточкой	 	1	EVERPOINT	0	2	36	38263,32	 	 	 	
EP8735H	PROLENE	PROLENE	1	7/0	60	BV175-6	8	3/8    	Колющая	 	1	EVERPOINT	0	2	36	20213,59	 	 	 	
EP8741H	PROLENE	PROLENE	1	8/0	60	BV175-6	8	3/8    	Колющая	 	1	EVERPOINT	0	2	36	37998,90	 	 	 	
EP8702H	PROLENE	PROLENE	1	7/0	60	BV-1 	 	3/8    	Колющая	 	1	EVERPOINT	0	2	36	26226,39	 	 	 	
EP8747H	PROLENE	PROLENE	1	7/0	60	BV175-8	9,3	3/8    	Колющая	 	1	EVERPOINT	0	2	36	26226,39	 	 	 	
EP8704SLH	PROLENE	PROLENE	1	7/0	60	CC 175-8	9,3	3/8    	Колющая игла с микрозаточкой	 	1	EVERPOINT	0	2	36	23727,17	 	 	 	
EPM8706	PROLENE	PROLENE	1	 6/0	75	C-1	13	3/8    	Колющая	 	4	EVERPOINT	0	2	12	38981,28	 	 	 	
EP8610H	PROLENE	PROLENE	1	 6/0	60	BV	 	3/8    	Колющая	 	1	EVERPOINT	0	2	36	38685,96	 	 	 	
EP8722H	PROLENE	PROLENE	1	 6/0	60	CC-1	13	3/8    	Колющая игла с микрозаточкой	 	1	EVERPOINT	0	2	36	26299,71	 	 	 	
EP8813H	PROLENE	PROLENE	1	7/0	60	CC-1 	13	3/8    	Колющая игла с микрозаточкой	 	1	EVERPOINT	0	2	36	28929,70	 	 	 	
EP8711H	PROLENE	PROLENE	1	 6/0	75	RB-2 	13	1/2	Колющая	 	1	EVERPOINT	0	2	36	23953,03	 	 	 	
EP8766H	PROLENE	PROLENE	1	7/0	60	BV175-7	8	3/8    	Колющая	 	1	EVERPOINT	0	2	36	40549,59	 	 	 	
F1800	PROLENE	PROLENE	1	 5/0 	75	RB-1	17	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	16689,30	 	 	 	
F1832	PROLENE	PROLENE	1	 6/0	75	CC	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	25586,14	 	 	 	
F1880	PROLENE	PROLENE	1	 5/0 	90	SH	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	8584,55	 	 	 	
F1890	PROLENE	PROLENE	1	8/0	60	BV130-5	6,5	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	36	26694,97	 	 	 	
F1891	PROLENE	PROLENE	1	9/0	45	BV100-4 	5,1	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	41973,39	 	 	 	
EH7813E	PROLENE	PROLENE	1	7/0	45	TF-6	5	1/2	Колющая	 	1	Нержавеющая сталь	0	2	24	13189,13	 	 	 	
EH7814E	PROLENE	PROLENE	1	 6/0	45	TF-6	5	1/2	Колющая	 	1	Нержавеющая сталь	0	2	24	15368,50	 	 	 	
EH8031H	PROLENE	PROLENE	1	 6/0	90	ACC-1 VB	13	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	36	18461,40	 	 	 	
M8726	PROLENE	PROLENE	1	 6/0	60	C-1	13	3/8    	Колющая	 	4	Нержавеющая сталь	0	2	12	26960,34	 	 	 	
MEH8024G	PROLENE	PROLENE	1	7/0	75	ACC VB 	9,3	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	12	20080,86	 	 	 	
OBB5692H	PROLENE	PROLENE	1	 5/0 	90	V-7	26	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	36	8676,71	 	 	 	
OKK5672H	PROLENE	PROLENE	1	 5/0 	75	V-6	22	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	36	9532,45	 	 	 	
PEE5692H	PROLENE	PROLENE	1	 5/0 	90	SH-1	22	1/2	Колющая	 	1	Ethalloy	0	2	36	9503,91	 	 	 	
PG5674H	PROLENE	PROLENE	1	  2/0 	75	MH  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	1	36	8850,04	 	 	 	
PGG5694H	PROLENE	PROLENE	1	  2/0 	90	MH  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	11257,69	 	 	 	
PGG5695H	PROLENE	PROLENE	1	0	90	MH  	36	1/2	Колющая	 	1	Нержавеющая сталь	0	2	36	9835,98	 	 	 	
W1709	PROLENE	PROLENE	1	9/0	23	TG140-8 	6,5	3/8    	шпательная   	 	1	Ethalloy	0	1	12	7721,37	 	 	 	
W1713	PROLENE	PROLENE	1	10/0	23	STC-6 PLUS	16	Прямая	шпательная   	 	1	Нержавеющая сталь	0	2	12	13900,19	 	 	 	
W1777	PROLENE	PROLENE	1	10/0	30	TG140-6 	6,2	3/8    	шпательная   	 	1	Нержавеющая сталь	0	2	12	10851,64	 	 	 	
W2775	PROLENE	PROLENE	1	8/0	13	BV130-5 	6,5	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	6092,80	 	 	 	
W2777	PROLENE	PROLENE	1	8/0	45	BV130-5 	13	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	9182,22	 	 	 	
W2780	PROLENE	PROLENE	1	9/0	13	BV130-4 	4,7	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	6051,95	 	 	 	
W2783	PROLENE	PROLENE	1	9/0	13	BV100-4 	5,1	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	9390,79	 	 	 	
W2790	PROLENE	PROLENE	1	10/0	13	BV75-3 	8	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	9881,71	 	 	 	
W2794	PROLENE	PROLENE	1	10/0	13	BV100-4 	5,1	3/8    	Колющая	 	1	Нержавеющая сталь	0	1	12	9599,37	 	 	 	
W295	PROLENE	PROLENE	1	  2/0 	75	MH-1 	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1643,85	 	 	 	
W486	PROLENE	PROLENE	1	0	100	V-34  	36	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	1	12	2018,89	 	 	 	
W523	PROLENE	PROLENE	1	 6/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	12	2406,50	 	 	 	
W525	PROLENE	PROLENE	1	4/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	12	2191,65	 	 	 	
W527	PROLENE	PROLENE	1	 5/0 	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	12	2280,27	 	 	 	
W538	PROLENE	PROLENE	1	  3/0 	45	BS-3  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	12	2631,01	 	 	 	
W626	PROLENE	PROLENE	1	  3/0 	75	PCLX  PRIME	36	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	12	3452,57	 	 	 	
W742	PROLENE	PROLENE	1	1	100	CT	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1677,43	 	 	 	
W753	PROLENE	PROLENE	1	0	100	KS-75	75	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	2149,57	 	 	 	
W7796	PROLENE	PROLENE	1	10/0	30	TG-6C	8	Сложный изгиб	Шпательная	 	1	Нержавеющая сталь	0	1	12	11952,77	 	 	 	
W8003T	PROLENE	PROLENE	1	 6/0	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5412,74	 	 	 	
W8005T	PROLENE	PROLENE	1	 6/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4471,53	 	 	 	
W8006T	PROLENE	PROLENE	1	 5/0 	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4298,75	 	 	 	
W8007T	PROLENE	PROLENE	1	4/0	45	PC-3  PRIME	16	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4168,08	 	 	 	
W8010T	PROLENE	PROLENE	1	 5/0 	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4383,33	 	 	 	
W8011T	PROLENE	PROLENE	1	4/0	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4290,67	 	 	 	
W8020T	PROLENE	PROLENE	1	4/0	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4512,25	 	 	 	
W8021T	PROLENE	PROLENE	1	  3/0 	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	4290,67	 	 	 	
W8026T	PROLENE	PROLENE	1	  2/0 	45	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	3953,24	 	 	 	
W8101	PROLENE	PROLENE	1	8/0	60	CC 175-6	8	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	9778,37	 	 	 	
W8121	PROLENE	PROLENE	1	7/0	60	ACC VB 	9,3	Сложный изгиб	Колющая	 	1	Нержавеющая сталь	1	2	12	8939,00	 	 	 	
W8304	PROLENE	PROLENE	1	7/0	60	BV-1 VB 	9,3	3/8    	Колющая	 	1	Ethalloy	1	2	12	8382,68	 	 	 	
W8305	PROLENE	PROLENE	1	 6/0	60	BV-1 VB 	9,3	3/8    	Колющая	 	1	Ethalloy	1	2	12	7910,93	 	 	 	
W8307	PROLENE	PROLENE	1	 6/0	60	C-1 VB 	13	3/8    	Колющая	 	1	Ethalloy	1	2	12	7524,96	 	 	 	
W8310	PROLENE	PROLENE	1	 5/0 	75	RB-2 VB 	13	1/2	Колющая	 	1	Нержавеющая сталь	1	2	12	3885,93	 	 	 	
W8316	PROLENE	PROLENE	1	 6/0	75	C-1 VB 	13	3/8    	Колющая	 	1	Ethalloy	1	2	12	6291,17	 	 	 	
W8321	PROLENE	PROLENE	1	 5/0 	60	C-1 VB 	13	3/8    	Колющая	 	1	Ethalloy	1	2	12	4235,23	 	 	 	
W8329	PROLENE	PROLENE	1	4/0	90	RB-1 VB 	17	1/2	Колющая	 	1	Ethalloy	1	2	12	3439,01	 	 	 	
W8330	PROLENE	PROLENE	1	 5/0 	90	RB-1 VB 	17	1/2	Колющая	 	1	Ethalloy	1	2	12	3684,24	 	 	 	
W8340	PROLENE	PROLENE	1	4/0	90	SH-2 VB 	20	1/2	Колющая	 	1	Нержавеющая сталь	1	2	12	3542,18	 	 	 	
W8354	PROLENE	PROLENE	1	  3/0 	90	SH VB  	26	1/2	Колющая	 	1	Нержавеющая сталь	1	2	12	3542,18	 	 	 	
W8355	PROLENE	PROLENE	1	4/0	90	SH VB  	26	1/2	Колющая	 	1	Нержавеющая сталь	1	2	12	3629,28	 	 	 	
W8400	PROLENE	PROLENE	1	  2/0 	75	ST-70	 	Прямая	Колющая	 	1	Нержавеющая сталь	0	2	12	2709,94	 	 	 	
W8401	PROLENE	PROLENE	1	1	100	TP-1 	65	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2709,94	 	 	 	
W8430	PROLENE	PROLENE	1	0	100	MO-5 	31	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1976,83	 	 	 	
W8434	PROLENE	PROLENE	1	0	100	MO-2 	40	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	1976,83	 	 	 	
W8440	PROLENE	PROLENE	1	1	100	TR-30 	30	1/2	Троакарная	 	1	Нержавеющая сталь	0	1	12	2149,57	 	 	 	
W8470	PROLENE	PROLENE	1	0	100	CP  	40	1/2	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1635,34	 	 	 	
W8521	PROLENE	PROLENE	1	4/0	90	SH  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3056,28	 	 	 	
W8522	PROLENE	PROLENE	1	  3/0 	90	SH  	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3012,52	 	 	 	
W8525	PROLENE	PROLENE	1	  3/0 	90	MH-1 	31	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3056,28	 	 	 	
W8526	PROLENE	PROLENE	1	  2/0 	90	MH-1 	31	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	2967,26	 	 	 	
W8534	PROLENE	PROLENE	1	  3/0 	120	SH 	26	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3611,77	 	 	 	
W8549	PROLENE	PROLENE	1	  3/0 	75	FS-3 CONV	16	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	1803,66	 	 	 	
W8556	PROLENE	PROLENE	1	 5/0 	90	RB-1	17	1/2	Колющая	 	1	Ethalloy	0	2	12	3142,75	 	 	 	
W8557	PROLENE	PROLENE	1	4/0	90	RB-1	17	1/2	Колющая	 	1	Ethalloy	0	2	12	3009,34	 	 	 	
W8558	PROLENE	PROLENE	1	  3/0 	90	RB-1	17	1/2	Колющая	 	1	Ethalloy	0	2	12	3009,34	 	 	 	
W8597	PROLENE	PROLENE	1	 6/0	60	BV 	11	3/8    	Колющая	 	1	Ethalloy	0	2	12	4556,07	 	 	 	
W8625	PROLENE	PROLENE	1	  3/0 	45	PS  PRIME	26	3/8    	Обратно-режущая PRIME	 	1	Нержавеющая сталь	0	1	12	3308,33	 	 	 	
W8626	PROLENE	PROLENE	1	  3/0 	75	PCLX  PRIME	36	3/8    	Режущая PRIME	 	1	Нержавеющая сталь	0	1	12	2924,78	 	 	 	
W8630	PROLENE	PROLENE	1	  3/0 	75	KS 	60	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	2836,58	 	 	 	
W8631	PROLENE	PROLENE	1	  2/0 	75	KS 	60	Прямая	Режущая	 	1	Нержавеющая сталь	0	1	12	2709,94	 	 	 	
W8662	PROLENE	PROLENE	1	 5/0 	60	CC 	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	7970,32	 	 	 	
W8664	PROLENE	PROLENE	1	 5/0 	60	CC-4 	13	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	7392,73	 	 	 	
W8665	PROLENE	PROLENE	1	4/0	75	CC-4  	13	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	6491,99	 	 	 	
W8667	PROLENE	PROLENE	1	  3/0 	90	CC-16  	16	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	6463,86	 	 	 	
W8683	PROLENE	PROLENE	1	4/0	45	FS-2 	19	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1719,51	 	 	 	
W8684	PROLENE	PROLENE	1	  3/0 	45	FS  	26	3/8    	Обратно-режущая	 	1	Нержавеющая сталь	0	1	12	1719,51	 	 	 	
W8689	PROLENE	PROLENE	1	  2/0 	100	CEC 135-40	40	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	12	2364,41	 	 	 	
W8697	PROLENE	PROLENE	1	 6/0	45	G-1 	11	3/8    	Обратно-режущая	 	1	Ethalloy	0	1	12	2477,28	 	 	 	
W8702	PROLENE	PROLENE	1	7/0	60	BV175-8 	9,3	3/8    	Колющая	 	1	Нержавеющая сталь	0	2	12	6641,79	 	 	 	
W8702S	PROLENE	PROLENE	1	7/0	60	BV175-8 	9,3	3/8    	Колющая	 	4	Нержавеющая сталь	0	2	12	24796,08	 	 	 	
W8703	PROLENE	PROLENE	1	8/0	60	CC 	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	10269,69	 	 	 	
W8704	PROLENE	PROLENE	1	7/0	60	CC  	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	7565,41	 	 	 	
W8706	PROLENE	PROLENE	1	 6/0	75	C-1  	13	3/8    	Колющая	 	1	Ethalloy	0	2	12	3486,87	 	 	 	
W8707	PROLENE	PROLENE	1	 6/0	60	CC-1 	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	5255,92	 	 	 	
W8710	PROLENE	PROLENE	1	 5/0 	75	RB-2  	13	1/2	Колющая	 	1	Ethalloy	0	2	12	3401,55	 	 	 	
W8712	PROLENE	PROLENE	1	 6/0	60	BV-1  	9,3	3/8    	Колющая	 	1	Ethalloy	0	2	12	5434,93	 	 	 	
W8718	PROLENE	PROLENE	1	 6/0	60	C-1 	13	3/8    	Колющая	 	1	Ethalloy	0	2	12	3827,01	 	 	 	
W8721	PROLENE	PROLENE	1	 5/0 	75	CC-1  	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3928,18	 	 	 	
W8725	PROLENE	PROLENE	1	7/0	60	C-1  	13	3/8    	Колющая	 	1	Ethalloy	0	2	12	5157,35	 	 	 	
W8731	PROLENE	PROLENE	1	  3/0 	45	X-1  	22	1/2	Режущая	 	1	Нержавеющая сталь	0	1	12	1719,51	 	 	 	
W8761	PROLENE	PROLENE	1	4/0	90	SH-2  	20	1/2	Колющая	 	1	Нержавеющая сталь	0	2	12	3277,56	 	 	 	
W8770	PROLENE	PROLENE	1	  3/0 	75	SH-1  	22	1/2	Колющая	 	1	Нержавеющая сталь	0	1	12	2050,73	 	 	 	
W8801	PROLENE	PROLENE	1	7/0	60	CC-11 	11	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	7995,09	 	 	 	
W8802	PROLENE	PROLENE	1	 6/0	60	CC-11  	11	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	7085,17	 	 	 	
W8803	PROLENE	PROLENE	1	 5/0 	60	CC-5  	11	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	4318,54	 	 	 	
W8807	PROLENE	PROLENE	1	 6/0	60	CC 	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	7522,93	 	 	 	
W8813	PROLENE	PROLENE	1	7/0	60	CC-1 	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	5890,86	 	 	 	
W8814	PROLENE	PROLENE	1	 6/0	75	CC-1  	13	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	4798,72	 	 	 	
W8815	PROLENE	PROLENE	1	 6/0	60	CC-4 	13	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	5158,51	 	 	 	
W8816	PROLENE	PROLENE	1	 5/0 	75	CC-4  	13	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3894,82	 	 	 	
W8830	PROLENE	PROLENE	1	 5/0 	90	CC-16  	16	1/2	Колющая игла с микрозаточкой	 	1	Ethalloy	0	2	12	3627,49	 	 	 	
W8831	PROLENE	PROLENE	1	4/0	90	CC-16 	16	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3994,97	 	 	 	
W8840	PROLENE	PROLENE	1	4/0	90	CC-20 	20	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3676,78	 	 	 	
W8843	PROLENE	PROLENE	1	  2/0 	90	V-7	26	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3585,52	 	 	 	
W8844	PROLENE	PROLENE	1	  3/0 	90	V-7	26	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3585,52	 	 	 	
W8845	PROLENE	PROLENE	1	4/0	90	CC-25 	26	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3585,52	 	 	 	
W8849	PROLENE	PROLENE	1	  3/0 	90	CC-30  	31	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3481,09	 	 	 	
W8850	PROLENE	PROLENE	1	  2/0 	90	CC-30  	31	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3615,78	 	 	 	
W8851	PROLENE	PROLENE	1	  3/0 	90	CC-40  	40	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3763,88	 	 	 	
W8852	PROLENE	PROLENE	1	  2/0 	90	CC-40  	40	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3763,88	 	 	 	
W8868T	PROLENE	PROLENE	1	 6/0	45	P-1  PRIME	11	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5593,54	 	 	 	
W8871T	PROLENE	PROLENE	1	 6/0	45	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5593,54	 	 	 	
W8872T	PROLENE	PROLENE	1	 5/0 	45	P-3  PRIME	13	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5593,54	 	 	 	
W8877T	PROLENE	PROLENE	1	4/0	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5217,24	 	 	 	
W8878T	PROLENE	PROLENE	1	  3/0 	45	PC-5  PRIME	19	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5258,09	 	 	 	
W8880T	PROLENE	PROLENE	1	  3/0 	45	PC-25  PRIME	26	3/8    	Режущая PRIME	 	1	Ethalloy	0	1	24	5258,09	 	 	 	
W8882T	PROLENE	PROLENE	1	 5/0 	45	PS-3  PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5344,11	 	 	 	
W8884T	PROLENE	PROLENE	1	4/0	45	PS-3  PRIME	16	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5258,09	 	 	 	
W8885T	PROLENE	PROLENE	1	4/0	45	PS-2  PRIME	19	3/8    	Обратно-режущая PRIME	 	1	Ethalloy	0	1	24	5298,94	 	 	 	
W8890	PROLENE	PROLENE	1	 5/0 	75	C-1  	13	3/8    	Колющая	 	1	Ethalloy	0	1	12	2892,51	 	 	 	
W8895	PROLENE	PROLENE	1	  3/0 	120	CC-25 	26	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	3654,25	 	 	 	
W8935	PROLENE	PROLENE	1	4/0	90	V-5 	17	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3234,22	 	 	 	
W8936	PROLENE	PROLENE	1	  3/0 	90	V5  	17	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3234,22	 	 	 	
W8937	PROLENE	PROLENE	1	  2/0 	90	V5  	17	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3234,22	 	 	 	
EP8805H	PROLENE	PROLENE	1	 6/0	60	BV-1   	 	3/8    	Колющая	 	1	EVERPOINT	0	2	36	27594,35	 	 	 	
EP8726H	PROLENE	PROLENE	1	 6/0	60	C-1  	13	3/8    	Колющая	 	1	EVERPOINT	0	2	36	27595,46	 	 	 	
EP8706H	PROLENE	PROLENE	1	 6/0	75	C-1  	13	3/8    	Колющая	 	1	EVERPOINT	0	2	36	29933,56	 	 	 	
EP8807H	PROLENE	PROLENE	1	 6/0	60	CC  	9,3	3/8    	Колющая игла с микрозаточкой	 	1	EVERPOINT	0	2	36	29982,33	 	 	 	
BP8390	PROLENE	PROLENE	1	 5/0 	45	PC-5  	20	3/8    	Режущая	 	1	Нержавеющая сталь	0	1	36	7893,03	 	 	 	
W8976	PROLENE	PROLENE	1	  3/0 	90	V7 	26	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3234,22	 	 	 	
W8977	PROLENE	PROLENE	1	  2/0 	90	V7  	26	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	2	12	3140,02	 	 	 	
W8998	PROLENE	PROLENE	1	1	100	CTB 	40	1/2	тупоконечная	 	1	Нержавеющая сталь	0	1	12	2336,41	 	 	 	
W982	PROLENE	PROLENE	1	0	100	V-38 	31	Игла-крючок	колюще-режущая	 	1	Нержавеющая сталь	0	1	12	1960,09	 	 	 	
X1004G	PROLENE	PROLENE	1	7/0	60	CC-6  	12	1/2	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	12	12169,81	 	 	 	
X1012H	PROLENE	PROLENE	1	 6/0	75	CC VB 	9,3	3/8    	Колющая игла с микрозаточкой	 	1	Нержавеющая сталь	0	2	36	43275,67	 	 	 	
KBL5690JG	PROLENE	PROLENE	1	 6/0	90	C-1  	13	3/8    	Колющая	 	2	Нержавеющая сталь	0	2	12	16335,25	 	 	 	
HFL5671JG	PROLENE	PROLENE	1	 6/0	75	CC 	9,3	3/8    	Колющая игла с микрозаточкой	 	2	Нержавеющая сталь	0	2	12	18612,17	 	 	 	
X967G	PROLENE	PROLENE	1	7/0	60	BV175-8	9,3	3/8    	Колющая	 	4	Нержавеющая сталь	0	2	12	69885,77	 	 	 	
X535G	PROLENE	PROLENE	1	 6/0	75	CC-1 MP 	13	3/8    	Колющая игла с микрозаточкой	 	2	Нержавеющая сталь	0	2	12	15258,77	 	 	 	
JV522	VICRYL	VICRYL	1	1	90	V-39	45	1/2	колюще-режущая	 	1	Нержавеющая сталь	0	1	36	7516,89	 	VCP9377H	 	";

	public function anyMain(){

		ini_set('memory_limit', '256M'); 

	 	$result = 0;

	 	$input = explode("\n", $this->main_data);

	 	$result = '';

	 	/////////////////////////////////////////////////////////////////////////////////////////////////
	 	/*
	 	 	Набор прошлых значений, для обработки пустых строк $cur_%поле%
	 	 	Порядок перечисления переменных соответствует порядку строк в таблице БД

	 	*/
	 	//id - не может повторяться
	 	//code - не может повторяться
	 	//product_id - всегда должен быть
	 	//description - всегда должен быть

	 	$cur_string_size = '';//varchar(255)
	 	$cur_string_length = '';//varchar(255)

	 	$cur_needle_code = '';//varchar(255)
	 	$cur_needle_size = 0.0;//float
	 	$cur_needle_curveness = '';//varchar(255)
	 	$cur_needle_color = 0;//enum(0,1)
	 	$cur_needle_count = 0;//int

	 	$cur_string_colored = 0;//enum(0,1,2)
	 	$cur_string_count = 0;//int

	 	$cur_is_ethalloy = 0;//int - 0/1

	 	$cur_pack_size = 0;//int

	 	//price - не может повторяться
	 	//needle_picture_id - зарезервирована

	 	$cur_needle_type = 0;//int - по строке из needle_type
	 	$cur_needle_subtype ='';//varchar

	 	//notes - не может повторяться

	 	/////////////////////////////////////////////////////////////////////////////////////////////////

	 	//////////////////////////
	 	// Порядок столбцов и соответствие их БД
	 	//////////////////////////

	 	/*
	 	0 - code
	 	1 - description
	 	2 - product_id (строковая сверка с ProductCategory)
	 	3 - string_colored (0,1,2 = нет, да, частично)
	 	4 - string_size
	 	5 - string_length
	 	6 - needle_code
	 	8 - needle_subtype

	 	7 - needle_size
	 	9 - needle_curveness
	 	10 - needle_type
	 	11 - //пиктограмма, пропускаем
	 	12 - string_count
	 	13 - is_ethalloy //Ethalloy => 1/Нержавеющая сталь => 0
	 	14 - needle_color //1 - Visiblack, 0 - обычная
	 	15 - needle_count
	 	
	 	16 - pack_size
	 	17 - price
	 	28 - notes
	 	19 - similar1
	 	20 - similar2

	 	*/

	 	//FIXME: привести в порядок модели и убрать
	 	Eloquent::unguard();

	 	//Выключаем все старые элементы, касающиеся данной продуктовой категории
        $affected = DB::table('items_main')->update(array('enabled' => 0));

	 	//1 Этап. Нормируем массив:убираем элементы, где не хватает длины или нет кода, убираем заголовок
	 	$basedata = array();
	 	for($i = 0; $i < count($input);$i++){
	 	 	$input[$i] = explode("\t", $input[$i]);
	 	 	if(count($input[$i]) > 10){
	 	 	 	if($input[$i][0] != "Код" && $input[$i][0] != '' && $input[$i][0] != null){
	 	 	 	 	array_push($basedata, $input[$i]);
	 	 	 	}
	 	 	}
	 	}

	 	if(count($basedata) == 0){
	 		return false;
	 	}

	 	//2 Этап. Проставляем типы продукции и типы нити. Если появились новые - добавляем в базу
	 	for($i = 0; $i < count($basedata);$i++){
	 	 	$tmp = $basedata[$i];
	 	 	//
	 	 	$product = $tmp[2];
	 	 	$category = 0;
	 	 	try{
	 	 	 	$cat = ProductCategory::firstOrCreate(array('export_id' => $product,'type' => 'main'));
	 	 	 	$category = $cat->id;

	 	 	}catch(ErrorException $e){
	 	 	 	//Не должно быть кроме смерти БД
	 	 	}

	 	 	$needle_type = $tmp[9];

	 	 	if(strrpos($needle_type," PRIME") > 1){
	 	 		$needle_type = explode(" PRIME", $needle_type)[0];
	 	 		$basedata[$i][20] = "PRIME";
	 	 	}else if(strrpos($needle_type," PLUS") > 1){
	 	 		$needle_type = explode(" PLUS", $needle_type)[0];
	 	 		$basedata[$i][20] = "PLUS";
	 	 	}else{
	 	 		$basedata[$i][20] = "";
	 	 	}

	 	 	//PRIME, PLUS

	 	 	$n_type = 0;
	 	 	try{
	 	 	 	$n_type = NeedleType::firstOrCreate(array('name' => $needle_type));
	 	 	 	$needle_type = $n_type->id;

	 	 	}catch(ErrorException $e){
	 	 	 	//Не должно быть кроме смерти БД
	 	 	}

	 	 	$result = $result.$basedata[$i][0].' '.$basedata[$i][2].' '.$basedata[$i][9]."\n";

	 	 	$basedata[$i][2] = $category;
	 	 	$basedata[$i][9] = $needle_type;
	 	}

	 	//3 Этап. Льем продукты в базу по коду.
	 	for($i = 0; $i < count($basedata);$i++){
	 	 	$cur = $basedata[$i];
	 	 	$item = MainProduct::firstOrCreate(array('code' => $cur[0]));
	 	 	$item->description = $cur[1];
	 	 	$item->product_id = $cur[2];
	 	 	$item->string_colored = intval($cur[3],10);
	 	 	$item->string_size = trim($cur[4]," ");
	 	 	$item->string_length = $cur[5];
	 	 	$item->needle_code = $cur[6];
	 	 	$item->needle_subtype = $cur[20];
	 	 	$item->needle_size = floatval(str_replace(',', '.',$cur[7]));
	 	 	$item->needle_curveness = $cur[8];
	 	 	$item->needle_type = intval($cur[9],10);
	 	 	$item->string_count = intval($cur[11],10);
	 	 	$item->is_ethalloy = (($cur[12] == "Ethalloy")?1:0);//Обработка некорректных значений
	 	 	$item->steel_type = trim($cur[12]," \n");
	 	 	$item->needle_color = (($cur[13] == "1")?'1':'0');//Обработка некорректных значений
	 	 	$item->needle_count = intval($cur[14],10);
	 	 	$item->pack_size = intval($cur[15],10);
	 	 	$item->price = floatval(str_replace(',', '.',$cur[16]));
	 	 	$item->notes = $cur[17];
	 	 	
	 	 	try{
		 	 	if(count($cur) >17){
		 	 		$item->similar1 = $cur[18];
		 	 		$item->similar2 = $cur[19];
		 	 	}else{
		 	 		$item->similar1 = '';
		 	 		$item->similar2 = '';
		 	 	}
		 	 }catch (Exception $e){
		 	 	$item->similar1 = '';
		 	 	$item->similar2 = '';
		 	 }

	 	 	$item->enabled = 1;

	 	 	$item->save();
	 	}

	 	/*
	0 - code
	 	1 - description
	 	2 - product_id (строковая сверка с ProductCategory)
	 	3 - string_colored (0,1,2 = нет, да, частично)
	 	4 - string_size
	 	5 - string_length
	 	6 - needle_code
	 	8 - needle_subtype

	 	7 - needle_size
	 	9 - needle_curveness
	 	10 - needle_type
	 	11 - //пиктограмма, пропускаем
	 	12 - string_count
	 	13 - is_ethalloy //Ethalloy => 1/Нержавеющая сталь => 0
	 	14 - needle_color //1 - Visiblack, 0 - обычная
	 	15 - needle_count
	 	
	 	16 - pack_size
	 	17 - price
	 	28 - notes
	 	19 - similar1
	 	20 - similar2
	 	*/

	 	/////////////////////////////////////////////
	 	//Отдаем json с продуктами
	 	////////////////////////////////////////////


	 	//////////Апдейт Changelog


        $newChange = new ChangelogItem;
        $newChange->time = time();
        $newChange->description = 'Обновлена продукция: шовные материалы';
        $newChange->save();

	 	////

	 	return json_encode(array(
	 	 	'status' => 'success', 
	 	 	'products_in_db' => MainProduct::count()
	 	 	), JSON_UNESCAPED_UNICODE);

	}


	






	public function anyMainjson(){
	 	$maindata = array();

	 	$products = MainProduct::all();

	 	foreach ($products as $product) {
	 	 	$nxt = array();

	 	 	$ct = ProductCategory::find($product->product_id);
	 	 	$nt = NeedleType::find($product->needle_type);

	 	 	$nxt["product_id"] = $ct->export_id;
	 	 	$nxt["code"] = $product->code;
	 	 	$nxt["description"] = $product->description;
	 	 	$nxt["string_size"] = $product->string_size;
	 	 	$nxt["string_length"] = $product->string_length;
	 	 	$nxt["string_colored"] = $product->string_colored;
	 	 	$nxt["string_count"] = $product->string_count;
	 	 	$nxt["needle_code"] = $product->needle_code;
	 	 	$nxt["needle_size"] = $product->needle_size;
	 	 	$nxt["needle_curveness"] = $product->needle_curveness;
	 	 	$nxt["needle_color"] = $product->needle_color;
	 	 	$nxt["needle_type"] = $nt->name;
	 	 	$nxt["needle_subtype"] = $product->needle_subtype;
	 	 	$nxt["needle_count"] = $product->needle_count;
	 	 	$nxt["is_ethalloy"] = (($product->is_ethalloy == 1)? true:false);
	 	 	$nxt["steel_type"] = $product->steel_type;
	 	 	$nxt["pack_size"] = $product->pack_size;
	 	 	$nxt["price"] = $product->price;
	 	 	$nxt["notes"] = $product->notes;

	 	 	array_push($maindata, $nxt);
	 	}


	 	return json_encode(array(
	 	 	'items' => $maindata
	 	 	), JSON_UNESCAPED_UNICODE);
	}
	/*
	 	JSON: Прочие продукты (клеи, сетки, дренажи,...)
	*/
	public function anyOtherjson(){
	 	$otherdata = array();

	 	$products = OtherProduct::all();

	 	foreach ($products as $product) {
	 	 	$nxt = array();

	 	 	$ct = ProductCategory::find($product->product_id);

	 	 	$nxt["product_id"] = $ct->export_id;
	 	 	$nxt["code"] = $product->code;
	 	 	$nxt["name"] = $product->name;

	 	 	//Для клеев и сеток описание отсутствует согласно API v1.0
	 	 	if(($ct->export_id != "SkinGlue") && ($ct->export_id != "MashImplants")){
	 	 	 	$nxt["description"] = $product->description;
	 	 	}

	 	 	//Форма сетки - только для сеток, согласно API v1.0
	 	 	if($ct->export_id == "MashImplants"){
	 	 	 	$nxt["mash_shape"] = $product->mash_shape;
	 	 	}

	 	 	//Длина раны - только для клеев, согласно API v1.0
	 	 	if($ct->export_id == "SkinGlue"){
	 	 	 	$nxt["glue_wound_length"] = $product->glue_wound_length;
	 	 	}

	 	 	//Строка, т.к. единицы измерения различны
	 	 	$nxt["size"] = $product->size;

	 	 	$nxt["pack_size"] = $product->pack_size;
	 	 	$nxt["price"] = $product->price;

	 	 	array_push($otherdata, $nxt);
	 	}

	 	return json_encode(array(
	 	 	'items' => $otherdata
	 	 	), JSON_UNESCAPED_UNICODE);
	}

	public function anyDocumentjson(){
	 	$docdata = array();

	 	$docs = Document::all();

	 	foreach ($docs as $doc) {
	 	 	$nxt = array();
	 	 	$nxt["id"] = $doc->id;
	 	 	$nxt["filename"] = $doc->filename;
	 	 	$nxt["url"] = $doc->url;
	 	 	array_push($docdata, $nxt);
	 	}

	 	return json_encode(array(
	 	 	'items' => $docdata
	 	 	), JSON_UNESCAPED_UNICODE);

	}






}
