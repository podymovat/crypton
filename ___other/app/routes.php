<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::controller('/api/catalog', 'CatalogController');

Route::controller('/api/feedback', 'FeedbackController');

Route::controller('/parse','ParserController');


//Route::controller('/admin','CoreAdminController@show');

Route::any('/login','LoginController@showLogin');
Route::any('/logout','LoginController@logout');
Route::any('/gologin','LoginController@authUser');


Route::get('/admin/docs','DocumentAdminController@show');



Route::get('/products/main','ProductController@showMain');

Route::get('/products/other','ProductController@showOther');

Route::get('/products/upload','ProductController@upload');
Route::post('/products/upload','ProductController@upload');



//Dev only!
//Генерация базы изображений игл из ФС
//Route::get('/generateNeedlePics','AdminController@generateNeedlePics');
//

Route::group(array('before' => 'auth'), function() {
    Route::get('/admin','AdminController@index');

    Route::get('/admin/products-main','AdminController@productsMain');
    Route::get('/admin/products-mesh','AdminController@productsMesh');
    Route::get('/admin/products-glue','AdminController@productsGlue');
    Route::get('/admin/products-drainage','AdminController@productsDrainage');
    Route::get('/admin/products-other','AdminController@productsOther');

    

    Route::get('/admin/documents','AdminController@documentsView');
    Route::get('/admin/media','AdminController@mediaView');
    Route::get('/admin/needlepics','AdminController@needlePicsView');

    Route::get('/admin/producttexts','AdminController@needleProductTextsView');
    Route::get('/admin/producttexts/edit','AdminController@needleProductTextEdit');
    Route::post('/admin/producttexts/edit','AdminController@needleProductTextChange');

    Route::post('/admin/docs/upload','DocumentAdminController@upload');
    Route::post('/admin/media/upload','DocumentAdminController@uploadMedia');
    Route::post('/admin/main/upload','DocumentAdminController@uploadMain');   
	Route::post('/admin/others/upload','DocumentAdminController@uploadOthers');

	Route::post('/admin/needlepics/create','DocumentAdminController@createNeedlePic');
	Route::get('/admin/needlepics/edit','DocumentAdminController@editNeedlePic');
	Route::post('/admin/needlepics/delete','DocumentAdminController@deleteNeedlePic'); 

	Route::get('/admin/products-main/edit','AdminController@editMainProduct');
	Route::get('/admin/products-other/edit','AdminController@editOtherProduct');

	Route::post('/admin/products-main/edit','AdminController@changeMainProduct');
	Route::post('/admin/products-other/edit','AdminController@changeOtherProduct');
	Route::post('/admin/needlepics/edit','DocumentAdminController@changeNeedlePic');

	Route::get('/admin/docs/delete','DocumentAdminController@deleteDocument');
	Route::get('/admin/media/delete','DocumentAdminController@deleteMedia');
    Route::get('/admin/needle/delete','DocumentAdminController@deleteNeedlePic');     

	Route::get('/admin/product','AdminController@showProduct');

    Route::get('/admin/mediaload','AdminController@documentLoad');
    Route::get('/admin/documentload','AdminController@mediaLoad');
});
