<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Ethicon - управление контентом приложения</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Ethicon Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Инструкция</a></li>
            <li><a href="/logout">Выйти</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="/admin">Главная</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <?php
            if($page == 'products-main'){
              echo '<li class="active"><a href="/admin/products-main">Шовные материалы</a></li>';
            }else{
              echo '<li><a href="/admin/products-main">Шовные материалы</a></li>';
            }
            if($page == 'products-mesh'){
              echo '<li class="active"><a href="/admin/products-mesh">Сетки</a></li>';
            }else{
              echo '<li><a href="/admin/products-mesh">Сетки</a></li>';
            }
            if($page == 'products-drainage'){
              echo '<li class="active"><a href="/admin/products-drainage">Дренажи</a></li>';
            }else{
              echo '<li><a href="/admin/products-drainage">Дренажи</a></li>';
            }
            if($page == 'products-glue'){
              echo '<li class="active"><a href="/admin/products-glue">Кожные клеи</a></li>';
            }else{
              echo '<li><a href="/admin/products-glue">Кожные клеи</a></li>';
            }
            if($page == 'products-other'){
              echo '<li class="active"><a href="/admin/products-other">Прочие</a></li>';
            }else{
              echo '<li><a href="/admin/products-other">Прочие</a></li>';
            }          
            

            ?>
          </ul>
          <ul class="nav nav-sidebar">
          <?php
            if($page == 'documents-view'){
              echo '<li class="active"><a href="/admin/documents">Документы</a></li>';
            }else{
              echo '<li><a href="/admin/documents">Документы</a></li>';
            } 
            if($page == 'media-view'){
              echo '<li class="active"><a href="/admin/media">Медиафайлы</a></li>';
            }else{
              echo '<li><a href="/admin/media">Медиафайлы</a></li>';
            }
            if($page == 'needlepics-view'){
              echo '<li class="active"><a href="/admin/needlepics">Изображения игл</a></li>';
            }else{
              echo '<li><a href="/admin/needlepics">Изображения игл</a></li>';
            }            
            
          ?>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Управление контентом iPad каталога</h1>
          <div class="panel panel-default">
            <a class="btn btn-default" href="/admin/products-main" role="button">Назад к списку продуктов</a>
          </div>

          <h2 class="sub-header"><?php echo htmlspecialchars($pagename); ?></h2>
          <div class="table-responsive">
            <table class="table table-striped">
            <?php
              //Заголовок таблицы

              echo '<thead><tr>';
              if(isset($columns)){
                for($i = 0;$i<count($columns);$i++){
                  echo '<th>'.$columns[$i].'</th>';
                }
              }
              echo '</tr></thead>';

              ////

              //Тело таблицы
              echo '<tbody>';

              if(isset($values)){
                for($i = 0;$i<count($values);$i++){
                  echo '<tr>';

                  for($j = 0;$j<count($values[$i]);$j++){
                    //Вывод здесь не HTML-safe, так как выводятся кнопки скачивания-редактирования-удаления
                    echo '<td>'.$values[$i][$j].'</td>';
                  }

                  echo '</tr>';
                }
              }

              ///

              echo '</tbody>';
            ?>               
              
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../js/vendor/holder.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
