<?php

class AdminController extends BaseController {

	const ERROR_400 = "Не удалось обработать сообщение об ошибке";

	const ERROR_405 = "Метод не поддерживается";

	const ERROR_421 = "Размер архива слишком велик";

	const ERROR_429 = "Повторите запрос позже";

	const ERROR_500 = "Неизвестная ошибка";

    //

    //Предустановленные категории продуктов
    //Шовные                                                     | product_id = 1
    //Сетка: code, name, mash_shape, size, pack_size, price      | product_id = 4
    //Дренаж: code, name, description, size, pack_size, price    | product_id = 2
    //Клей:code, name, size, glue_wound_length, pack_size, price | product_id = 3
    //Прочие: code, name, description, size, pack_size, price    | product_id = 5

    const CATEGORY_MAIN = 1;
    const CATEGORY_DRAIANGE = 2; 
    const CATEGORY_GLUE = 3;
    const CATEGORY_MESH = 4;
    const CATEGORY_SPECIAL = 5;

    //////////////////////////////////////////////////////////////////////////////////////
    ///Главная страница
    //////////////////////////////////////////////////////////////////////////////////////
    public function index(){
        $maincount = MainProduct::where('enabled','=', 1)->count();
        $othercount = OtherProduct::where('enabled','=', 1)->count();
        $documentcount = Document::count();
        $mediacount = MediaFile::count();

        $columns = array('Дата','Изменение');
        $values = array();

        $changes = ChangelogItem::orderBy('time','DESC')->get();

        foreach ($changes as $change) {
            $date = new DateTime();
            $date->setTimestamp($change->time);
            $date->add(new DateInterval('PT3H'));

            $values[] = array($date->format('Y-m-d H:i:s'),$change->description);
        }


        return View::make('adminIndex',
            array(
                'maincount' => $maincount, 
                'othercount' => $othercount, 
                'documentcount' => $documentcount, 
                'mediacount' => $mediacount, 
                'columns' => $columns,
                'left_menu' => View::make('parts._left_menu'),
                'values' => $values,'success' => htmlspecialchars(Input::get('success'))));
    }

    public function productsMain(){

        $columns = array('id','Код','Продукт','Тип иглы','Число игл','Число в упаковке', 'Цена, р','');
        $values = array();

        $products = MainProduct::where('enabled','=',1)->get();

        foreach ($products as $product) {
             $values[] = array(
                $product->id,
                '<a href="/admin/product?code='.($product->code).'" >'.$product->code.'</a>',
                ProductCategory::find($product->product_id)->export_id,
                $product->needle_code,
                $product->needle_count,
                $product->pack_size,
                number_format($product->price,2,',',' '),
                 '<a class="btn btn-default" href="/admin/products-main/edit?code='.($product->code).'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                );
        }

        return View::make('adminList',array('page' => 'products-main', 'pagename' => 'Шовные материалы', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));        
    }

    public function productsMesh(){

        $columns = array('id','Код','Имя','Форма сетки','Размер','Число в упаковке', 'Цена, р','');
        $values = array();

        $products = OtherProduct::where('product_id','=',4)->where('enabled','=',1)->get();

        foreach ($products as $product) {
             $values[] = array(
                $product->id,
                $product->code,
                $product->name,
                $product->mash_shape,
                $product->size,$product->pack_size,
                number_format($product->price,2,',',' '),
                '<a class="btn btn-default" href="/admin/products-other/edit?code='.($product->code).'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                );
        }

        return View::make('adminList',array('page' => 'products-mesh', 'pagename' => 'Сетки', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));        
    }

    public function productsDrainage(){

        $columns = array('id','Код','Имя','Описание','Размер','Число в упаковке', 'Цена, р','');
        $values = array();

        $products = OtherProduct::where('product_id','=',2)->where('enabled','=',1)->get();

        foreach ($products as $product) {
             $values[] = array(
                $product->id,
                $product->code,
                $product->name,
                $product->description_ru,
                $product->size,
                $product->pack_size,
                number_format($product->price,2,',',' '),
                '<a class="btn btn-default" href="/admin/products-other/edit?code='.($product->code).'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                );
        }


        return View::make('adminList',array('page' => 'products-drainage','pagename' => 'Дренажи', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));        
    }

    public function productsGlue(){

        $columns = array('id','Код','Имя','Объем','Длина раны','Число в упаковке', 'Цена, р',' ');
        $values = array();

        $products = OtherProduct::where('product_id','=',3)->where('enabled','=',1)->get();

        foreach ($products as $product) {
             $values[] = array(
                $product->id,
                $product->code,
                $product->name,
                $product->size,
                $product->glue_wound_length,
                $product->pack_size,
                number_format($product->price,2,',',' '),
                '<a class="btn btn-default" href="/admin/products-other/edit?code='.($product->code).'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                );
        }

        return View::make('adminList',array('page' => 'products-glue', 'pagename' => 'Кожные клеи', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));        
    }

    public function productsOther(){

        $columns = array('id','Код','Имя','Описание','Размер','Число в упаковке', 'Цена, р','');
        $values = array();

        $products = OtherProduct::where('product_id','=',5)->where('enabled','=',1)->get();

        foreach ($products as $product) {
             $values[] = array(
                $product->id,$product->code,
                $product->name,
                $product->description_ru,
                $product->size,
                $product->pack_size,
                number_format($product->price,2,',',' '),
                '<a class="btn btn-default" href="/admin/products-other/edit?code='.($product->code).'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                );
        }

        return View::make('adminList',array('page' => 'products-other', 'pagename' => 'Прочие продукты', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));        
    }

    public function documentsView(){

        $columns = array('id','Имя документа','Файл','');
        $values = array();

        $docs = Document::all();

        foreach ($docs as $doc) {
            $values[] = array(
                $doc->id,$doc->filename,
                "<!--a href='/admin/documentload?id=".$doc->id."' target='_blank'-->".($doc->url)."<!--/a-->",
                '<a class="btn btn-default" href="#">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
                <a href="/admin/docs/delete?id='.$doc->id.'" class="btn btn-default" href="#">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>'
                );
        }

        return View::make('adminList',array('page' => 'documents-view', 'pagename' => 'Документы', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));        
    }

    public function mediaView(){

        $columns = array('id','Продукт','Файл', 'Папка','');
        $values = array();

        $docs = MediaFile::all();

        $products = ProductCategory::orderBy('export_id', 'asc')->get();
        $folders = Folder::all();

        foreach ($docs as $doc) {
            $values[] = array(
                $doc->id,
                MediaFile::getProducts($doc->id),
                "<!--a href='/admin/mediaload?id=".$doc->id."' target='_blank'-->".($doc->file)."<!--/a-->",
                MediaFile::getFolders($doc->id),
                '<a class="btn btn-default" href="/admin/media/edit/'.$doc->id.'">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
                <a class="btn btn-default" href="#" onclick="ConfirmDeleteMedia('.$doc->id.');">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>'
                );
        }

        return View::make('adminList',array('page' => 'media-view', 'pagename' => 'Медиафайлы', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success')), 'products' => $products, 'folders' => $folders));        
    }

    public function needlePicsView(){
        $columns = array('id','Изображение в стандартном разрешении','Изображение в высоком разрешении','Имя','');
        $values = array();

        $pics = NeedlePicture::all();

        foreach ($pics as $pic) {

            $removeString = "javascript:removeObject('".($pic->id)."','needle')";

            $nameBase = ($pic->name);
            $is_double = ($pic->is_double);

            if($is_double == 1){
                $nameBase = $nameBase."-double";
            }

            $values[] = array(
                $pic->id,
                "<img class='needleimg' src='../needlepics/".($nameBase.".png")."?rand=".(rand(1,1000000))."'></img>",
                "<img class='needleimg' src='../needlepics/".($nameBase."@2x.png")."?rand=".(rand(1,1000000))."'></img>",
                $pic->name.(($is_double == 1)?('(double)'):('')),
                '<a class="btn btn-default" href="/admin/needlepics/edit?id='.($pic->id).'">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
                <a class="btn btn-default" href="'.$removeString.'">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>'
                );
        }

        return View::make('adminList',array('page' => 'needlepics', 'pagename' => 'Изображения игл', 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success')))); 
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //Только на время разработки, для первичной заливки контента
    //Генерирует готовую базу игл по залитым в папку материалам
    //
    public function generateNeedlePics(){
        $result = '';

        $directory = '/var/www/public/needleimg';
        $scanned_directory = scandir($directory);//array_diff(scandir($directory), array('..', '.'));

        $result = implode("\n", $scanned_directory);


        Eloquent::unguard();
        
        for($i = 0;$i<count($scanned_directory);$i++){

           $nameparts = explode("_", $scanned_directory[$i]);

            if(count($nameparts)<2){
                continue;
            }else{
                //Хвост. _original.png или iPad.png
                $last = array_pop($nameparts);
                //все остальное, меняем _ на пробелы
                $main = implode(" ", $nameparts);
                //
                $item = NeedlePicture::firstOrCreate(array('name' => $main));

                if($last == "original.png"){
                    $item->file_original = $scanned_directory[$i];
                }else if(($last == "iPad.png") || ($last == "ipad.png")){
                    $item->file_ipad = $scanned_directory[$i];
                }

                $item->save();
            }            
        }


        return $result;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function showProduct(){

        $columns = array('Поле','Значение');
        $values = array();

        $product = MainProduct::where('code','=',Input::get('code'))->first();

        if(!$product){
            return Redirect::to('/admin/products-main');
        }

        $values[] = array('id',$product->id);
        $values[] = array('Код',$product->code);
        $values[] = array('Описание',$product->description);
        $values[] = array('   ','   ');
        $values[] = array('Размер нити',$product->string_size);
        $values[] = array('Длина нити',$product->string_length);
        $values[] = array('Окрашенность нити',$product->string_colored);
        $values[] = array('Число нитей',$product->string_count);
        $values[] = array('   ','   ');
        $values[] = array('Код иглы',$product->needle_code);
        $values[] = array('Размер иглы',$product->needle_size);
        $values[] = array('Кривизна иглы',$product->needle_curveness);
        $values[] = array('Цвет иглы',$product->needle_color);
        $values[] = array('Подтип иглы',$product->needle_subtype);
        $values[] = array('Число игл',$product->needle_count);
        $values[] = array('Тип стали',$product->steel_type);
        $values[] = array('   ','   ');
        $values[] = array('Количество в упаковке',$product->pack_size);
        $values[] = array('Цена, р.',number_format($product->price,2,',',' '));
        $values[] = array('Примечание',$product->notes);
        $values[] = array('   ','   ');
        $values[] = array('Похожий продукт 1',$product->similar1);
        $values[] = array('Похожий продукт 2',$product->similar2);


        return View::make('adminItem',array('page' => 'item-view', 'pagename' => 'Информация о продукте', 'columns' => $columns,'values' => $values));
    }

    public function editMainProduct(){
        $columns = array('Поле','Значение');
        $values = array();

        $product = MainProduct::where('code','=',Input::get('code'))->first();

        if(!$product){
            return Redirect::to('/admin');
        }

        $values[] = array('id',$product->id);
        $values[] = array('Код',$product->code);
        $values[] = array('Продукт',$product->description);
        $values[] = array('   ','   ');
        $values[] = array('Размер нити','<input type="text" class="form-control" name="string_size" value="'.($product->string_size).'"></input>');
        $values[] = array('Длина нити','<input type="text" class="form-control" name="string_length" value="'.($product->string_length).'"></input>');
        $values[] = array('Окрашенность нити',$this->stringColoredHTML($product->string_colored));
        $values[] = array('Число нитей','<input type="number" class="form-control" name="string_count" value="'.($product->string_count).'" min="0" max="100000"></input>');
        $values[] = array('   ','   ');
        $values[] = array('Код иглы','<input type="text" class="form-control" name="needle_code" value="'.($product->needle_code).'"></input>');
        $values[] = array('Размер иглы','<input type="text" class="form-control" name="needle_size" value="'.($product->needle_size).'"></input>');
        $values[] = array('Кривизна иглы','<input type="text" class="form-control" name="needle_curveness" value="'.($product->needle_curveness).'"></input>');
        $values[] = array('Цвет иглы',$this->needleColorHTML($product->needle_color));
        $values[] = array('Число игл',$this->needleCountHTML($product->needle_count));
        $values[] = array('Тип стали','<input type="text" class="form-control" name="steel_type" value="'.($product->steel_type).'"></input>');
        $values[] = array('   ','   ');

        $values[] = array('Количество в упаковке','<input type="text" class="form-control" name="pack_size" value="'.($product->pack_size).'"></input>');
        $values[] = array('Цена, р.','<input type="text" class="form-control" name="price" value="'.($product->price).'"></input>');

        $values[] = array('Примечание','<input type="text" class="form-control" name="notes" value="'.($product->notes).'"></input>');
        $values[] = array('   ','   ');
        $values[] = array('Похожий продукт 1','<input type="text" class="form-control" name="similar1" value="'.($product->similar1).'"></input>');
        $values[] = array('Похожий продукт 2','<input type="text" class="form-control" name="similar2" value="'.($product->similar2).'"></input>');

        return View::make('editItemMain',array('page' => 'item-edit', 'pagename' => 'Информация о продукте', 'code' => ($product->code), 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));
    }


    private function productHTML($value){
        $result = "<select name='product_id' class='form-control'>";
        //TBD
        $result.= "</select>";
        return $result;
    }

    /*
        Генерация выпадающего списка редактирования количества игл у шовного материала
    */
    private function needleCountHTML($value){
        $result = "<select name='needle_count' class='form-control'>";
        $result.= ("<option  class='form-control' value='0'".(($value == 0)? " selected":"").">(Нет)</option>");
        $result.= ("<option  class='form-control' value='1'".(($value == 1)? " selected":"").">1</option>");
        $result.= ("<option  class='form-control' value='2'".(($value == 2)? " selected":"").">2</option>");
        $result.= "</select>";
        return $result;
    }


    private function needleColorHTML($value){
        $result = "<select name='needle_color' class='form-control'>";
        $result.= ("<option  class='form-control' value='0'".(($value == 0)? " selected":"").">Обычный</option>");
        $result.= ("<option  class='form-control' value='1'".(($value == 1)? " selected":"").">Visiblack</option>");
        $result.= "</select>";
        return $result;
    }

    private function stringColoredHTML($value){
        $result = "<select name='string_colored' class='form-control'>";
        $result.= ("<option  class='form-control' value='0'".(($value == 0)? " selected":"").">Не окрашены</option>");
        $result.= ("<option  class='form-control' value='1'".(($value == 1)? " selected":"").">Окрашены</option>");
        $result.= ("<option  class='form-control' value='2'".(($value == 2)? " selected":"").">Половина нитей окрашены</option>");
        $result.= "</select>";
        return $result;
    }


    public function editOtherProduct(){
        $columns = array('Поле','Значение');
        $values = array();

        $product = OtherProduct::where('code','=',Input::get('code'))->first();

        if(!$product){
            return Redirect::to('/admin');
        }

        $values[] = array('id',$product->id);
        $values[] = array('Код',$product->code);
        $values[] = array('Имя','<input type="text" class="form-control" name="name" value="'.$product->name.'"></input>');

        //Сетка: code, name, mash_shape, size, pack_size, price      | product_id = 4
        //Дренаж: code, name, description, size, pack_size, price    | product_id = 2
        //Клей:code, name, size, glue_wound_length, pack_size, price | product_id = 3
        //Прочие: code, name, description, size, pack_size, price    | product_id = 5

        $category = $product->product_id;


        if($category == self::CATEGORY_MESH){
             $returnpage = 'products-mesh';
             $values[] = array('Категория','Сетка');
             $values[] = array('Форма сетки','<input type="text" class="form-control" name="mash_shape" value="'.$product->mash_shape.'"></input>');
             $values[] = array('Размер','<input type="text" class="form-control" name="size" value="'.$product->size.'"></input>');
        }else if($category == self::CATEGORY_GLUE){
            $returnpage = 'products-glue';
            $values[] = array('Категория','Кожный клей');
            $values[] = array('Длина раны','<input type="text" class="form-control" name="glue_wound_length" value="'.$product->glue_wound_length.'"></input>');
            $values[] = array('Объем','<input type="text" class="form-control" name="size" value="'.$product->size.'"></input>');
        }else if($category == self::CATEGORY_DRAIANGE){
            $returnpage = 'products-drainage';
            $values[] = array('Категория','Дренаж');
            $values[] = array('Описание','<input type="text" class="form-control" name="description" value="'.$product->description.'"></input>');
            $values[] = array('Размер','<input type="text" class="form-control" name="size" value="'.$product->size.'"></input>');
        }else if($category == self::CATEGORY_SPECIAL){
            $returnpage = 'products-other';
            $values[] = array('Категория','Специальный продукт');
            $values[] = array('Описание','<input type="text" class="form-control" name="description" value="'.$product->description.'"></input>');
            $values[] = array('Размер','<input type="text" class="form-control" name="size" value="'.$product->size.'"></input>');
        }else{
            //Ненормальный продукт из неверной категории
            return Redirect::to('/admin');
        }


        $values[] = array('Количество в упаковке','<input type="text" class="form-control" name="pack_size" value="'.$product->pack_size.'"></input>');
        $values[] = array('Цена, р.','<input type="text" class="form-control" name="price" value="'.($product->price).'"></input>');


        return View::make('editItem',array('page' => 'item-edit', 'pagename' => 'Информация о продукте', 'code' => ($product->code), 'returnpage' => $returnpage, 'columns' => $columns,'values' => $values,'success' => htmlspecialchars(Input::get('success'))));
    }

    public function changeMainProduct(){
        $product = MainProduct::where('code','=',Input::get('code'))->first();

        if(!$product){
            return Redirect::to('/admin');
        }

        try{


            if(Input::get('steel_type')){
                $product->steel_type = trim(htmlspecialchars(Input::get('steel_type'))," ");
            }

            if(Input::get('string_count')){
                $product->string_count = intval(htmlspecialchars(Input::get('string_count')));
            }

            //string_count
                

            if(Input::get('string_length')){
                $product->string_length = trim(htmlspecialchars(Input::get('string_length'))," ");
            }   
            if(Input::get('string_size')){
                $product->string_size = trim(htmlspecialchars(Input::get('pack_size'))," ");
            }

            if(Input::get('pack_size')){
                $product->pack_size = htmlspecialchars(Input::get('pack_size'));
            }
            if(Input::get('price')){
                $product->price = trim(htmlspecialchars(Input::get('price'))," ");
            }
            if(Input::get('needle_curveness')){
                $product->needle_curveness = trim(htmlspecialchars(Input::get('needle_curveness'))," ");
            }
            if(Input::get('needle_count')){
                $product->needle_count = intval(htmlspecialchars(Input::get('needle_count')));
            }
            if(Input::get('needle_size')){
                $product->needle_size = floatval(htmlspecialchars(Input::get('needle_size')));
            }
            if(Input::get('needle_color')){
                $product->needle_color = intval(htmlspecialchars(Input::get('needle_color')));
            }
            if(Input::get('needle_code')){
                $product->needle_code = trim(htmlspecialchars(Input::get('needle_code'))," ");
            }
            if(Input::get('string_colored')){
                $product->string_colored = intval(htmlspecialchars(Input::get('string_colored')));
            }

            if(Input::get('notes')){
                $product->notes = htmlspecialchars(Input::get('notes'));
            }

            if(Input::get('similar1')){
                $product->similar1 = htmlspecialchars(Input::get('similar1'));
            }

            if(Input::get('similar2')){
                $product->similar2 = htmlspecialchars(Input::get('similar2'));
            }

            $product->save();

            //Записываем апдейт в лог

            $newChange = new ChangelogItem;
            $newChange->time = time();
            $newChange->description = 'Обновлен продукт: '.($product->code);
            $newChange->save();

        }catch(Exception $e){
            return Redirect::to('/admin/products-other/edit?code='.($product->code).'&success=false');
        }

        return Redirect::to('/admin/products-main/edit?code='.($product->code).'&success=true');

    }


    public function changeOtherProduct(){
        $product = OtherProduct::where('code','=',Input::get('code'))->first();

        if(!$product){
            return Redirect::to('/admin');
        }
        try{
            if(Input::get('name')){
                $product->name = htmlspecialchars(Input::get('name'));
            }
            if(Input::get('description')){
                $product->name = htmlspecialchars(Input::get('description'));
            }
            if(Input::get('size')){
                $product->size = htmlspecialchars(Input::get('size'));
            }
            if(Input::get('mash_shape')){
                $product->mash_shape = htmlspecialchars(Input::get('mash_shape'));
            }
            if(Input::get('glue_wound_length')){
                $product->glue_wound_length = htmlspecialchars(Input::get('glue_wound_length'));
            }
            if(Input::get('pack_size')){
                $product->pack_size = htmlspecialchars(Input::get('pack_size'));
            }
            if(Input::get('price')){
                $product->price = htmlspecialchars(Input::get('price'));
            }

            $product->save();

            //Записываем апдейт в лог

            $newChange = new ChangelogItem;
            $newChange->time = time();
            $newChange->description = 'Обновлен продукт: '.($product->code);
            $newChange->save();

        }catch(Exception $e){
            return Redirect::to('/admin/products-other/edit?code='.($product->code).'&success=false');
        }

        return Redirect::to('/admin/products-other/edit?code='.($product->code).'&success=true');
    }



    public function documentLoad(){
       // if(isset(Input::get('id'))){
            //TBD
      //  }else{
            return '';
       // }
    }

    public function mediaLoad(){
       
    }




    /*
		Пытаются вызвать что-то не то
    */
    public function missingMethod($parameters = array())
    {
        return $this->makeError(405);
    }

    /*
		Что-то пошло не так - формируем сообщение об ошибке
    */
    protected function makeError($code){

    	$message = "Unknown error";

    	switch ($code) {
    		case 400:
    			$message = self::ERROR_400;
    			break;

    		case 405:
    			$message = self::ERROR_405;
    			break;
    		
    		default:
    			$message = self::ERROR_500;
    			break;
    	}

    	return json_encode(array('status' => 'error', 'error_code' => ''.($code), 'error_message' => $message));
    }

}