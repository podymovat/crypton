<?php

class BaseController extends Controller {

    /**
     * Instantiate a new BaseController instance.
     */
    public function __construct() {
    	
    }


    protected function setLayout($params = array()) {
    	$success = htmlspecialchars(Input::get('success'));
    	$data = array_merge(array('page' => $this->page, 
    		                      'pagename' => $this->title,
                                  'head' => View::make('layoutes.parts._head'),
                                  'header' => View::make('layoutes.parts._header'),
    		                      'footer' => View::make('layoutes.parts._footer'),
    		                      'left_menu' => View::make('layoutes.parts._left_menu', array('page' => $this->page)),
    		                      'form' => View::make('layoutes.parts._form', array('page' => $this->page)),
    		                      'info' => View::make('layoutes.parts._info', array('page' => $this->page, 'success' => $success)),
    		                      'success' => $success), $params);
    	return View::make($this->layout, $data);      
    }

    protected function setPage($page) {
    	$this->page = $page;
    }    

    protected function setTitle($title) {
    	$this->title = $title;
    }   


    public function addChangeLog($text = null) {
        if (is_null($text)) return false;

        $newChange = new ChangelogItem;
        $newChange->time = time();
        $newChange->description = $text;
        $newChange->save();        
    }	


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	/*protected function setupLayout() {
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}*/


}
