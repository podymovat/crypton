<?php

class CatalogController extends BaseController {

	const ERROR_400 = "Не удалось обработать сообщение об ошибке";

	const ERROR_405 = "Метод не поддерживается";

	const ERROR_421 = "Размер архива слишком велик";

	const ERROR_429 = "Повторите запрос позже";

	const ERROR_500 = "Неизвестная ошибка";

    public function __construct() {
        $this->app_path = Config::get('paths.app');
        $this->app_host = Config::get('paths.host');
    }

    public function anyCurrentOld()
    {
        return json_encode(array("status" => "success","zip_url" => $this->app_host."/sync/0000000009.zip"));
    }

    public function json_cb(&$item, $key) { 
        if (is_string($item)) $item = mb_encode_numericentity($item, array (0x80, 0xffff, 0, 0xffff), 'UTF-8'); 
    }

    public function my_json_encode($arr){
        //convmap since 0x80 char codes so it takes all multibyte codes (above ASCII 127). So such characters are being "hidden" from normal json_encoding
        array_walk_recursive($arr, array($this, 'json_cb'));
        return mb_decode_numericentity(json_encode($arr), array (0x80, 0xffff, 0, 0xffff), 'UTF-8');

    }

    public function anyCurrent()
    {
        $result = '';

        $syncfolder = $this->app_path."/public/sync/";
        $testdatafolder = $this->app_path."/public/testdata/";
        $documentsfolder = $this->app_path."/catalogdata/documents/";
        $mediafolder = $this->app_path."/catalogdata/media/";
        $needlepicsfolder = $this->app_path."/public/needlepics/";


        //Проверяем, нет ли готового архива с самыми свежими изменениями

        $lastChange = ChangelogItem::orderBy('time','DESC')->first();

        if(file_exists($syncfolder.($lastChange->time).'.zip')){
            //Файл уже есть. Не перегенерируем ничего.
            $path = ($this->app_host."/sync/".($lastChange->time).".zip");
            return $this->my_json_encode(array("status" => "success","zip_url" => $path,"last_updated" => '"'.($lastChange->time).'"'));
        }

        //Файла не оказалось. Собираем.

        $date = new DateTime();

        $name = ($lastChange->time);

        $zip = new ZipArchive;
        $res = $zip->open($syncfolder.($name).'.zip', ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
        if ($res === TRUE) {
            $result = 'ok';
            //contents file
            $zip->addFile($testdatafolder.'index.json','index.json');
            //product lists
            $zip->addFromString('products-main/index.json', $this->getMainjson());
            $zip->addFromString('products-other/index.json', $this->getOtherjson());
            //

            //product descriptions
            $prods = ProductCategory::where('type','=','main')->get();

             foreach ($prods as $prod) {
                try{
                     $zip->addFromString('product_descriptions/'.($prod->export_id).'.html','<html>'.($prod->html_description).'</html>');
                }catch(Exception $e){

                }
            }



            //documents
            $zip->addFromString('documents/index.json',$this->getDocumentjson());
            //media (stub)
            $zip->addFromString('media/index.json',$this->getMediajson());

            $zip->addFromString('folder/index.json',$this->getFolderjson());
            //$zip->addFile($testdatafolder.'/media/index.json','media/index.json');
            //
            //add document files
            $docs = Document::all();
            foreach ($docs as $doc) {
                try{
                     $zip->addFile(($documentsfolder.($doc->id).'/'.($doc->url)),iconv('cp866', 'utf-8', ('documents/'.($doc->url))));
                }catch(Exception $e){

                }
            }

            $docs = MediaFile::all();
            foreach ($docs as $doc) {
                try{
                     $zip->addFile(($mediafolder.($doc->id).'/'.($doc->file)),iconv('cp866', 'utf-8', ('media/'.($doc->file))));
                }catch(Exception $e){

                }
            }

            //needle pics

            $needlepics = array_diff(scandir($needlepicsfolder), array('..', '.'));
            $info = "";

            if(count($needlepics) > 0){
                foreach ($needlepics as $npic) {
                    try{
                        $zip->addFile(($needlepicsfolder.$npic),('needlepics/'.$npic));
                       // $info.= "NPIC:: ".$npic." ".($zip->addFile(($needlepicsfolder.$npic),('needlepics/'.$npic)) == TRUE);
                    }catch(Exception $e){
                       // $info.= "NPIC:: ".$npic." FAIL! ";
                    }
                    
                }
               // $info = implode(" :: ", $needlepics);
            }else{
                return json_encode(array("status" => "success","neeldefail" => 1, "zip_url" => $path,"last_updated" => '"'.($lastChange->time).'"'));
            }
            //
            $zip->close();

            $path = ($this->app_host."/sync/".($name).".zip");

            return $this->my_json_encode(array("status" => "success","ninfo" => $info, "zip_url" => $path,"last_updated" => '"'.($lastChange->time).'"'));
        } else {
            $result = 'failed, code:' . $res;
            return makeError($res);
        }
        return $result;
    }

    public function trimNeedleCode($src){
        $result = $src;

        $result = trim(htmlspecialchars($src)," ");
        //Убираем лишние пробелы
        $result = trim(preg_replace('/\s+/',' ', $result));

        return $result;
    }

    public function getMainjson(){
        $maindata = array();

        $products = MainProduct::where('enabled','=','1')->get();

        foreach ($products as $product) {
            $nxt = array();

            $ct = ProductCategory::find($product->product_id);
            $nt = NeedleType::find($product->needle_type);

            $nxt["product_id"] = $ct->export_id;
            $nxt["code"] = $product->code;
            $nxt["description"] = $product->description;
            $nxt["string_size"] = $product->string_size;
            $nxt["string_length"] = $product->string_length;
            $nxt["string_colored"] = $product->string_colored;
            $nxt["string_count"] = $product->string_count;
            //Код иглы может содержать лишние пробелы и переносы. Вычищаем их для корректной работы приложения
            $nxt["needle_code"] = $this->trimNeedleCode($product->needle_code);
            $nxt["needle_size"] = $product->needle_size;
            $nxt["needle_curveness"] = $product->needle_curveness;
            $nxt["needle_color"] = $product->needle_color;
            $nxt["needle_type"] = $nt->name;
            $nxt["needle_subtype"] = $product->needle_subtype;
            $nxt["needle_count"] = $product->needle_count;
            $nxt["is_ethalloy"] = (($product->is_ethalloy == 1)? true:false);
            $nxt["steel_type"] = $product->steel_type;
            $nxt["pack_size"] = $product->pack_size;
            $nxt["price"] = $product->price;
            $nxt["notes"] = $product->notes;
            $nxt["similar1"] = $product->similar1;
            $nxt["similar2"] = $product->similar2;

            array_push($maindata, $nxt);
        }


        return $this->my_json_encode(array(
            'items' => $maindata
            ));
    }
    /*
        JSON: Прочие продукты (клеи, сетки, дренажи,...)
    */
    public function getOtherjson(){
        $otherdata = array();

        $products = OtherProduct::where('enabled','=','1')->get();

        foreach ($products as $product) {
            $nxt = array();

            $ct = ProductCategory::find($product->product_id);

            $nxt["product_id"] = $ct->export_id;
            $nxt["code"] = $product->code;
            $nxt["name"] = $product->name;

            //Для клеев и сеток описание отсутствует согласно API v1.0
            if(($ct->export_id != "SkinGlue") && ($ct->export_id != "MashImplants")){
                $nxt["description"] = $product->description_ru;
            }

            //Форма сетки - только для сеток, согласно API v1.0
            if($ct->export_id == "MashImplants"){
                $nxt["mash_shape"] = $product->mash_shape;
            }

            //Длина раны - только для клеев, согласно API v1.0
            if($ct->export_id == "SkinGlue"){
                $nxt["glue_wound_length"] = $product->glue_wound_length;
            }

            //Строка, т.к. единицы измерения различны
            $nxt["size"] = $product->size;

            $nxt["pack_size"] = $product->pack_size;
            $nxt["price"] = $product->price;

            array_push($otherdata, $nxt);
        }

        return $this->my_json_encode(array(
            'items' => $otherdata
            ));
    }

    public function getDocumentjson(){
        $docdata = array();

        $docs = Document::all();

        foreach ($docs as $doc) {
            $nxt = array();
            $nxt["id"] = $doc->id;
            $nxt["filename"] = $doc->filename;
            $nxt["url"] = $doc->url;
            array_push($docdata, $nxt);
        }

        return $this->my_json_encode(array(
            'items' => $docdata
            ));

    }

        public function getFolderjson(){
        $docdata = array();

        $docs = Folder::all();

        foreach ($docs as $doc) {
            $nxt = array();
            $nxt["id"] = $doc->id;
            $nxt["name"] = $doc->name;

            $medias = MediaFolders::where('folder_id', $doc->id)->get();
            
            if (!empty($medias)) {
                foreach ($medias as $media) { 
                    if(MediaFile::where('id', $media->media_id)->exists()) { 
                        $m = MediaFile::where('id', $media->media_id)->first();

                        $nxt["media"][] = array(
                                'id' => $m->id,
                                'url' => $m->file,
                                'products' => $this->getMediaProducts($m->id)
                            );                        
                    }

                }
            } else {
                $nxt["media"] = array();
            }

            //die(print_r($nxt));
            array_push($docdata, $nxt);
        }

        return $this->my_json_encode(array(
            'items' => $docdata
            ));

    }



    public function getMediaProducts($id) {
        $result = array();
        $products = MediaProducts::where('media_id', $id)->get();
        if (!empty($products)) {
            foreach ($products as $product) {
                if(ProductCategory::where('id', $product->product_id)->exists()) {
                    $result[] = array(
                            'product_id' => $product->product_id,
                            'product_name' => ProductCategory::find($product->product_id)->export_id
                        );
                }
            }
        } else {
            $result[] = array();
        }   
        return $result;     
    }



    public function getMediajson(){
        $docdata = array();

        $docs = MediaFile::all();

        foreach ($docs as $doc) {
            $nxt = array();
            $nxt["id"] = $doc->id;
            
            $products = MediaProducts::where('media_id', $doc->id)->get();
            if (!empty($products)) {
                foreach ($products as $product) {
                    if(ProductCategory::where('id', $product->product_id)->exists()) {
                        $nxt["products"][] = array(
                                'product_id' => $product->product_id,
                                'product_name' => ProductCategory::find($product->product_id)->export_id
                            );
                    }
                }
            } else {
                $nxt["products"] = array();
            }


            $folders = MediaFolders::where('media_id', $doc->id)->get();
            if (!empty($folders)) {
                foreach ($folders as $folder) {
                    if(Folder::where('id', $folder->folder_id)->exists()) {
                        $nxt["folders"][] = array(
                                'folder_id' => $folder->folder_id,
                                'folder_name' => Folder::find($folder->folder_id)->name
                            );                        
                    }

                }                
            } else {
                $nxt["folders"] = array();
            }

            $nxt["url"] = $doc->file;

            //die(print_r($nxt));
            array_push($docdata, $nxt);
        }

        return $this->my_json_encode(array(
            'items' => $docdata
            ));

    }

    /*
		Пытаются вызвать что-то не то
    */
    public function missingMethod($parameters = array())
    {
        return $this->makeError(405);
    }

    /*
		Что-то пошло не так - формируем сообщение об ошибке
    */
    protected function makeError($code){

    	$message = "Unknown error";

    	switch ($code) {
    		case 400:
    			$message = self::ERROR_400;
    			break;

    		case 405:
    			$message = self::ERROR_405;
    			break;
    		
    		default:
    			$message = self::ERROR_500;
    			break;
    	}

    	return $this->my_json_encode(array('status' => 'error', 'error_code' => ''.($code), 'error_message' => $message));
    }

}