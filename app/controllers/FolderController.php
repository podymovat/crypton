<?php

class FolderController extends BaseController {

    /**
     * Instantiate a new FolderController instance.
     */
    public function __construct() {
    	
    }


	public function index() {      
        $this->layout = 'layoutes.admin.list';  
		$this->setPage('folders-view');
		$this->setTitle('Папки');

		$columns = array('id','Название','');
        $values = array();

        $folders = Folder::all();

        //$products = ProductCategory::orderBy('export_id', 'asc')->get();

        foreach ($folders as $item) {
            $values[] = array(
                $item->id,
                $item->name,
                '<a class="btn btn-default" href="/admin/folders/edit/'.$item->id.'">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
                <!--<a class="btn btn-default" href="#">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>-->
                <a class="btn btn-default" href="#" onclick="ConfirmDeleteFolder('.$item->id.');">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>'
                );
        }        

    	return $this->setLayout(array('columns' => $columns, 'values' => $values));       
	}

    public function item() {
    	
    }	

    public function edit($id) {
        $folder = Folder::find($id);    

        if (!$folder)  {
            $this->setPage('folders-edit');  
            return Redirect::to('/admin/folders?success=false');
        }

        $this->layout = 'layoutes.admin.edit';
        $this->setPage('folders-edit');            
        $this->setTitle('Редактирование папки '.$folder->name); 
        $back = '<a class="btn btn-default" href="/admin/folders" role="button">Назад к списку папок</a>';
        $form_bound = View::make('layoutes.parts._form_bound', array('data' => array(
                'page' => 'folder_media_new',
                'folder_id' => $id,
                'medias' => MediaFile::all(),
            )));

        $columns = array('id','Файл','');
        $values = Folder::getMedia($id);
        //$medias = MediaFile::all();    
        
   
        
        return $this->setLayout(array('form' => View::make('layoutes.parts._form', array('page' => $this->page, 'folder' => $folder)),
                                      'form_bound' => $form_bound,
                                      'columns' => $columns,
                                      'values' => $values,
                                      'back' => $back));
    }

    public function create() {
        $folderName = Input::get('folderName');
        try {
            if (empty($folderName)) {
                return Redirect::to('/admin/folders?success=false');
            }

            if (Folder::checkName(Input::get('folderName'))) {
                return Redirect::to('/admin/folders?success=false');   
            }

            $folder = new Folder;
            $folder->name = trim(Input::get('folderName'));
            $folder->save();

            //Запись об изменении
            $this->addChangeLog('Создана папка: '.($folder->name));

            return Redirect::to('/admin/folders?success=true');
        }catch(Exception $e){                
            return Redirect::to('/admin/folders?success=false');
        }
    }    

    public function addMedia() {
        try {
            $folder_id = Input::get('folderID');
            foreach (Input::get('mediaID') as $i) {
                $newMediaBound = new MediaFolders;
                $newMediaBound->media_id = $i;                    
                $newMediaBound->folder_id = $folder_id;    
                $newMediaBound->save();

                $folder = Folder::find($folder_id);

                //Запись об изменении
                $this->addChangeLog('В папку '.$folder->name.' добавлен файл');                
            }
            $this->setPage('folders-media-add');  
            return Redirect::to('/admin/folders/edit/'.$folder_id.'?success=true');
        }catch(Exception $e){     
            $this->setPage('folders-media-add');           
            return Redirect::to('/admin/folders/edit/'.$folder_id.'?success=false');
        }        
    }

    public function editMedia($folder_id,$media_id) {
        $media = MediaFile::find($media_id);    

        if (!$media)  {
            $this->setPage('folders-edit');  
            return Redirect::to('/admin/folders/edit/'.$folder_id.'?success=false');
        }

        list($filename, $ext) = explode('.',$media->file); 


        $this->layout = 'layoutes.admin.edit';
        $this->setPage('media-edit');            
        $this->setTitle('Редактирование медиафайла '.$media->file);
        $back = '<a class="btn btn-default" href="/admin/folders/edit/'.$folder_id.'" role="button">Назад к папке</a>'; 
        
        
        
        return $this->setLayout(array('form' => View::make('layoutes.parts._form', array('page' => $this->page, 'media' => $media, 'folder' => $folder_id, 'filename' => $filename, 'ext' => $ext)), 'back' => $back ));

       
    }    

    public function updateMedia() {
        $this->setPage('folders-edit');
            $folder_id = Input::get('folderID');
            $media_id = Input::get('mediaID');
            $filename = '';

                $media = MediaFile::find($media_id);
                $media->type = ((preg_match('/^.*\.(.jpg|.jpeg|.png|.tif|.tiff|.JPG|.PNG|.gif|.GIF)$/i', $media->file))?'image':'video');

                $path = Config::get('paths.app')."/catalogdata/media/".$media->id; 
                
                if (Input::hasFile('docFile')) {
                    Eloquent::unguard(); 

                        // Delete file physically
                          if (is_dir($path)) {
                              $objects = scandir($path); 
                              foreach ($objects as $object) { 
                                if ($object != "." && $object != "..") {
                                     if (is_dir($path."/".$object))
                                       rmdir($path."/".$object);
                                     else
                                       unlink($path."/".$object);                                    
                                }
                              }
                          }

                    $extension =  Input::file('docFile')->getClientOriginalExtension();   

                    if (Input::has('docFileName')) {  
                      $filename = Input::get('docFileName').".".$extension;  
                    } else {
                      $filename = Input::file('docFile')->getClientOriginalName();    
                    }  
                    
                    Input::file('docFile')->move($path,$filename);
                } else {
                    if (Input::has('docFileName')) {  
                      $filename = Input::get('docFileName').".".Input::get('docFileExt'); 
                      rename($path."/".$media->file, $path."/".$filename);
                    }                
                }

                if (isset($filename)) {
                    $media->file = $filename;
                } 

                $media->update();

                //Запись об изменении
                $this->addChangeLog('Изменен медиафайл: '.$media->file);                 

                return Redirect::to('/admin/folders/edit/'.$folder_id.'?success=true');
            
    }

    public function deleteMedia($folder, $media) {
        if($folder){
            try {
                    //$folder_id = intval($folder,10);
                    //$media_id = intval($media,10);

                    $mm = MediaFolders::find($media);
                    if($mm){
                        //foreach($media as $m) {
                            $mm->delete();    
                        //}
                        

                            $f = Folder::find($folder);

                            //Запись об изменении
                            $this->addChangeLog('Из папки '.$f->name.' удален файл');  
                    }


                return Redirect::to('/admin/folders/edit/'.$folder.'?success=true');
            }catch(Exception $e){                
                return Redirect::to('/admin/folders/edit/'.$folder.'?success=false');
            }  
        }
    }    

    public function update() {

        $id = Input::get('folderID');
        $folder = Folder::find($id);    

        if (!$folder)  {
            $this->setPage('folders-edit');  
            return Redirect::to('/admin/folders?success=false');
        }

        $folderName = Input::get('folderName');
        if (empty($folderName)) {
            return Redirect::to('/admin/folders?success=false');
        }

        if (Folder::checkName(Input::get('folderName'))) {
            if (Input::get('folderName') !== $folder->name)
              return Redirect::to('/admin/folders?success=false');   
        }        

        try {
            $folder->name = Input::get('folderName');
            $folder->save();

            //Запись об изменении
            $this->addChangeLog('Изменена папка: '.($folder->name));   

            return Redirect::to('/admin/folders?success=upd'); 
        }catch(Exception $e){                
            return Redirect::to('/admin/folders/edit/'.$id.'?success=false');
        }
    }    

    public function delete($id) {
        try {
            if($id){

                //$folder = Folder::find($id);
                if (Folder::where('id', $id)->exists()) {
                    $folder = Folder::where('id', $id)->first();

                    $medias = MediaFolders::where('folder_id',$id)->get();

                    $folder->delete();
                    if ($medias) {
                        foreach ($medias as $media) {
                            $media->delete();
                            /*if (MediaFile::where('id', $media->media_id)->exists()) {
                                MediaFile::where('id', $media->media_id)->delete();
                            }*/
                        }
                    }

                    //Запись об изменении
                    $this->addChangeLog('Удалена папка: '.($folder->name));

                }
            }

            return Redirect::to('/admin/folders'); 
        }catch(Exception $e){                
            return Redirect::to('/admin/folders');
        }
    }    





}
