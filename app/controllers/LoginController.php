<?php

class LoginController extends BaseController {

	const ERROR_400 = "Не удалось обработать сообщение об ошибке";

	const ERROR_405 = "Метод не поддерживается";

	const ERROR_421 = "Размер архива слишком велик";

	const ERROR_429 = "Повторите запрос позже";

	const ERROR_500 = "Неизвестная ошибка";

    /*
        Показать форму авторизации
    */
    public function showLogin(){
        
        if (Auth::check())
        {
            return Redirect::to('/admin');
        }

        $data = array();

        if(Input::get('success') == 'false'){
            $data = array('success' => false);
        }

        return View::make('login',$data);
    }
    /*
        Логаут
        Выбрасываемся на форму авторизации
    */
    public function logout(){

        if (Auth::check())
        {
            Auth::logout();
        }

        //багфикс, на случай кривой очистки сессий ларавелом
        Session::flush();

        return Redirect::to('/login');
    }

    //Ручная авторизация
    //TODO:переделать на БД
    public function authUser(){

        if(Input::get('login') && Input::get('password')){
            if(Input::get('login') == 'ethiconadmin'){

                if(Input::get('password') == 'rollingNeedles2015'){
                    $user = User::find(2);
                    Auth::login($user);
                    return Redirect::to('/admin');
                }


            }else{
                return Redirect::to('/login?success=false');
            }
        }else{
            return Redirect::to('/login?success=false');
        }

        return Redirect::to('/login?success=false');
    }



    /*
		Пытаются вызвать что-то не то
    */
    public function missingMethod($parameters = array())
    {
        return $this->makeError(405);
    }

    /*
		Что-то пошло не так - формируем сообщение об ошибке
    */
    protected function makeError($code){

    	$message = "Unknown error";

    	switch ($code) {
    		case 400:
    			$message = self::ERROR_400;
    			break;

    		case 405:
    			$message = self::ERROR_405;
    			break;
    		
    		default:
    			$message = self::ERROR_500;
    			break;
    	}

    	return json_encode(array('status' => 'error', 'error_code' => ''.($code), 'error_message' => $message));
    }

}