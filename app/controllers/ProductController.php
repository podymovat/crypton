<?php

class ProductController extends BaseController {

	const ERROR_400 = "Не удалось обработать сообщение об ошибке";

	const ERROR_405 = "Метод не поддерживается";

	const ERROR_421 = "Размер архива слишком велик";

	const ERROR_429 = "Повторите запрос позже";

	const ERROR_500 = "Неизвестная ошибка";

    
    public function show(){
        $result = '<html><form enctype="multipart/form-data" action="/admin/docs/upload" method="POST"><input type="file" name="docFile"><input type="submit" value="Загрузить" /></form></html>';

        $result.="<table border='1'>";
        $result.="<tr><th>id</th><th>Имя</th><th>Файл</th></tr>";

        $docdata = array();

        $docs = Document::all();

        foreach ($docs as $doc) {
            $result.="<tr><td>".($doc->id)."</td><td>".($doc->filename)."</td><td><a href='#'>".($doc->url)."</a></td></tr>";
        }

        $result.="</table></html>";


        return $result;
    }



    ///На тестовом наборе данных

    public function upload(){
        //Шовные                                                     | product_id = 1
        //Сетка: code, name, mash_shape, size, pack_size, price      | product_id = 4
        //Дренаж: code, name, description, size, pack_size, price    | product_id = 2
        //Клей:code, name, size, glue_wound_length, pack_size, price | product_id = 3
        //Прочие: code, name, description, size, pack_size, price    | product_id = 5

        $source = "Код	Название	Описание (вместо Типа)	Размер	Упаковка, шт.	Цена
2160	J-VAC	Круглый резервуар	100 мл	10	10057.54
2161	J-VAC	Плоский активный резервуар	150 мл	10	12151.53
2162	J-VAC	Плоский активный резервуар	450 мл	10	12151.53
2163	J-VAC	Плоский активный резервуар	300 мл	10	12151.53
2210	BLAKE Drain	Плоский дренаж, пазы на 3/4 протяжения	7 мм	10	14538.45
2211	BLAKE Drain	Плоский дренаж, пазы на всем протяжении	7 мм	10	14538.45
2212	BLAKE Drain	Плоский дренаж, пазы на всем протяжении, с троакаром 3/16	7 мм	10	19312.31
2213	BLAKE Drain	Плоский дренаж, пазы на 3/4 протяжения	10 мм	10	16925.39
2214	BLAKE Drain	Плоский дренаж, пазы на всем протяжении	10 мм	10	16925.39
2215	BLAKE Drain	Плоский дренаж, пазы на всем протяжении, с троакаром 3/16	10 мм	10	19312.31
2216	BLAKE Drain	Плоский дренаж, пазы на 3/4 протяжения, с троакаром 3/16	7 мм	10	19312.31
2217	BLAKE Drain	Плоский дренаж, пазы на 3/4 протяжения, с троакаром 3/16	10 мм	10	19312.31
2226	BLAKE Drain	Круглый дренаж, цельный	10 FR	10	12151.53
2227	BLAKE Drain	Круглый дренаж, цельный, с троакаром 1/8	10 FR	10	14538.45
2228	BLAKE Drain	Круглый дренаж, цельный	15 FR	10	12151.53
2229	BLAKE Drain	Круглый дренаж, цельный, с троакаром 3/16	15 FR	10	14538.45
2230	BLAKE Drain	Круглый дренаж, цельный	19 FR 	10	12516.08
2231	BLAKE Drain	Круглый дренаж, цельный, с троакаром 1/4	19 FR 	10	17433.14
2232	BLAKE Drain	Круглый дренаж, цельный, с изгибающимся троакаром	19 FR 	10	17433.14
2233	BLAKE Drain	Круглый дренаж, цельный, с изгибающимся троакаром	15 FR	10	17433.14
2234	BLAKE Drain	Круглый дренаж, цельный	24 FR	10	14538.45
BCC1	BLAKE	Кардиоконнектор 1:1, для дренажей 19 FR и 24 FR	-	20	5140.46
BCC2	BLAKE	Кардиоконнектор 2:1, для дренажей 19 FR и 24 FR	-	20	5140.46
BCC3	BLAKE	Кардиоконнектор 3:1, для дренажей 19 FR и 24 FR	-	20	5140.46";


        $current_product = 2;

        //FIXME: привести в порядок модели и убрать
        Eloquent::unguard();

        //Выключаем все старые элементы, касающиеся данной продуктовой категории
        $affected = DB::table('items_others')->where('product_id', '=', $current_product)->update(array('enabled' => 0));
        //
        //0 этап. Создаем массив
        $input = explode("\n", $source);

        //1 Этап. Нормируем массив:убираем элементы, где не хватает длины или нет кода, убираем заголовок
        $basedata = array();
        for($i = 0; $i < count($input);$i++){
            $input[$i] = explode("\t", $input[$i]);
            if(count($input[$i]) > 5){
                if($input[$i][0] != "Код" && $input[$i][0] != '' && $input[$i][0] != null){
                    array_push($basedata, $input[$i]);
                }
            }
        }

        //2 Этап. Льем продукты в базу по коду.
        for($i = 0; $i < count($basedata);$i++){
            $cur = $basedata[$i];
            $item = OtherProduct::firstOrCreate(array('code' => $cur[0]));
            $item->name = $cur[1];

            if($current_product == 5){
	            $item->description_ru = $cur[2];            
	            $item->size = $cur[3];
				$item->product_id = 5;
			}else if($current_product == 4){
				$item->mash_shape = $cur[2];
				$item->size = $cur[3];  
				$item->product_id = 4;
			}else if($current_product == 3){
				$item->size = $cur[2];
				$item->glue_wound_length = $cur[3];  
				$item->product_id = 3;
			}else if($current_product == 2){
	            $item->description_ru = $cur[2];            
	            $item->size = $cur[3];
				$item->product_id = 2;
			}

            $item->pack_size = intval($cur[4],10);
            $item->price = floatval($cur[5]);

            
            $item->enabled = 1;
            $item->save();
        }

    }

    public function uploadWithData($source,$pr){
        $current_product = $pr;

        //FIXME: привести в порядок модели и убрать
        Eloquent::unguard();

        //Выключаем все старые элементы, касающиеся данной продуктовой категории
        $affected = DB::table('items_others')->where('product_id', '=', $current_product)->update(array('enabled' => 0));
        //
        //0 этап. Создаем массив
        $input = explode("\n", $source);

        //1 Этап. Нормируем массив:убираем элементы, где не хватает длины или нет кода, убираем заголовок
        $basedata = array();
        for($i = 0; $i < count($input);$i++){
            $input[$i] = explode("\t", $input[$i]);
            if(count($input[$i]) > 5){
                if($input[$i][0] != "Код" && $input[$i][0] != '' && $input[$i][0] != null){
                    array_push($basedata, $input[$i]);
                }
            }
        }

        if(count($basedata) == 0){
            return false;
        }

        //2 Этап. Льем продукты в базу по коду.
        for($i = 0; $i < count($basedata);$i++){
            $cur = $basedata[$i];
            $item = OtherProduct::firstOrCreate(array('code' => $cur[0]));
            $item->name = $cur[1];

            if($current_product == 5){
                $item->description_ru = trim(trim($cur[2]," "),'"');             
                $item->size = trim(trim($cur[3]," "),'"');  
                $item->product_id = 5;
            }else if($current_product == 4){
                $item->mash_shape = trim(trim($cur[2]," "),'"');  
                $item->size = trim(trim($cur[3]," "),'"');  
                $item->product_id = 4;
            }else if($current_product == 3){
                $item->size = trim(trim($cur[2]," "),'"');  
                $item->glue_wound_length = trim(trim($cur[3]," "),'"');   
                $item->product_id = 3;
            }else if($current_product == 2){
                $item->description_ru = trim(trim($cur[2]," "),'"');             
                $item->size = trim(trim($cur[3]," "),'"');  
                $item->product_id = 2;
            }

            $item->pack_size = intval($cur[4],10);
            $item->price = floatval(str_replace(',', '.',$cur[5]));

            
            $item->enabled = 1;
            $item->save();
        }

        //3 Этап. Записываем информацию об изменении в лог изменений

        $newChange = new ChangelogItem;
        $newChange->time = time();
        $newChange->description = 'Обновлена продукция: '.ProductCategory::find($current_product)->export_id;
        $newChange->save();

        return true;

    }

    /*
		Пытаются вызвать что-то не то
    */
    public function missingMethod($parameters = array())
    {
        return $this->makeError(405);
    }

    /*
		Что-то пошло не так - формируем сообщение об ошибке
    */
    protected function makeError($code){

    	$message = "Unknown error";

    	switch ($code) {
    		case 400:
    			$message = self::ERROR_400;
    			break;

    		case 405:
    			$message = self::ERROR_405;
    			break;
    		
    		default:
    			$message = self::ERROR_500;
    			break;
    	}

    	return json_encode(array('status' => 'error', 'error_code' => ''.($code), 'error_message' => $message));
    }

}