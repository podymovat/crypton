<?php

class Folder extends Eloquent{
	
	protected $table = 'folder';

    static function getMedia($id) {
        $media = MediaFolders::where('folder_id', $id)->get();

        $result = array();
        foreach ($media as $item) {
            if (MediaFile::where('id', $item->media_id)->exists()) {
                $media = MediaFile::where('id', $item->media_id)->first();
                $result[] = array(
                        $item->media_id,
                        $media->file,
                        '<a class="btn btn-default" href="/admin/folders/editmedia/'.$id.'/'.$item->media_id.'">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </a>
                        <a class="btn btn-default" href="#" onclick="ConfirmDeleteFolderMedia('.$id.','.$item->id.');">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>'                   
                    );
            } else {
                $result = array();
            }

        }
        return $result;
    }  


    static function checkName($name) {
        $folder = Folder::where('name', $name)->exists();

        if ($folder) {
            return true;
        } else {
            return false;
        }
    } 	
	
}