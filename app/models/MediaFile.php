<?php

class MediaFile extends Eloquent{
	
	protected $table = 'media';

	static function getProducts($doc_id) {
    	$media_products = MediaProducts::where('media_id', $doc_id)->get();
    	$result = array();
    	foreach ($media_products as $item) {
    		$result[] = ProductCategory::find($item->product_id)->export_id;
    	}
    	return implode($result, '<br/> ');
	}

    static function getFolders($doc_id) {
        $media_folders = MediaFolders::where('media_id', $doc_id)->get();
        $result = array();
        
        try {
            if ($media_folders) {
                foreach ($media_folders as $item) {
                    $result[] = Folder::find($item->folder_id)->name;
                }            
            }            
        }catch(Exception $e){                
            return '';
        }


        return implode($result, '<br/> ');
    }    
	
}