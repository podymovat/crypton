<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Ethicon - управление контентом приложения</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <link href="../css/select2.min.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>

    <script src="../js/functions.js" type="text/javascript"></script>   
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Ethicon Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Инструкция</a></li>
            <li><a href="/logout">Выйти</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="/admin">Главная <span class="sr-only">(текущая)</span></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <?php
            if($page == 'products-main'){
              echo '<li class="active"><a href="/admin/products-main">Шовные материалы</a></li>';
            }else{
              echo '<li><a href="/admin/products-main">Шовные материалы</a></li>';
            }
            if($page == 'products-mesh'){
              echo '<li class="active"><a href="/admin/products-mesh">Сетки</a></li>';
            }else{
              echo '<li><a href="/admin/products-mesh">Сетки</a></li>';
            }
            if($page == 'products-drainage'){
              echo '<li class="active"><a href="/admin/products-drainage">Дренажи</a></li>';
            }else{
              echo '<li><a href="/admin/products-drainage">Дренажи</a></li>';
            }
            if($page == 'products-glue'){
              echo '<li class="active"><a href="/admin/products-glue">Кожные клеи</a></li>';
            }else{
              echo '<li><a href="/admin/products-glue">Кожные клеи</a></li>';
            }
            if($page == 'products-other'){
              echo '<li class="active"><a href="/admin/products-other">Прочие</a></li>';
            }else{
              echo '<li><a href="/admin/products-other">Прочие</a></li>';
            }          
            

            ?>
          </ul>
          <?php //echo $left_menu; ?>
          <ul class="nav nav-sidebar">
          <?php
            if($page == 'folders-view'){
              echo '<li class="active"><a href="/admin/folders">Папки</a></li>';
            }else{
              echo '<li><a href="/admin/folders">Папки</a></li>';
            }           
            if($page == 'documents-view'){
              echo '<li class="active"><a href="/admin/documents">Документы</a></li>';
            }else{
              echo '<li><a href="/admin/documents">Документы</a></li>';
            } 
            if($page == 'media-view'){
              echo '<li class="active"><a href="/admin/media">Медиафайлы</a></li>';
            }else{
              echo '<li><a href="/admin/media">Медиафайлы</a></li>';
            }
            if($page == 'needlepics'){
              echo '<li class="active"><a href="/admin/needlepics">Изображения игл</a></li>';
            }else{
              echo '<li><a href="/admin/needlepics">Изображения игл</a></li>';
            }            
            
          ?>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Управление контентом iPad каталога</h1>

          <?php

          if(isset($success) && ($success == 'true')){
            if($page == 'documents-view'){
              echo '<div class="alert alert-success" role="alert">Документ успешно загружен!</div>';
            }else if($page == 'media-view'){
              echo '<div class="alert alert-success" role="alert">Медиафайл успешно загружен!</div>';
            }else if($page == 'needlepics-view'){
              echo '<div class="alert alert-success" role="alert">Игла успешно создана!</div>';
            }else{
              echo '<div class="alert alert-success" role="alert">Список продукции успешно загружен!</div>';
            }
          }

          //Строковое сравнение, т.к. undefined = булево false
          if(isset($success) && ($success == 'false')){
            if($page == 'documents-view'){
              echo '<div class="alert alert-danger" role="alert">Не удалось загрузить документ. Повторите попытку позже.</div>';
            }else if($page == 'media-view'){
              echo '<div class="alert alert-danger" role="alert">Не удалось загрузить медиафайл. Проверьте корректность формы.</div>';
            }else if($page == 'needlepics'){
              echo '<div class="alert alert-danger" role="alert">Не удалось создать новую иглу.</div>';
            }else{
              echo '<div class="alert alert-danger" role="alert">Не удалось загрузить список продукции. Некорректный формат файла.</div>';
            }
          }

          if(isset($success) && ($success == 'del')){
            if($page == 'documents-view'){
              echo '<div class="alert alert-success" role="alert">Документ успешно загружен!</div>';
            }else if($page == 'media-view'){
              echo '<div class="alert alert-success" role="alert">Медиафайл успешно удален!</div>';
            }else if($page == 'needlepics-view'){
              echo '<div class="alert alert-success" role="alert">Игла успешно создана!</div>';
            }else{
              echo '<div class="alert alert-success" role="alert">Список продукции успешно загружен!</div>';
            }
          }  

          if(isset($success) && ($success == 'upd')){
            if($page == 'documents-view'){
              echo '<div class="alert alert-success" role="alert">Документ успешно загружен!</div>';
            }else if($page == 'media-view'){
              echo '<div class="alert alert-success" role="alert">Медиафайл успешно обновлен!</div>';
            }else if($page == 'needlepics-view'){
              echo '<div class="alert alert-success" role="alert">Игла успешно создана!</div>';
            }else{
              echo '<div class="alert alert-success" role="alert">Список продукции успешно загружен!</div>';
            }
          }                  

          ?>

          <?php

          if($page == 'documents-view'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка документа</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/docs/upload" method="POST">
                Введите имя файла
                <br/> 
                <input id="inputName" type="text" name="docName">
                <br/>
                Выберите документ для загрузки
                <br/>
                <input type="file" name="docFile">
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';
          }else if($page == 'media-view'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка медиафайла</div>
              <div class="panel-body">
                <form enctype="multipart/form-data" action="/admin/media/upload" method="POST">
                  Выберите продукты, к котором относится файл
                  <br/> 
                  <select name="productID[]" class="js-example-basic-multiple form-control" multiple="multiple">';

                  foreach ($products as $product) {
                    echo '<option value="'.($product->id).'">'.($product->export_id).'</option>';
                  }

            echo '</select>
                  <br/>
                  <br/>
                  Выберите папки, к котором относится файл
                  <br/>      
                  <select name="folderID[]" class="js-example-basic-multiple form-control" multiple="multiple">';
                    foreach ($folders as $folder) {
                      echo '<option value="'.($folder->id).'">'.($folder->name).'</option>';
                    }            
            echo '</select>
                  <br/>
                  <br/>
                  Выберите файл для загрузки
                  <br/>
                  <input type="file" name="docFile">
                  <br/>
                  <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';
          }else if($page == 'products-drainage'){
             echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка обновленного каталога - Дренажи</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
                Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
                <br/>
                <input type="file" name="docFile">
                <input type="hidden" name="productID" value="2">
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';
          }else if($page == 'products-glue'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка обновленного каталога - Кожные клеи</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
                Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
                <br/>
                <input type="file" name="docFile">
                <input type="hidden" name="productID" value="3">
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';

          }else if($page == 'products-mesh'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка обновленного каталога - Сетки</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
                Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
                <br/>
                <input type="file" name="docFile">
                <input type="hidden" name="productID" value="4">
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';

          }else if($page == 'products-other'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка обновленного каталога - Специальные продукты</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
                Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
                <br/>
                <input type="file" name="docFile">
                <input type="hidden" name="productID" value="5">
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';

          }else if($page == 'products-main'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Загрузка обновленного каталога - Шовные материалы</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/main/upload" method="POST">
                Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
                <br/>
                <input type="file" name="docFile">
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';
          }else if($page == 'needlepics'){
            echo '<div class="panel panel-default">
              <div class="panel-heading">Добавление новой иглы</div>
              <div class="panel-body">                
                <form enctype="multipart/form-data" action="/admin/needlepics/create" method="POST">
                Введите имя новой иглы
                <br/> 
                <input id="inputName" type="text" name="name">
                <br/>
                Выберите png файл изображения в высоком разрешении.
                <br/>
                <input type="file" name="file_original" class="form-control">
                <br/>
                Выберите png файл изображения в стандартном разрешении.
                <br/>
                <input type="file" name="file_ipad" class="form-control">
                <br/>
                <input type="checkbox" name="is_double" value="true" />&nbsp;&nbsp;Игла двойная? 
                <br/>
                <br/>
                <input type="submit" value="Загрузить" class="btn btn-default"/>
                </form>
              </div>
            </div>';
          }

          ?>

          <h2 class="sub-header"><?php echo htmlspecialchars($pagename); ?></h2>
          <div class="table-responsive">
            <table class="table table-striped">
            <?php
              //Заголовок таблицы

              echo '<thead><tr>';
              if(isset($columns)){
                for($i = 0;$i<count($columns);$i++){
                  echo '<th>'.$columns[$i].'</th>';
                }
              }
              echo '</tr></thead>';

              ////

              //Тело таблицы
              echo '<tbody>';

              if(isset($values)){
                for($i = 0;$i<count($values);$i++){
                  echo '<tr>';

                  for($j = 0;$j<count($values[$i]);$j++){
                    //Вывод здесь не HTML-safe, так как выводятся кнопки скачивания-редактирования-удаления
                    echo '<td>'.$values[$i][$j].'</td>';
                  }

                  echo '</tr>';
                }
              }

              ///

              echo '</tbody>';
            ?>               
              
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../js/select2/select2.min.js"></script> 
      <script type="text/javascript">
        $(document).ready(function(){
          $('.js-example-basic-multiple').select2({
            placeholder: "Выбрать...",
          });
        });
      </script>    
    <script src="../js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../js/vendor/holder.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
