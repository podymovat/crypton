<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $head; ?>
  </head>

  <body>

    <?php echo $header; ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <?php echo $left_menu; ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Управление контентом iPad каталога</h1>

          <!--<a class="btn btn-default" href="/admin/folders" role="button">Назад к списку папок</a>-->
          <?php echo $back; ?>

          <h2 class="sub-header"><?php echo htmlspecialchars($pagename); ?></h2>

          <?php echo $info; ?>
          <?php echo $form; ?>

          <?php 
            if (isset($form_bound)) {
              echo $form_bound;
            }
          ?>

          <div class="table-responsive">
            <table class="table table-striped">
            <?php
              //Заголовок таблицы

              echo '<thead><tr>';
              if(isset($columns)){
                foreach ($columns as $column) {
                  echo '<th>'.$column.'</th>';
                }
                /*for($i = 0;$i<count($columns);$i++){
                  echo '<th>'.$columns[$i].'</th>';
                }*/
              }
              echo '</tr></thead>';

              ////

              //Тело таблицы
              echo '<tbody>';

              if(isset($values)){
                for($i = 0;$i<count($values);$i++){
                  echo '<tr>';

                  for($j = 0;$j<count($values[$i]);$j++){
                    //Вывод здесь не HTML-safe, так как выводятся кнопки скачивания-редактирования-удаления
                    echo '<td>'.$values[$i][$j].'</td>';
                  }

                  echo '</tr>';
                }
              }

              ///

              echo '</tbody>';
            ?>               
              
            </table>
          </div>           
        </div>       
      </div>



    </div>

    <?php echo $footer; ?>
  </body>
</html>