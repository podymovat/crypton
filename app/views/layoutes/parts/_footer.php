<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/select2/select2.min.js"></script> 
<script src="/js/bootstrap-fileinput/fileinput.min.js" type="text/javascript"></script>
<script src="/js/bootstrap-fileinput/fileinput_locale_ru.js" type="text/javascript"></script>
<script src="/js/functions.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.js-example-basic-multiple').select2({
        placeholder: "Выбрать...",
      });
      $(".file").fileinput({
      	language:'ru',
      	showUpload:false, 
      	uploadUrl:'#',
      	previewFileType:'any'      	
      });
    });
  </script>    
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="/js/vendor/holder.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>