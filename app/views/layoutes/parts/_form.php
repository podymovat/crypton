<?php

    if($page == 'documents-view'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка документа</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/docs/upload" method="POST">
          Введите имя файла
          <br/> 
          <input id="inputName" type="text" name="docName">
          <br/>
          Выберите документ для загрузки
          <br/>
          <input type="file" name="docFile">
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    } else if($page == 'folders-view') { 
      echo '<div class="panel panel-default">
        <div class="panel-heading">Создание папки</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/folders/create" method="POST">
          Введите имя папки
          <br/> 
          <input type="text" name="folderName" placeholder="Название" class="form-control">
          <br/>
          <br/>
          <input type="submit" value="Создать" class="btn btn-default"/>
          </form>
        </div>
      </div>';      
    }else if($page == 'folders-edit') { 
      echo '<div class="panel panel-default">
        <div class="panel-heading">Редактирование папки</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/folders/update" method="POST">
          Введите имя папки
          <br/> 
          <input type="hidden" name="folderID" value="'.$folder->id.'">
          <input type="text" name="folderName" value="'.$folder->name.'" class="form-control">
          <br/>
          <br/>
          <input type="submit" value="Обновить" class="btn btn-default"/>
          </form>
        </div>
      </div>';      
    }else if($page == 'media-view'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка медиафайла</div>
        <div class="panel-body">
          <form enctype="multipart/form-data" action="/admin/media/upload" method="POST">
            Выберите продукт, к которому относится файл
            <br/> 
            <select name="productID[]" class="js-example-basic-multiple form-control" multiple="multiple">';

            foreach ($products as $product) {
              echo '<option value="'.($product->id).'">'.($product->export_id).'</option>';
            }

      echo '</select>
            <br/>
            <select name="productID[]" class="js-example-basic-multiple form-control" multiple="multiple">';
              foreach ($folders as $folder) {
                echo '<option value="'.($folder->id).'">'.($folder->name).'</option>';
              }            
      echo '</select>
            <br/>
            Выберите файл для загрузки
            <br/>
            <!--<input type="file" id="docFile" name="docFile" class="file" data-preview-file-type="text">-->
            <input type="file" name="docFile">
            <br/>
            <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    }else if($page == 'media-edit'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Изменение медиафайла</div>
        <div class="panel-body">
          <form enctype="multipart/form-data" action="/admin/folders/updatemedia" method="POST">
            <input type="hidden" name="mediaID" value="'.$media->id.'">
            <input type="hidden" name="folderID" value="'.$folder.'">
            <br/>
            Выберите файл для загрузки
            <br/>
            <div id="myFileParent">
              <input type="file" id="docFile" name="docFile" class="file" data-preview-file-type="text">
              <input type="text" name="docFileName" value="'.$filename.'" class="form-control file-caption  kv-fileinput-caption" style="width: 100%;position: absolute;top: 0;z-index: 1000;border-right: 0;height: 100%;border: 0;left: 0;border-radius: 5px;" id="myFileName">
              <input type="hidden" name="docFileExt" value="'.$ext.'">
            </div>
            <br/>
            <input type="submit" value="Обновить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    }else if($page == 'media-edit-main'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Изменение медиафайла</div>
        <div class="panel-body">
          <form enctype="multipart/form-data" action="/admin/media/updatemedia/'.$media->id.'" method="POST">
            <input type="hidden" name="mediaID" value="'.$media->id.'">
            <br/>
            Выберите продукт, к которому относится файл
            <br/>
            <select name="productID[]" class="js-example-basic-multiple form-control" multiple="multiple">';

            foreach ($products as $product) {
              $sel = (in_array($product->id, $media_products)) ? 'selected' : '';
              echo '<option value="'.($product->id).'" '.$sel.'>'.($product->export_id).'</option>';
            }

      echo '</select>   
            <br/><br/>Выберите папку<br/>
            <select name="folderID[]" class="js-example-basic-multiple form-control" multiple="multiple">';
              foreach ($folders as $folder) {
                $sel = (in_array($folder->id, $media_folders)) ? 'selected' : '';
                echo '<option value="'.($folder->id).'" '.$sel.'>'.($folder->name).'</option>';
              }            
      echo '</select>    
            <br/><br/>                
            Выберите файл для загрузки
            <br/>            
            
            <div id="myFileParent">
              <input type="file" id="docFile" name="docFile" class="file" data-preview-file-type="text">
              <input type="text" name="docFileName" value="'.$filename.'" class="form-control file-caption  kv-fileinput-caption" style="width: 100%;    position: absolute;
    top: 0;z-index: 1000;border-right: 0;height: 100%;border: 0;left: 0;border-radius: 5px;" id="myFileName">
              <input type="hidden" name="docFileExt" value="'.$ext.'">
            </div>            
            <br/>
            <input type="submit" value="Обновить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    }else if($page == 'products-drainage'){
       echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка обновленного каталога - Дренажи</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
          Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
          <br/>
          <input type="file" name="docFile">
          <input type="hidden" name="productID" value="2">
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    }else if($page == 'products-glue'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка обновленного каталога - Кожные клеи</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
          Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
          <br/>
          <input type="file" name="docFile">
          <input type="hidden" name="productID" value="3">
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';

    }else if($page == 'products-mesh'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка обновленного каталога - Сетки</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
          Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
          <br/>
          <input type="file" name="docFile">
          <input type="hidden" name="productID" value="4">
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';

    }else if($page == 'products-other'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка обновленного каталога - Специальные продукты</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/others/upload" method="POST">
          Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
          <br/>
          <input type="file" name="docFile">
          <input type="hidden" name="productID" value="5">
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';

    }else if($page == 'products-main'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Загрузка обновленного каталога - Шовные материалы</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/main/upload" method="POST">
          Загрузите лист Excel, экспортированный в формат Текст Юникод (файл .txt), в кодировке UTF-8
          <br/>
          <input type="file" name="docFile">
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    }else if($page == 'needlepics'){
      echo '<div class="panel panel-default">
        <div class="panel-heading">Добавление новой иглы</div>
        <div class="panel-body">                
          <form enctype="multipart/form-data" action="/admin/needlepics/create" method="POST">
          Введите имя новой иглы
          <br/> 
          <input id="inputName" type="text" name="name">
          <br/>
          Выберите png файл изображения в высоком разрешении.
          <br/>
          <input type="file" name="file_original" class="form-control">
          <br/>
          Выберите png файл изображения в стандартном разрешении.
          <br/>
          <input type="file" name="file_ipad" class="form-control">
          <br/>
          <input type="checkbox" name="is_double" value="true" />&nbsp;&nbsp;Игла двойная? 
          <br/>
          <br/>
          <input type="submit" value="Загрузить" class="btn btn-default"/>
          </form>
        </div>
      </div>';
    }

?>