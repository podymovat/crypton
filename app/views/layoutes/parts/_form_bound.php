<?php
	if ($data['page'] == 'folder_media_new') {
      echo '<div class="panel panel-default">
        <div class="panel-heading">Добавить медиафайл</div>
        <div class="panel-body">
          <form enctype="multipart/form-data" action="/admin/folders/addmedia" method="POST">
            <input type="hidden" name="folderID" value="'.$data['folder_id'].'">
            Выберите файл
            <br/> 
            <select name="mediaID[]" class="js-example-basic-multiple form-control" multiple="multiple">';

            foreach ($data['medias'] as $media) {
              echo '<option value="'.($media->id).'">'.($media->file).'</option>';
            }

      echo '</select>
            <br/>
            <br/>
            <br/>
            <input type="submit" value="Добавить" class="btn btn-default"/>
          </form>
        </div>
      </div>';		
	}
?>