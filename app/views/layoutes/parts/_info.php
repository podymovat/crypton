<?php

    if(isset($success) && ($success == 'true')){
        if($page == 'documents-view'){
          echo '<div class="alert alert-success" role="alert">Документ успешно загружен!</div>';
        }else if($page == 'media-view'){
          echo '<div class="alert alert-success" role="alert">Медиафайл успешно загружен!</div>';
        }else if($page == 'needlepics-view'){
          echo '<div class="alert alert-success" role="alert">Игла успешно создана!</div>';
        }else if($page == 'folders-view'){
          echo '<div class="alert alert-success" role="alert">Папка успешно создана!</div>';
        }else if($page == 'folders-edit'){
          echo '<div class="alert alert-success" role="alert">Папка успешно обновлена!</div>';
        }else{
          echo '<div class="alert alert-success" role="alert">Список продукции успешно загружен!</div>';
        }
    }

    //Строковое сравнение, т.к. undefined = булево false
    if(isset($success) && ($success == 'false')){
        if($page == 'documents-view'){
          echo '<div class="alert alert-danger" role="alert">Не удалось загрузить документ. Повторите попытку позже.</div>';
        }else if($page == 'media-view'){
          echo '<div class="alert alert-danger" role="alert">Не удалось загрузить медиафайл. Повторите попытку позже.</div>';
        }else if($page == 'needlepics'){
          echo '<div class="alert alert-danger" role="alert">Не удалось создать новую иглу.</div>';
        }else if($page == 'folders-view'){
          echo '<div class="alert alert-danger" role="alert">Не удалось создать новую папку.</div>';
        }else if($page == 'folders-edit'){
          echo '<div class="alert alert-danger" role="alert">Не удалось отредактировать папку. Папка не существует</div>';
        }else{
          echo '<div class="alert alert-danger" role="alert">Не удалось загрузить список продукции. Некорректный формат файла.</div>';
        }
    }

    if(isset($success) && ($success == 'upd')){
        if($page == 'documents-view'){
          echo '<div class="alert alert-success" role="alert">Не удалось загрузить документ. Повторите попытку позже.</div>';
        }else if($page == 'media-view'){
          echo '<div class="alert alert-success" role="alert">Не удалось загрузить медиафайл. Повторите попытку позже.</div>';
        }else if($page == 'needlepics'){
          echo '<div class="alert alert-success" role="alert">Не удалось создать новую иглу.</div>';
        }else if($page == 'folders-view'){
          echo '<div class="alert alert-success" role="alert">Папка успешно обновлена!</div>';
        }else if($page == 'folders-edit'){
          echo '<div class="alert alert-success" role="alert">Не удалось отредактировать папку. Папка не существует</div>';
        }else{
          echo '<div class="alert alert-success" role="alert">Не удалось загрузить список продукции. Некорректный формат файла.</div>';
        }
    }    

?>