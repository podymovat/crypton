<ul class="nav nav-sidebar">
    <li><a href="/admin">Главная <span class="sr-only">(текущая)</span></a></li>
</ul>
<ul class="nav nav-sidebar">
    <?php
        if($page == 'products-main'){
          echo '<li class="active"><a href="/admin/products-main">Шовные материалы</a></li>';
        }else{
          echo '<li><a href="/admin/products-main">Шовные материалы</a></li>';
        }
        if($page == 'products-mesh'){
          echo '<li class="active"><a href="/admin/products-mesh">Сетки</a></li>';
        }else{
          echo '<li><a href="/admin/products-mesh">Сетки</a></li>';
        }
        if($page == 'products-drainage'){
          echo '<li class="active"><a href="/admin/products-drainage">Дренажи</a></li>';
        }else{
          echo '<li><a href="/admin/products-drainage">Дренажи</a></li>';
        }
        if($page == 'products-glue'){
          echo '<li class="active"><a href="/admin/products-glue">Кожные клеи</a></li>';
        }else{
          echo '<li><a href="/admin/products-glue">Кожные клеи</a></li>';
        }
        if($page == 'products-other'){
          echo '<li class="active"><a href="/admin/products-other">Прочие</a></li>';
        }else{
          echo '<li><a href="/admin/products-other">Прочие</a></li>';
        }          
    ?>
</ul>
<ul class="nav nav-sidebar">
    <?php
        if($page == 'folders-view'){
          echo '<li class="active"><a href="/admin/folders">Папки</a></li>';
        }else{
          echo '<li><a href="/admin/folders">Папки</a></li>';
        }           
        if($page == 'documents-view'){
          echo '<li class="active"><a href="/admin/documents">Документы</a></li>';
        }else{
          echo '<li><a href="/admin/documents">Документы</a></li>';
        } 
        if($page == 'media-view'){
          echo '<li class="active"><a href="/admin/media">Медиафайлы</a></li>';
        }else{
          echo '<li><a href="/admin/media">Медиафайлы</a></li>';
        }
        if($page == 'needlepics'){
          echo '<li class="active"><a href="/admin/needlepics">Изображения игл</a></li>';
        }else{
          echo '<li><a href="/admin/needlepics">Изображения игл</a></li>';
        }            
    ?>
</ul>