<ul class="nav nav-sidebar">
  <li class="active"><a href="/admin">Главная <span class="sr-only">(текущая)</span></a></li>
</ul>
<ul class="nav nav-sidebar">
  <li><a href="/admin/products-main">Шовные материалы</a></li>
  <li><a href="/admin/products-mesh">Сетки</a></li>
  <li><a href="/admin/products-drainage">Дренажи</a></li>
  <li><a href="/admin/products-glue">Кожные клеи</a></li>
  <li><a href="/admin/products-other">Прочие</a></li>
</ul>
<ul class="nav nav-sidebar">
  <li><a href="/admin/folders">Папки</a></li>
  <li><a href="/admin/documents">Документы</a></li>
  <li><a href="/admin/media">Медиафайлы</a></li>
  <li><a href="/admin/needlepics">Изображения игл</a></li> 
</ul>