$( document ).ready(function() {
    var myfileName = $('#myFileName');
    myfileName.remove();
    var myFileParent = $('#myFileParent .file-input .input-group .file-caption').append(myfileName);
});

function removeObject(id, type){


	var msg = 'Удалить объект? Это действие невозможно отменить.';

	switch(type){
		case 'needle':
			msg = 'Удалить изображения этой иглы? Это действие невозможно отменить';
			break;
	}

	if (window.confirm(msg))
	{
	    //Да, удаляем
	    window.location = "/admin/needle/delete?id="+id; //проверка значения на безопасность на стороне php
	}
	else
	{
	    //Ничего не делаем, пользователь передумал удалять объект
	}
}


function ConfirmDeleteFolder(id) {
	var msg = "Удалить папку? Это действие невозможно отменить.";
	if (window.confirm(msg)) {
		window.location = "/admin/folders/delete/" + id;
	} else {
		
	}
}

function ConfirmDeleteMedia(id) {
	var msg = "Удалить медиафайл? Это действие невозможно отменить.";
	if (window.confirm(msg)) {
		window.location = '/admin/media/delete/' + id;
	} else {
		
	}
}

function ConfirmDeleteFolderMedia(folder_id,media_id) {
	var msg = "Удалить медиафайл из папки?";
	if (window.confirm(msg)) {
		window.location = "/admin/folders/deletemedia/" + folder_id + "/" + media_id;
	} else {
		
	}
}

function ConfirmEditFolder(folder_id,media_id) {
	var msg = "Удалить медиафайл из папки?";
	if (window.confirm(msg)) {
		window.location = "/admin/folders/deletemedia/" + folder_id + "/" + media_id;
	} else {
		
	}
}