-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog` (
  `category` varchar(255) CHARACTER SET latin1 NOT NULL,
  `version` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `changelog`;
CREATE TABLE `changelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` enum('main','others','documents','media') NOT NULL DEFAULT 'main',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pushed` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `filename` text NOT NULL,
  `url` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `items_main`;
CREATE TABLE `items_main` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `description` int(11) NOT NULL,
  `string_size` varchar(255) NOT NULL,
  `string_length` int(11) NOT NULL,
  `needle_code` varchar(255) NOT NULL,
  `needle_size` float NOT NULL,
  `needle_curveness` varchar(255) NOT NULL,
  `needle_color` enum('0','1') NOT NULL,
  `needle_count` int(11) NOT NULL,
  `string_colored` enum('0','1','2') NOT NULL,
  `string_count` int(11) NOT NULL,
  `is_ethalloy` int(11) NOT NULL,
  `pack_size` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `needle_picture_id` int(11) NOT NULL,
  `needle_type` int(11) NOT NULL,
  `needle_subtype` text NOT NULL,
  `notes` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `items_others`;
CREATE TABLE `items_others` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `description_ru` text NOT NULL,
  `description_en` text NOT NULL,
  `mash_shape` text NOT NULL,
  `glue_wound_length` text NOT NULL,
  `size` text NOT NULL,
  `pack_size` int(11) NOT NULL,
  `price` float NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `items_others` (`id`, `product_id`, `code`, `name`, `description_ru`, `description_en`, `mash_shape`, `glue_wound_length`, `size`, `pack_size`, `price`, `updated_at`, `created_at`) VALUES
(0,	4,	'PCDB1',	'PROCEED',	'',	'',	'Прямоугольная',	'',	'5х10 см',	1,	11272.3,	'2014-12-22 15:56:15',	'0000-00-00 00:00:00'),
(0,	2,	'2160',	'J-VAC',	'Круглый резервуар',	'',	'',	'',	'100 мл',	10,	0,	'2014-12-22 15:57:30',	'0000-00-00 00:00:00'),
(0,	2,	'2161',	'J-VAC',	'Плоский активный резервуар',	'',	'',	'',	'150 мл',	10,	0,	'2014-12-22 16:01:09',	'0000-00-00 00:00:00'),
(0,	2,	'2210',	'BLAKE Drain',	'Плоский дренаж, пазы на 3/4 протяжения',	'',	'',	'',	'7 мм',	10,	0,	'2014-12-22 16:01:17',	'0000-00-00 00:00:00'),
(0,	2,	'2226',	'BLAKE Drain',	'Круглый дренаж, цельный',	'',	'',	'',	'10 FR',	10,	0,	'2014-12-22 16:00:54',	'0000-00-00 00:00:00'),
(0,	2,	'2231',	'BLAKE Drain',	'Круглый дренаж, цельный, с троакаром 1/4',	'',	'',	'',	'19 FR',	10,	0,	'2014-12-22 16:02:25',	'0000-00-00 00:00:00'),
(0,	2,	'BCC1',	'BLAKE',	'Кардиоконнектор 1:1, для дренажей 19 FR и 24 FR',	'',	'',	'',	'',	20,	0,	'2014-12-22 16:02:59',	'0000-00-00 00:00:00'),
(0,	3,	'AHV12',	'DERMABOND',	'',	'',	'',	'12-15 см',	'0,5 мл',	6,	0,	'2014-12-22 16:04:03',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type` enum('photo','video') NOT NULL,
  `file` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `needle_type`;
CREATE TABLE `needle_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `export_id` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `type` enum('main','other') NOT NULL DEFAULT 'main',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `product` (`id`, `export_id`, `name`, `name_en`, `type`, `created_at`, `updated_at`) VALUES
(1,	'Suture',	'Шовные материалы',	'Suture',	'main',	'0000-00-00 00:00:00',	'2014-12-21 20:51:31'),
(2,	'Drainage',	'Дренажные системы',	'Drainage',	'other',	'0000-00-00 00:00:00',	'2014-12-21 20:54:09'),
(3,	'SkinGlue',	'Клей',	'SkinGlue',	'other',	'0000-00-00 00:00:00',	'2014-12-21 20:54:09'),
(4,	'MashImplants',	'Сетки',	'MashImplants',	'other',	'0000-00-00 00:00:00',	'2014-12-21 20:56:09'),
(5,	'Special',	'Специальные продукты',	'Special',	'other',	'0000-00-00 00:00:00',	'2014-12-21 20:56:09'),
(6,	'MONOCRYL',	'MONOCRYL',	'MONOCRYL',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:07:25'),
(7,	'PDS II',	'PDS II',	'PDS II',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:08:04'),
(8,	'VICRYL',	'VICRYL',	'VICRYL',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:09:34'),
(9,	'ETHIBOND EXCEL',	'ETHIBOND EXCEL',	'ETHIBOND EXCEL',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:09:34'),
(10,	'ETHILON',	'ETHILON',	'ETHILON',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:10:30'),
(11,	'MERSILENE',	'MERSILENE',	'MERSILENE',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:10:30'),
(12,	'MONOCRYL PLUS',	'MONOCRYL PLUS',	'MONOCRYL PLUS',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:11:29'),
(13,	'NUROLON',	'NUROLON',	'NUROLON',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:11:29'),
(14,	'PDS PlUS',	'PDS PlUS',	'PDS PlUS',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:11:51'),
(15,	'PRONOVA',	'PRONOVA',	'PRONOVA',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:11:51'),
(16,	'VICRYL PLUS',	'VICRYL PLUS',	'VICRYL PLUS',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:12:46'),
(17,	'VICRYL RAPIDE',	'VICRYL RAPIDE',	'VICRYL RAPIDE',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:12:46'),
(18,	'Хирургическая проволока из нержавеющей стали',	'Хирургическая проволока из нержавеющей стали',	'Хирургическая проволока из нержавеющей стали',	'main',	'0000-00-00 00:00:00',	'2014-12-22 07:13:07');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` text NOT NULL,
  `role` enum('admin','content_manager') CHARACTER SET latin1 NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `password`, `role`, `enabled`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'9283a03246ef2dacdc21a9b137817ec1',	'admin',	1,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00');

-- 2015-01-12 00:32:56
